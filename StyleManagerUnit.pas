unit StyleManagerUnit;

interface

uses
    INIFiles, SysUtils, System.Zip, StrUtils, ShellAPI, LangAPI;

type
    ESMException = class(Exception);

    TStyleList = record
        name: array [0 .. 50] of String;    //style name
        author: array [0 .. 50] of String;  //style author
        dir: array [0 .. 50] of String;     //style directory
        count: Integer;                     //count of all installed styles
    end;

    TStyleManager = class
        ActiveStyle: String;
        StylesDir: String;
        DataFile: String;
        Styles: TStyleList;
        procedure InstallStyle(path: String);
        procedure UninstallStyle(name: String);
        procedure GetInstalledStyles();
        function GetStyleFromId(id: Integer): String;
    private
        procedure AddNewStyle(name: String);
        procedure RemoveStyle(name: String);
        function IsStyleVaild(path: String): Boolean;
        procedure DecompressStyle(path: String);
        procedure DeleteFolder(path: String);
    end;

implementation

procedure TStyleManager.InstallStyle(path: string);
begin
    if (IsStyleVaild(path)) then
    begin
        DecompressStyle(path);
        AddNewStyle(AnsiReplaceText(ExtractFilename(path),
            ExtractFileExt(path), ''));
    end
    else
        raise ESMException.Create(GetLangStr('StyleFormatError'));
end;

procedure TStyleManager.UninstallStyle(name: string);
begin
    RemoveStyle(name);
    DeleteFolder(StylesDir + '\' + name);
end;

procedure TStyleManager.GetInstalledStyles();
var
    i: Integer;
    INI: TINIFile;
begin
    INI := TINIFile.Create(StylesDir + '\' + DataFile);
    try
        Styles.count := INI.ReadInteger('Installed Styles', 'count', 0);
        if (Styles.count > 0) then
            for i := 0 to Styles.count - 1 do
            begin
                Styles.dir[i] := INI.ReadString('Installed Styles',
                    IntToStr(i), '');
            end;
    finally
        INI.Free;
    end;

    for i := 0 to Styles.count - 1 do
    begin
        INI := TINIFile.Create(StylesDir + '\' + Styles.dir[i] + '\style.ini');
        try
            Styles.name[i] := INI.ReadString('Style', 'Name', '');
            Styles.author[i] := GetLangStr('Author') + ' ' +
                INI.ReadString('Style', 'Author', '');
        finally
            INI.Free;
        end;
    end;
end;

function TStyleManager.GetStyleFromId(id: Integer): String;
var
    INI: TINIFile;
    i: Integer;
begin
    INI := TINIFile.Create(StylesDir + '\' + DataFile);
    try
        Result := INI.ReadString('Installed Styles', IntToStr(id), '');
    finally
        INI.Free;
    end;
end;

{ PRIVATE MEMBERS }

procedure TStyleManager.AddNewStyle(name: string);
var
    INI: TINIFile;
    count: Integer;
begin
    INI := TINIFile.Create(StylesDir + '\' + DataFile);
    try
        count := INI.ReadInteger('Installed Styles', 'count', 0);
        INI.WriteString('Installed Styles', IntToStr(count), name);
        count := count + 1;
        INI.WriteInteger('Installed Styles', 'count', count);
    finally
        INI.Free;
    end;
end;

procedure TStyleManager.RemoveStyle(name: string);
var
    INI: TINIFile;
    count: Integer;
    i: Integer;
    l: Integer;
    tmp: array [0 .. 50] of String;
begin
    INI := TINIFile.Create(StylesDir + '\' + DataFile);
    try
        count := INI.ReadInteger('Installed Styles', 'count', 0);
        l := 0;
        if (count > 1) then
        begin
            for i := 0 to count - 1 do
                if (INI.ReadString('Installed Styles', IntToStr(i), '') <> name)
                then
                begin
                    tmp[l] := INI.ReadString('Installed Styles',
                        IntToStr(i), '');
                    l := l + 1;
                end;
            for i := 0 to count - 1 do
                INI.DeleteKey('Installed Styles', IntToStr(i));

            count := count - 1;
            INI.WriteInteger('Installed Styles', 'count', count);
            for i := 0 to count - 1 do
                INI.WriteString('Installed Styles', IntToStr(i), tmp[i]);
        end
        else
            raise ESMException.Create(GetLangStr('StyleLastUninstall'))
    finally
        INI.Free;
    end;
end;

function TStyleManager.IsStyleVaild(path: string): Boolean;
var
    Zip: TZipFile;
    i: Integer;
begin
    Zip := TZipFile.Create;
    try
        if FileExists(path) then
        begin
            Zip.Open(path, zmReadWrite);
            for i := 0 to Zip.FileCount - 1 do
                if (Zip.FileName[i] = 'style.ini') then
                begin
                    Zip.Close();
                    Result := true;
                    break;
                end
                else if (i = Zip.FileCount - 1) then
                begin
                    Result := false;
                end;
        end
        else
            Result := false;
    finally
        Zip.Free;
    end;
end;

procedure TStyleManager.DecompressStyle(path: string);
var
    Zip: TZipFile;
begin
    Zip := TZipFile.Create;
    try
        if FileExists(path) then
        begin
            if not(DirectoryExists(StylesDir + '\' +
                AnsiReplaceText(ExtractFilename(path), ExtractFileExt(path),
                ''))) then
                Zip.ExtractZipFile(path, StylesDir + '\' +
                    AnsiReplaceText(ExtractFilename(path),
                    ExtractFileExt(path), ''));
        end;
    finally
        Zip.Free;
    end;
end;

procedure TStyleManager.DeleteFolder(path: string);
var
    FileOpStruct: TShFileOpStruct;
    ErrorCode: Integer;
begin
    if DirectoryExists(path) then
    begin
        FillChar(FileOpStruct, SizeOf(FileOpStruct), #0);
        FileOpStruct.Wnd := 0;
        FileOpStruct.wFunc := FO_DELETE;
        FileOpStruct.pFrom := PChar(path + #0#0);
        FileOpStruct.pTo := nil;
        FileOpStruct.fFlags := FOF_SILENT or FOF_NOCONFIRMATION or
            FOF_NOERRORUI or FOF_NOCONFIRMMKDIR;
        FileOpStruct.lpszProgressTitle := nil;
        ErrorCode := ShFileOperation(FileOpStruct);
    end;
end;

end.
