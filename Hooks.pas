unit Hooks;

interface

uses
    PluginAPI, Varibles, windows, dialogs, AQQFunctions, SysUtils,
    shellapi, StrUtils, LangAPI, Forms,
    UpdateManagerUnit, Functions,
    frmChooseStyle;

{ HOOKS }
procedure AddHooks();
procedure DeleteHooks();
function OnPopup(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnContactUpdate(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnColorChange(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnThemeChange(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnLangCodeChanged(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnBeforeUnload(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnListReady(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnPerformCopydata(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnReplyList(wParam: wParam; lParam: lParam): NativeInt; stdcall;
function OnAddonIstalled(wParam: wParam; lParam: lParam): NativeInt; stdcall;

{ SERVICES }

procedure RegisterServices();
procedure UnregisterServices();
function FastTalkService(wPara: wParam; lPara: lParam): NativeInt; stdcall;

implementation

{ ********************************* HOOKS ************************************ }

// Hook AQQ events with plugin functions
procedure AddHooks();
begin
    PluginLink.HookEvent(AQQ_SYSTEM_POPUP, OnPopup);
    PluginLink.HookEvent(AQQ_CONTACTS_UPDATE, OnContactUpdate);
    PluginLink.HookEvent(AQQ_SYSTEM_COLORCHANGEV2, OnColorChange);
    PluginLink.HookEvent(AQQ_SYSTEM_THEMECHANGED, OnThemeChange);
    PluginLink.HookEvent(AQQ_SYSTEM_LANGCODE_CHANGED, OnLangCodeChanged);
    PluginLink.HookEvent(AQQ_SYSTEM_BEFOREUNLOAD, OnBeforeUnload);
    PluginLink.HookEvent(AQQ_CONTACTS_LISTREADY, OnListReady);
    PluginLink.HookEvent(AQQ_SYSTEM_PERFORM_COPYDATA, OnPerformCopydata);
    PluginLink.HookEvent(AQQ_CONTACTS_REPLYLIST, OnReplyList);
    PluginLink.HookEvent(AQQ_SYSTEM_ADDONINSTALLED, OnAddonIstalled);
end;

// Unhook AQQ events
procedure DeleteHooks();
begin
    PluginLink.UnhookEvent(THandle(@OnPopup));
    PluginLink.UnhookEvent(THandle(@OnContactUpdate));
    PluginLink.UnhookEvent(THandle(@OnColorChange));
    PluginLink.UnhookEvent(THandle(@OnThemeChange));
    PluginLink.UnhookEvent(THandle(@OnLangCodeChanged));
    PluginLink.UnhookEvent(THandle(@OnBeforeUnload));
    PluginLink.UnhookEvent(THandle(@OnListReady));
    PluginLink.UnhookEvent(THandle(@OnPerformCopydata));
    PluginLink.UnhookEvent(THandle(@OnReplyList));
    PluginLink.UnhookEvent(THandle(@OnAddonIstalled));
end;

// Function called when any popup is showing
function OnPopup(wParam: wParam; lParam: lParam): NativeInt; stdcall;
begin
    if (PPluginPopup(lParam)^.Name = CONTACT_POPUP) then
    begin
        contact.JID := PPluginContact(wParam)^.JID;
        contact.Name := PPluginContact(wParam)^.Nick;
        contact.State := PPluginContact(wParam)^.State;
        contact.Resource := PPluginContact(wParam)^.Resource;

        // get all contacts on updatelist
        UpdateManager.GetUpdateList();

        if (UpdateManager.IsOnUpdateList(contact.JID)) then
        begin
            contactInfo := UpdateManager.GetInfoFromJID(contact.JID);
            if (FileExists(GetDesktopPath() + '\' + contactInfo.Name + '.lnk'))
            then
                // update popup menu item name to 'Edit shortcut'
                EditPopupItem(GetLangStr('PopUpItemEdit'))
            else
                // set popup menu item name to 'Create shortcut'
                EditPopupItem('');
        end
        else
            EditPopupItem('');
    end;
end;

// Function called when any contact on AQQ contact list update status
function OnContactUpdate(wParam: wParam; lParam: lParam): NativeInt; stdcall;
var
    n: String;
begin

    // get all contacts on updatelist
    UpdateManager.GetUpdateList();

    updateContact.JID := PPluginContact(wParam)^.JID;
    updateContact.State := PPluginContact(wParam)^.State;
    updateContact.Resource := PPluginContact(wParam)^.Resource;
    contactInfo := UpdateManager.GetInfoFromJID(updateContact.JID);

    if (UpdateManager.IsOnUpdateList(updateContact.JID) and
        not FileExists(GetDesktopPath() + '\' + contactInfo.Name + '.lnk')) then
    begin
        // if contact is on update list, but shortcut doesn't exists then
        // delete contact from the update list
        UpdateManager.RemoveFromUpdateList(updateContact.JID);
    end;

    if (UpdateManager.IsOnUpdateList(updateContact.JID)) then
    begin

        if (updateContact.State = CONTACT_INV) or
            (updateContact.State = CONTACT_NULL) then
            updateContact.State := CONTACT_OFFLINE;

        if (contactInfo.Status = true) then
            CreateLink(GetAppFilePath(), 'ft:' + updateContact.JID + '+' +
                updateContact.Resource, GetAppPath(), GetDesktopPath + '\' +
                contactInfo.Name + '.lnk', GetDirectories().Avatars + '\' +
                AnsiReplaceText(contactInfo.JID, '-', '') + '\' +
                contactInfo.style + '\' +
                IntToStr(updateContact.State) + '.ico')
        else
            CreateLink(GetAppFilePath(), 'ft:' + updateContact.JID + '+' +
                updateContact.Resource, GetAppPath(), GetDesktopPath + '\' +
                contactInfo.Name + '.lnk', GetDirectories().Avatars + '\' +
                AnsiReplaceText(contactInfo.JID, '-', '') + '\' +
                contactInfo.style + '\off.ico');

    end;
end;

// Function called when user changed theme color
function OnColorChange(wParam: wParam; lParam: lParam): NativeInt; stdcall;
begin
    Result := 0;
    if (Assigned(frmChoose)) then
    begin
        frmChoose.UpdateColors();
    end;
end;

// Function called when user changed theme
function OnThemeChange(wParam: wParam; lParam: lParam): NativeInt; stdcall;
// var
// Styles: TThemeStyles;
// i: Integer;
begin
    { if (DirectoryExists(GetThemeDir() + '\FastTalk\Styles')) then
      begin
      Styles := GetStyles(GetThemeDir() + '\FastTalk\Styles');
      StyleManager.StylesDir := GetPluginDir() + '\FastTalk\Styles';
      StyleManager.DataFile := 'Styles.ini';
      for i := 0 to Styles.count - 1 do
      if not(DirectoryExists(StyleManager.StylesDir + '\' +
      FileNameWithoutExt(Styles.filename[i]))) then
      begin
      StyleManager.InstallStyle(Styles.filename[i]);
      end;
      end; }

    if (DirectoryExists(GetThemeDir() + '\FastTalk\Icons')) and
        (FileExists(GetThemeDir() + '\FastTalk\Icons\item_icon.png')) then
        ReplacePNG(Icons[ITEM_ICON],
            PWideChar(GetThemeDir() + '\FastTalk\Icons\item_icon.png'))
    else
        ReplacePNG(Icons[ITEM_ICON],
            PWideChar(GetPluginDir() + '\FastTalk\item_icon.png'));

    if (Assigned(frmChoose)) then
    begin
        frmChoose.UpdateSkin();
        frmChoose.LoadIcons();
    end;
end;

function OnLangCodeChanged(wParam: windows.wParam; lParam: windows.lParam)
    : NativeInt; stdcall;
var
    LangCode: String;
    i: Integer;
    PlugPath: String;
begin
    ClearLngCache;
    PlugPath := GetPluginDir();
    LangCode := PWideChar(lParam);
    LangPath := PlugPath + '\Languages\FastTalk\' + LangCode + '\';

    if not DirectoryExists(LangPath) then
    begin
        LangCode :=
            PWideChar(PluginLink.CallService
            (AQQ_FUNCTION_GETDEFLANGCODE, 0, 0));
        LangPath := PlugPath + '\Languages\FastTalk\' + LangCode + '\';
    end;

    for i := 0 to Screen.FormCount - 1 do
    begin
        LangForm(Screen.Forms[i]);
        Screen.Forms[i].Caption := Screen.Forms[i].Caption + ' - ' +
            contact.Name;
    end;

    EditPopupItem('');
    frmChoose.AddStyleButton.Hint := GetLangStr('AddStyleBtnHint');
    frmChoose.RemoveStyleButton.Hint := GetLangStr('RemoveStyleBtnHint');
    StyleManager.GetInstalledStyles(); // refresh author text in combobox
end;

// Set off style avatars for shortcuts when AQQ is closed
function OnBeforeUnload(wParam: wParam; lParam: lParam): NativeInt; stdcall;
var
    i: Integer;
begin
    UpdateManager.GetUpdateList();
    for i := 0 to UpdateManager.count - 1 do
    begin
        contactInfo := UpdateManager.GetInfoFromJID
            (FileNameWithoutExt(UpdateManager.Files[i].JID));

        if (FileExists(GetDirectories.Avatars + '\' + contactInfo.JID + '\' +
            contactInfo.style + '\off.ico')) then
        begin
            if FileExists(GetDesktopPath() + '\' + contactInfo.Name + '.lnk')
            then
                CreateLink(GetAppFilePath(), 'ft:' + contactInfo.JID + '+' +
                    contactInfo.res, GetAppPath(), GetDesktopPath + '\' +
                    contactInfo.Name + '.lnk', GetDirectories.Avatars + '\' +
                    contactInfo.JID + '\' + contactInfo.style + '\off.ico')
            else
                UpdateManager.RemoveFromUpdateList(contactInfo.JID);
        end;
    end;
end;

// Set offline icon on avatars for shortcuts when AQQ is starting
function OnListReady(wParam: wParam; lParam: lParam): NativeInt; stdcall;
var
    i: Integer;
begin
    UpdateManager.GetUpdateList();
    for i := 0 to UpdateManager.count - 1 do
    begin
        contactInfo := UpdateManager.GetInfoFromJID
            (FileNameWithoutExt(UpdateManager.Files[i].JID));

        if (FileExists(GetDirectories.Avatars + '\' + contactInfo.JID + '\' +
            contactInfo.style + '\0.ico')) then
        begin
            if FileExists(GetDesktopPath() + '\' + contactInfo.Name + '.lnk')
            then
                if (contactInfo.Status = true) then
                    CreateLink(GetAppFilePath(), 'ft:' + contactInfo.JID + '+' +
                        contactInfo.res, GetAppPath(),
                        GetDesktopPath + '\' + contactInfo.Name + '.lnk',
                        GetDirectories.Avatars + '\' + contactInfo.JID + '\' +
                        contactInfo.style + '\0.ico')
                else
                    CreateLink(GetAppFilePath(), 'ft:' + contactInfo.JID + '+' +
                        contactInfo.res, GetAppPath(),
                        GetDesktopPath + '\' + contactInfo.Name + '.lnk',
                        GetDirectories.Avatars + '\' + contactInfo.JID + '\' +
                        contactInfo.style + '\off.ico')
            else
                UpdateManager.RemoveFromUpdateList(contactInfo.JID);
        end;
    end;
end;

// Called when user double click on shortcut
function OnPerformCopydata(wParam: wParam; lParam: lParam): NativeInt; stdcall;
var
    command: String;
    JID, res: String;
    id: Integer;
begin
    command := PWideChar(lParam);
    if (Pos('ft:', command) > 0) then
    begin
        res := Copy(command, Pos('+', command) + 1, Length(command));
        JID := StringReplace(command, 'ft:', '', [rfReplaceAll, rfIgnoreCase]);
        JID := StringReplace(JID, '+' + res, '', [rfReplaceAll, rfIgnoreCase]);
        id := GetTickCount();
        runJID := JID;
        runRes := res;
        PluginLink.CallService(AQQ_CONTACTS_REQUESTLIST, id, 0);
        Result := 1;
    end
    else
        Result := 0;
end;

// Called in OnPerformCopydata function. Shows message window for contact
// corresponding with shortcut
function OnReplyList(wParam: wParam; lParam: lParam): NativeInt; stdcall;
var
    pwc: PWideChar;
    temp: String;
begin
    if ((PPluginContact(lParam)^.JID = runJID) and
        (PPluginContact(lParam)^.Resource = runRes)) then
    begin
        userIdx := PPluginContact(lParam)^.userIdx;
        ExecuteMsg(userIdx, PWideChar(runJID + '/' + runRes));
    end;
end;

// Function used to run FastTalk Update Process
function OnAddonIstalled(wParam: wParam; lParam: lParam): NativeInt; stdcall;
begin
    if (ExtractFileName(PWideChar(lParam)) = 'FastTalk.dll') then
    begin
        UpdatePlugin();
    end;
end;

{ ******************************** SERVICES ********************************** }

// Register plugin services
procedure RegisterServices();
begin
    PluginLink.CreateServiceFunction(FASTTALK_SERVICE, FastTalkService);
end;

// Unregister plugin services
procedure UnregisterServices();
begin
    PluginLink.DestroyServiceFunction(THandle(@FastTalkService));
end;

// Function called when user click 'Create shortcut' button in contact popup menu
function FastTalkService(wPara: wParam; lPara: lParam): NativeInt; stdcall;
begin
    frmChoose := TfrmChoose.Create(nil);
    frmChoose.Show();
    frmChoose.Caption := GetLangStr('Title') + ' - ' + contact.Name;
    frmChoose.shortcutNameEdit.Text := NormalizeNick(contact.Name);
end;

end.
