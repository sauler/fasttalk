﻿unit frmChooseStyle;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sCheckBox, sEdit,
    Vcl.ExtCtrls, acImage, sPanel, System.ImageList, Vcl.ImgList,
    acAlphaImageList, Vcl.Buttons, sBitBtn, sComboBox, sBevel, sLabel,
    sSkinProvider, sSkinManager, AQQFunctions, Varibles, Functions, LangAPI,
    Vcl.ExtDlgs, sDialogs, AvatarFunctions, StrUtils, Vcl.AppEvnts;

type
    TfrmChoose = class(TForm)
        titleLabel: TsLabel;
        titleDescLabel: TsLabel;
        Bevel1: TsBevel;
        StylesList: TsComboBox;
        AddStyleButton: TsBitBtn;
        Icons: TsAlphaImageList;
        RemoveStyleButton: TsBitBtn;
        previewPanel: TsPanel;
        previewImage: TsImage;
        StyleLivePreview: TsAlphaImageList;
        shortcutNameLabel: TsLabel;
        shortcutNameEdit: TsEdit;
        tip1Label: TsLabel;
        Bevel2: TsBevel;
        statusIconCheckbox: TsCheckBox;
        tip2Label: TsLabel;
        sBitBtn1: TsBitBtn;
        cancelButton: TsBitBtn;
        SkinManager: TsSkinManager;
        SkinProvider: TsSkinProvider;
        StylePreviews: TsAlphaImageList;
        customAvatarLoad: TsOpenPictureDialog;
        fbTimer: TTimer;
    ApplicationEvents1: TApplicationEvents;
    styleOpenDialog: TsOpenDialog;
        procedure FormClose(Sender: TObject; var Action: TCloseAction);
        procedure FormCreate(Sender: TObject);
        procedure cancelButtonClick(Sender: TObject);
        procedure StylesListDrawItem(Control: TWinControl; Index: Integer;
            Rect: TRect; State: TOwnerDrawState);
        procedure FormShow(Sender: TObject);
        procedure statusIconCheckboxClick(Sender: TObject);
        procedure shortcutNameEditKeyPress(Sender: TObject; var Key: Char);
        procedure previewImageClick(Sender: TObject);
        procedure StylesListSelect(Sender: TObject);
        procedure fbTimerTimer(Sender: TObject);
        procedure sBitBtn1Click(Sender: TObject);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure AddStyleButtonClick(Sender: TObject);
    procedure RemoveStyleButtonClick(Sender: TObject);
    private
        procedure WMTransparency(var Message: TMessage);
            message WM_ALPHAWINDOWS;
    public
        procedure UpdateSkin();
        procedure UpdateColors();
        procedure LoadIcons();
        procedure LoadStyles();
        procedure SetButtonState();
        procedure LivePreview();
        procedure GetAvatar();
    end;

var
    frmChoose: TfrmChoose;

implementation

{$R *.dfm}
{ ***************************** FORM FUNCTIONS ******************************* }

// Function called on form create event
procedure TfrmChoose.FormCreate(Sender: TObject);
begin
    UpdateSkin();
    LoadIcons();
    LoadStyles();

    // Form language set
    LangForm(Self);
    AddStyleButton.Hint := GetLangStr('AddStyleBtnHint');
    RemoveStyleButton.Hint := GetLangStr('RemoveStyleBtnHint');
end;

// Function called on form close event
procedure TfrmChoose.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

// Function called when user click on Cancel button
procedure TfrmChoose.cancelButtonClick(Sender: TObject);
begin
    Close();
end;

// Function called on exception
procedure TfrmChoose.ApplicationEvents1Exception(Sender: TObject; E: Exception);
begin
    Log(E.ToString);
end;

// Function called when user want to install new style
procedure TfrmChoose.AddStyleButtonClick(Sender: TObject);
begin
    if (styleOpenDialog.Execute()) then
    begin
        if not(DirectoryExists(StyleManager.StylesDir + '\' +
            AnsiReplaceText(ExtractFilename(styleOpenDialog.FileName),
            ExtractFileExt(styleOpenDialog.FileName), ''))) then
        begin
            StyleManager.InstallStyle(styleOpenDialog.FileName);
            LoadStyles();
        end
        else
            AQQShowMessage(0, PWideChar(GetLangStr('StyleInstalledError')));
    end;
end;

// Function called when user want to remove style
procedure TfrmChoose.RemoveStyleButtonClick(Sender: TObject);
begin
    StyleManager.UninstallStyle
        (StyleManager.GetStyleFromId(StylesList.ItemIndex));
    LoadStyles();
end;

// Function called on draw item event
procedure TfrmChoose.StylesListDrawItem(Control: TWinControl; Index: Integer;
    Rect: TRect; State: TOwnerDrawState);
begin
    with StylesList do
    begin

        // Style preview
        StylePreviews.Draw(StylesList.Canvas, Rect.Left + 4,
            Rect.Top + 4, index);

        // Font settings
        Canvas.Font.Color := SkinManager.GetGlobalFontColor;
        Canvas.Brush.style := bsClear;
        Canvas.Font.style := [fsBold];
        Canvas.Font.Size := 9;

        // Style name
        Canvas.TextOut(Rect.Left + 61, Rect.Top + 6,
            StyleManager.Styles.name[index]);
        Canvas.Font.style := [];
        Canvas.Font.Size := 8;

        // Style author
        Canvas.TextOut(Rect.Left + 61, Rect.Top + 30,
            StyleManager.Styles.author[index]);
    end;
end;

// Function called when user select something in combobox
procedure TfrmChoose.StylesListSelect(Sender: TObject);
var
    avatarPath: String;
begin
    StyleManager.ActiveStyle := StyleManager.GetStyleFromId
        (StylesList.ItemIndex);

    SetButtonState();

    GetAvatar();
end;

// Function called on form show event
procedure TfrmChoose.FormShow(Sender: TObject);
var
    avatarPath: String;
begin
    if ((contact.State = CONTACT_INV) or (contact.State = CONTACT_NULL)) then
        contact.State := CONTACT_OFFLINE;

    SetButtonState();
    useCustomAvatar := false;

    GetAvatar();
end;

// Function called when user change statusIconCheckbox value
procedure TfrmChoose.statusIconCheckboxClick(Sender: TObject);
begin
    LivePreview();
end;

// Function called when user write something in shortcutNameEdit
procedure TfrmChoose.shortcutNameEditKeyPress(Sender: TObject; var Key: Char);
const
    illegal: set of Char = ['/', '\', ':', '*', '?', '"', '<', '>'];
begin
    if (Key in illegal) then
    begin
        Key := #0;
        MessageBeep(0);
    end;
end;

// Function called when user click on previewImage or previewPanel
procedure TfrmChoose.previewImageClick(Sender: TObject);
begin
    if (customAvatarLoad.Execute()) then
    begin
        if (customAvatarLoad.FileName <> '') then
        begin
            useCustomAvatar := true;
            ImageTool.avatarPath := customAvatarLoad.FileName;
            LivePreview();
        end;
    end;
end;

// Function called when user wants to create shortcut
procedure TfrmChoose.sBitBtn1Click(Sender: TObject);
var
    i: Integer;
begin
    StyleManager.ActiveStyle := StyleManager.GetStyleFromId
        (StylesList.ItemIndex);

    if not(DirectoryExists(GetDirectories.Avatars + '\' + contact.JID + '\' +
        StyleManager.ActiveStyle)) then
    begin
        ForceDirectories(PWideChar(GetDirectories.Avatars + '\' + contact.JID +
            '\' + StyleManager.ActiveStyle));
    end;

    if (shortcutNameEdit.Text = '') then
        shortcutNameEdit.Text := contact.name;

    if (UpdateManager.IsOnUpdateList(contact.JID)) then
    begin
        contactInfo := UpdateManager.GetInfoFromJID(contact.JID);
        DeleteFile(GetDesktopPath() + '\' + contactInfo.name + '.lnk');
        RefreshDesktop();
    end;

    ImageTool.JID := contact.JID;
    ImageTool.styleName := StyleManager.ActiveStyle;

    if (statusIconCheckbox.Checked) then
    begin
        ImageTool.MakeIconFromImage(contact.State);
        CreateLink(GetAppFilePath(), 'ft:' + contact.JID + '+' +
            contact.Resource, GetAppPath(), GetDesktopPath + '\' +
            shortcutNameEdit.Text + '.lnk', GetDirectories.Avatars + '\' +
            contact.JID + '\' + StyleManager.ActiveStyle + '\' +
            IntToStr(contact.State) + '.ico');
    end
    else
    begin
        ImageTool.MakeIconFromImage(AQQ_OFF);
        CreateLink(GetAppFilePath(), 'ft:' + contact.JID + '+' +
            contact.Resource, GetAppPath(), GetDesktopPath + '\' +
            shortcutNameEdit.Text + '.lnk', GetDirectories.Avatars + '\' +
            contact.JID + '\' + StyleManager.ActiveStyle + '\off.ico');
    end;

    RefreshDesktop();
    frmChoose.Hide;

    // Make icons for other states
    for i := 0 to 5 do
        if ((i <> contact.State) or not(statusIconCheckbox.Checked)) then
            ImageTool.MakeIconFromImage(i);

    // Make icon for aqq off state
    if (statusIconCheckbox.Checked) then
        ImageTool.MakeIconFromImage(AQQ_OFF);

    // save new info about shortcut
    contactInfo.JID := contact.JID;
    contactInfo.name := shortcutNameEdit.Text;
    contactInfo.style := StyleManager.ActiveStyle;
    contactInfo.status := statusIconCheckbox.Checked;
    contactInfo.res := contact.Resource;
    UpdateManager.AddToUpdateList(contactInfo);

    Close();
end;

// Function check if facebook avatar is downloaded
procedure TfrmChoose.fbTimerTimer(Sender: TObject);
var
    fbAvatarPath: String;
begin
    fbAvatarPath := GetDirectories.Avatars + '\' + ImageTool.JID + '\' +
        StyleManager.ActiveStyle + '\avatar.jpg';

    if (FileExists(fbAvatarPath)) then
    begin
        ImageTool.avatarPath := fbAvatarPath;
        LivePreview();
        fbTimer.Enabled := false;
    end;
end;

{ ************************** ADDITIONAL FUNCTIONS **************************** }

// Update form skin
procedure TfrmChoose.UpdateSkin();
begin
    if ((ChkSkinEnabled()) and (FileExists(GetThemeDir() + '\skin\skin.asz')))
    then
    begin
        SkinManager.Active := true;
        SkinManager.SkinDirectory := GetThemeDir() + '\skin';
        SkinManager.SkinName := 'skin.asz';
        SkinManager.HueOffset := GetHue();
        SkinManager.Saturation := GetSaturation();
        SkinManager.Brightness := GetBrightness();
        if (ChkThemeAnimateWindows()) then
            SkinManager.AnimEffects.FormShow.Time := 200
        else
            SkinManager.AnimEffects.FormShow.Time := 0;
        SkinManager.Effects.AllowGlowing := ChkThemeGlowing();
    end;

    if not((ChkSkinEnabled()) or
        not(FileExists(GetThemeDir() + '\Skin\Skin.asz'))) then
    begin
        SkinManager.Active := false;
    end;
end;

// Update form colors
procedure TfrmChoose.UpdateColors();
begin
    SkinManager.HueOffset := GetHue();
    SkinManager.Saturation := GetSaturation();
    SkinManager.Brightness := GetBrightness();
end;

// Load icons used in this form
procedure TfrmChoose.LoadIcons();
begin
    Icons.Items.Clear;
    Icons.LoadFromFile(GetThemeDir() + '\Icons\14_Add.png');
    Icons.LoadFromFile(GetThemeDir() + '\Icons\15_Delete.png');
    Icons.LoadFromFile(GetThemeDir() + '\Icons\126_Tick.png');
    Icons.LoadFromFile(GetThemeDir() + '\Icons\22_Close.png');
end;

// Load styles to styles list
procedure TfrmChoose.LoadStyles();
var
    i: Integer;
    previewPath: String;
begin
    StyleManager.GetInstalledStyles();
    StylesList.Items.Clear();
    StylePreviews.Items.Clear();

    for i := 0 to StyleManager.Styles.count - 1 do
    begin
        previewPath := GetDirectories.Styles + '\' + StyleManager.Styles.dir[i]
            + '\preview.png';
        StylesList.Items.Add(StyleManager.Styles.dir[i]);
        StylesList.ItemIndex := 0;
        StyleManager.ActiveStyle := StyleManager.GetStyleFromId
            (StylesList.ItemIndex);
        StylePreviews.LoadFromFile(previewPath);
    end;
end;

// Disable 'RemoveStyleButton' if choosed style is standard style
procedure TfrmChoose.SetButtonState();
begin
    if ((StyleManager.ActiveStyle = 'Messenger') or
        (StyleManager.ActiveStyle = 'Square')) then
        RemoveStyleButton.Enabled := false
    else
        RemoveStyleButton.Enabled := true;
end;

// Create live preview
procedure TfrmChoose.LivePreview();
begin
    ImageTool.JID := contact.JID;
    ImageTool.styleName := StyleManager.ActiveStyle;
    if (statusIconCheckbox.Checked) then
        ImageTool.MakeLivePreview(contact.State, StyleLivePreview)
    else
        ImageTool.MakeLivePreview(AQQ_OFF, StyleLivePreview)
end;

// Get avatar
procedure TfrmChoose.GetAvatar();
var
    avatarPath: String;
begin
    if not(useCustomAvatar) then
    begin
        // first we create no_avatar preview
        ImageTool.avatarPath := GetDirectories().Styles + '\' +
            StyleManager.Styles.dir[StylesList.ItemIndex] + '\no_avatar.png';
        LivePreview();

        if (AnsiContainsStr(contact.JID, '@chat.facebook.com')) then
        begin
            avatarPath := GetAvatarPath(contact.JID, StyleManager.ActiveStyle);
            fbTimer.Enabled := true;
        end
        else
        begin
            avatarPath := GetAvatarPath(contact.JID, StyleManager.ActiveStyle);
            ImageTool.avatarPath := StringReplace(avatarPath, '{PROFILEPATH}',
                GetUserDir(), [rfReplaceAll, rfIgnoreCase]);
        end;

        if not(FileExists(ImageTool.avatarPath)) then
            ImageTool.avatarPath := GetDirectories().Styles + '\' +
                StyleManager.Styles.dir[StylesList.ItemIndex] +
                '\no_avatar.png';

        LivePreview();
    end
    else
        LivePreview();
end;

{ ************************** OTHER PLUGINS SUPPORT *************************** }

// Support for AlphaWindows plugin
procedure TfrmChoose.WMTransparency(var Message: TMessage);
begin
    Application.ProcessMessages;
    if (SkinManager.Active) then
        SkinProvider.BorderForm.UpdateExBordersPos(true, Message.LParam);
end;

end.
