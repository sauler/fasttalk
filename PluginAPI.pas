unit PluginAPI;

interface

uses Windows;

/// Hooks
const
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Komunikator AQQ został wyłączony.
  AQQ_SYSTEM_BEFOREUNLOAD = 'AQQ/System/BeforeUnload';
  // Notyfikacja: TAK wParam=PWidechar (identyfikator sieci) lParam=0 Res=0
  // Opis: Jeżeli wtyczka sieciowa obsługuje wskazaną sieć w parametrze wParam, powinna uruchomić się jej konfiguracja.
  AQQ_SYSTEM_PRECONFIG = 'AQQ/System/PreConfig';
  // Notyfikacja: TAK wParam=PWideChar (JID kontaktu) lParam=0 Res=0
  // Opis: Informuje o zamknięciu notyfikacji o nowej wiadomości w zasobniku systemowym.
  AQQ_SYSTEM_NOTIFICATIONCLOSED = 'AQQ/System/NotificationClosed';
  // Funkcja: TAK wParam=PPluginNewsData lParam=Integer (indeks konta) Res=0
  // Opis: Dodaje nowe źródło powiadomień.
  AQQ_SYSTEM_NEWSSOURCE_ADD = 'AQQ/System/NewsSource/Add';
  // Notyfikacja: TAK wParam=PWideChar (źródło) lParam=0 Res=0
  // Opis: Informuje o usunięciu źródła powiadomień.
  // Funkcja: TAK wParam=PPluginNewsData lParam=0 Res=[-1 błąd; 0 ok]
  // Opis: Usuwa źródło powiadomień o podanym ID.
  AQQ_SYSTEM_NEWSSOURCE_DELETE = 'AQQ/System/NewsSource/Delete';
  // Funkcja: TAK wParam=PPluginNewsData lParam=0 Res=[-1 błąd; 0 ok]
  // Opis: Uaktualnia dane o źródle powiadomień.
  AQQ_SYSTEM_NEWSSOURCE_UPDATE = 'AQQ/System/NewsSource/Update';
  // Notyfikacja: TAK wParam=PWideChar (źródło) lParam=[0 nieaktywne; 1 aktywne] Res=[-1 błąd; 0 ok]
  // Opis: Informuje o zmianie stanu aktywności źródła przez użytkownika.
  AQQ_SYSTEM_NEWSSOURCE_ACTIVE = 'AQQ/System/NewsSource/Active';
  // Funkcja: TAK wParam=PPluginNewsData lParam=0 Res=[-1 błąd; 0 ok]
  // Opis: Odświeża źródło powiadomień.
  AQQ_SYSTEM_NEWSSOURCE_REFRESH = 'AQQ/System/NewsSource/Refresh';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje o rozpoczęciu procesu synchronizacji źródeł powiadomień.
  AQQ_SYSTEM_NEWSSOURCE_BEFOREFETCH = 'AQQ/System/NewsSource/BeforeFetch';
  // Notyfikacja: TAK wParam=Integer (ID) lParam=PWideChar (źródło) Res=[0 nie dotyczy; -1 błąd; 1 ok]
  // Opis: Wskazane źródło powiadomień jest proszone o pobranie danych. wParam zawiera wew. ID źródła powiadomień.
  AQQ_SYSTEM_NEWSSOURCE_FETCH = 'AQQ/System/NewsSource/Fetch';
  // Funkcja: TAK wParam=Integer (wew. ID) lParam=0 Res=0
  // Opis: Informujemy o rozpoczęciu pobierania danych dla źródła. ID pochodzi z notyfikacji AQQ_SYSTEM_NEWSSOURCE_FETCH.
  AQQ_SYSTEM_NEWSSOURCE_FETCHSTART = 'AQQ/System/NewsSource/FetchStart';
  // Funkcja: TAK wParam=Integer (wew. ID) lParam=[0 ok; 1 nie udało się pobrać danych] Res=[1 ok; 0 błąd]
  // Opis: Informujemy o zakończeniu pobierania danych dla źródła. ID pochodzi z notyfikacji AQQ_SYSTEM_NEWSSOURCE_FETCH.
  AQQ_SYSTEM_NEWSSOURCE_FETCHEND = 'AQQ/System/NewsSource/FetchEnd';
  // Funkcja: TAK wParam=PPluginNewsItem lParam=0 Res=0
  // Opis: Przekazujemy dane na temat pobranego powiadomienia.
  AQQ_SYSTEM_NEWSSOURCE_ITEM = 'AQQ/System/NewsSource/Item';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=PPluginAccountInfo
  // Opis: AQQ odpytuje o informacje na temat konta do wyświetlenia w oknie zmiany opisu.
  AQQ_SYSTEM_GETACCOUNTINFO = 'AQQ/System/GetAccountInfo';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginPopUp Res=0
  // Opis: Dotyczy popmenu kontaktu na liście, które ma się pojawić za chwilę.
  AQQ_SYSTEM_POPUP = 'AQQ/System/Popup';
  // Notyfikacja: TAK wParam=0 lParam=PPluginPopUp Res=0
  // Opis: Dotyczy multi-popmenu zaznaczonych kontaktów na liście, które ma się pojawić za chwilę.
  AQQ_SYSTEM_MULTIPOPUP = 'AQQ/System/MultiPopup';
  // Funkcja: TAK wParam=0 lParam=PPluginChatPresence Res=0
  // Opis: Przekazujemy powiadomienie na temat aktywności kontaktu na czacie.
  AQQ_SYSTEM_CHAT_PRESENCE = 'AQQ/System/Chat/Presence';
  // Funkcja: TAK wParam=0 lParam=PPluginChatOpen Res=0
  // Opis: Otwieramy nowe okno pokoju rozmów.
  AQQ_SYSTEM_CHAT_OPEN = 'AQQ/System/Chat/Open';
  // Notyfikacja: TAK wParam=0 lParam=PPluginSong Res=[0 nie dotyczy; 1 ok]
  // Opis: Zwracamy piosenkę/utwór który jest aktualnie odgrywany.
  AQQ_SYSTEM_CURRENTSONG = 'AQQ/System/CurrentSong';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginChatState Res=[0 ok; 1 nastąpiły zmiany w lParam]
  // Opis: Informuje o pisaniu przez użytkownika wiadomości tekstowej.
  // Funkcja: TAK wParam=PPluginContact lParam=PPluginChatState Res=[0 błąd; 1 ok]
  // Opis: Zmiana w funkcji "pisaka" dla wskazanego rozmówcy.
  AQQ_SYSTEM_MSGCOMPOSING = 'AQQ/System/MsgComposing';
  // Funkcja: TAK wParam=0 lParam=PPluginError Res=[0 okno nie znalezione; 1 ok]
  // Opis: Informujemy o powstałym błędzie (w protokole wtyczki sieciowej) - w oknie rozmowy z kontaktem.
  AQQ_SYSTEM_ERROR = 'AQQ/System/Error';
  // Funkcja: TAK wParam=PPluginHook lParam=0 Res=0
  // Opis: Wywołujemy dowolną wskazaną funkcje / notyfikacje w obszarze SDK dla innych wtyczek.
  AQQ_SYSTEM_SENDHOOK = 'AQQ/System/SendHook';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje wtyczkę o załadowaniu się wszystkich dostępnych innych wtyczek.
  // Funkcja: TAK wParam=0 lParam=0 Res=[0 wtyczki nie załadowane; 1 wtyczki załadowane]
  // Opis: Odpytuję czy wszystkie wtyczki zostały przez AQQ załadowane.
  AQQ_SYSTEM_MODULESLOADED = 'AQQ/System/ModulesLoaded';
  // Funkcja: TAK wParam=0 lParam=PPluginSMSGate Res=Integer (Nadane ID bramki)
  // Opis: Funkcja dodaje nową bramkę SMS do zakładki SMSów w oknie głównym AQQ.
  AQQ_SYSTEM_ADDSMSGATE = 'AQQ/System/AddSMSGate';
  // Funkcja: TAK wParam=Integer (ID bramki) lParam=0 Res=[0 błąd; 1 ok]
  // Opis: Funkcja usuwa dodaną bramkę SMS - należy podać ID bramki uzyskane przy jej dodawaniu.
  AQQ_SYSTEM_REMOVESMSGATE = 'AQQ/System/RemoveSMSGate';
  // Funkcja: TAK wParam=PWideChar (nowa nazwa) lParam=Integer (ID bramki) Res=[0 błąd; 1 ok]
  // Opis: Funkcja zmienia nazwę dodanej bramki SMS.
  AQQ_SYSTEM_RENAMESMSGATE = 'AQQ/System/RenameSMSGate';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje wtyczkę, że o ile jest sieciowa, powinna przedstawić listę dostępnych agentów na serwerze (muc, wyszukiwarka, rejestracja, etc.)
  AQQ_SYSTEM_GETAGENTS = 'AQQ/System/GetAgents';
  // Funkcja: TAK wParam=0 lParam=PPluginAgent Res=[1 agent już istnieje i zostanie zaktualizowany; 0 agent zostanie dodany]
  // Opis: Funkcja edytuje lub dodaje nowego agenta serwera Jabberowego (lub jakiegokolwiek innego zgodnego ze standardami).
  AQQ_SYSTEM_SETAGENT = 'AQQ/System/SetAgent';
  // Notyfikacja: TAK wParam=PWideChar (jid agenta) lParam=Integer (stała sprawdzająca ##013 #0014) Res=[0 agent aktywny; 1 agent nie aktywny]
  // Opis: AQQ odpytuje, czy dany agent na serwerze obsługuję w danym momencie akcje. Akcja oznaczona jest stałą sprawdzającą, np. AQQ_QUERY_MESSAGE.
  AQQ_SYSTEM_ISAGENTENABLED = 'AQQ/System/IsAgentEnabled';
  // Funkcja: TAK wParam=PWideChar (jid agenta) lParam=0 Res=[1 usunięto; 0 nie usunięto/nie znaleziono agenta]
  // Opis: Funkcja usuwa dodanego wcześniej agenta.
  AQQ_SYSTEM_REMOVEAGENT = 'AQQ/System/RemoveAgent';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=[0 agent aktywny; 1 agent nie aktywny]
  // Opis: AQQ informuje, że wtyczka sieciowa - o ile obsługuję import/eksport kontaktów - powinna o tym poinformować AQQ za pomocą AQQ_SYSTEM_ADDIMPEXP (każdorazowo, po każdym takim zapytaniu).
  AQQ_SYSTEM_GETIMPEXP = 'AQQ/System/GetImpExp';
  // Funkcja: TAK wParam=0 lParam=PPluginImpExp Res=0
  // Opis: Funkcja dodaje serwis importu/eksportu kontaktów lub innych akcji związanych z listą kontaktów, które obsługuje wtyczka sieciowa.
  AQQ_SYSTEM_ADDIMPEXP = 'AQQ/System/AddImpExp';
  // Notyfikacja: TAK wParam=PPluginContact lParam=Integer (stała sprawdzająca ##013 ##014) Res=[0 zgoda/lub nie dotyczy; 1 akcja zablokowana]
  // Opis: AQQ odpytuje, czy dla wskazanego kontaktu, można wykonać akcje oznaczoną stałą sprawdzającą (np. AQQ_QUERY_SENDFILE).
  AQQ_SYSTEM_QUERY = 'AQQ/System/Query';
  // Notyfikacja: TAK wParam=PPluginStateChange lParam=Integer (stała sprawdzająca ##013 ##014) Res=[0 błąd; 1 wypełniono PPluginStateChange]
  // Opis: AQQ prosi o wypełnienie struktury sprawdzającej stan danego konta. Stała sprawdzająca w tym wypadku, to zazwyczaj zawsze AQQ_QUERY_NETWORKSTATE.
  AQQ_SYSTEM_QUERYEX = 'AQQ/System/QueryEx';
  // Notyfikacja: TAK wParam=PWideChar (numer telefonu) lParam=0 Res=[0 brak obsługi; Numeryczne ID bramki obsługującej nr. telefonu]
  // Opis: AQQ prosi wtyczkę o odpowiedź, która z wew. bramek SMS, obsługuje wskazany numer telefonu (o ile jakakolwiek to robi).
  AQQ_SYSTEM_SMSSUPPORTED = 'AQQ/System/SMSSupported';
  // Notyfikacja: TAK wParam=Integer (wew. ID bramki) lParam=0 Res=
  // Opis: Użytkownik uruchomił konfigurator wskazanej bramki SMS.
  AQQ_SYSTEM_SMSCONFIG = 'AQQ/System/SMSConfig';
  // Notyfikacja: TAK wParam=PPluginSMSResult lParam=PPluginSMS Res=Integer (stała wyniku wysyłki SMS ##021)
  // Opis: Użytkownik wysyła wiadomość SMS.
  AQQ_SYSTEM_SMSSEND = 'AQQ/System/SMSSend';
  // Notyfikacja: TAK wParam=Integer (0 domyślnie; 1 stałe ##004) lParam=PPluginStateChange Res=0
  // Opis: AQQ prosi o zmianę stanu wtyczki sieciowej, według podanych danych w strukturze PPluginStateChange.
  AQQ_SYSTEM_SETNOTE = 'AQQ/System/SetNote';
  // Notyfikacja: TAK wParam=0 lParam=PPluginWindowEvent Res=0
  // Opis: AQQ informuję o zmianie stanu jednego z otwartych okien.
  AQQ_SYSTEM_WINDOWEVENT = 'AQQ/System/WindowEvent';
  // Notyfikacja: TAK wParam=0 lParam=PPluginStateChange Res=0
  // Opis: AQQ informuję o zmianie stanu jednego z kont sieciowych.
  AQQ_SYSTEM_STATECHANGE = 'AQQ/System/StateChange';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ prosi wtyczki sieciowe, o nazwiązanie połączenia z zapamiętanym ostatnio stanem (przy starcie programu).
  AQQ_SYSTEM_SETLASTSTATE = 'AQQ/System/SetLastState';
  // Funkcja: TAK wParam=Integer (0 domyślnie; 1 wymuś reset połączenia) lParam=PWideChar (nowa nazwa zasobu) Res=0
  // Opis: Funkcja zmienia treść zasobów dla wszystkich sieci Jabber. Możliwy jest też restart połączenia.
  AQQ_SYSTEM_CHANGE_JABBERRESOURCES = 'AQQ/System/Change/Resources';
  // Funkcja: TAK wParam=0 lParam=PWideChar (nazwa akcji) Res=[0 błąd; 1 akcja uruchomiona]
  // Opis: Funkcja uruchamia jedną z wew. akcji dostępnych w AQQ (np. aSettings uruchomi ustawienia AQQ).
  AQQ_SYSTEM_RUNACTION = 'AQQ/System/RunAction';
  // Funkcja: TAK wParam=PWideChar (XML) lParam=Integer (ID konta) Res=[0 wysłano; 1 błędne ID; 2 konto rozłączone; 3 pakiet zablokowany]
  // Opis: Funkcja wysyła pakiet XML poprzez jedno ze wskazanych kont Jabberowych.
  AQQ_SYSTEM_SENDXML = 'AQQ/System/SendXML';
  // Notyfikacja: TAK wParam=PWideChar (XML) lParam=PPluginXMLChunk Res=[0 nie dotyczy; 1 pakiet został obsłużony]
  // Opis: Podgląda pakietów Jabberowych zawierających pole ID. Wtyczka może wybiórczo obsłużyć którykolwiek z tych pakietów.
  AQQ_SYSTEM_XMLIDDEBUG = 'AQQ/System/XMLIDDebug';
  // Notyfikacja: TAK wParam=PWideChar (XML) lParam=PPluginXMLChunk Res=[0 nie dotyczy; 1 pakiet został obsłużony]
  // Opis: Podgląda pakietów Jabberowych. Wtyczka może wybiórczo obsłużyć którykolwiek z tych pakietów.
  AQQ_SYSTEM_XMLDEBUG = 'AQQ/System/XMLDebug';
  // Notyfikacja: TAK wParam=PPluginContact lParam=Integer (stała online ##018) Res=[0 nie dotyczy; 1 konto połączone + zgoda; 2 konto rozłączone + brak zgody; 3 brak zogdy]
  // Opis: AQQ odpytuje wtyczkę sieciową czy dla wskazanego kontaktu, może nastąpić akcja oznaczoną stałą online (np. ONCHECK_DELETE).
  AQQ_SYSTEM_ONLINECHECK = 'AQQ/System/OnlineCheck';
  // Funkcja: TAK wParam=0 lParam=PPluginDebugInfo Res=0
  // Opis: Funkcja dodaje pakiet XML do konna konsili XML.
  AQQ_SYSTEM_DEBUG_XML = 'AQQ/System/Debug/XML';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Wtyczka sieciowa po otrzymaniu tej notyfikacji, powinna się natychmiast rozłączyć z serwerem.
  AQQ_SYSTEM_DISCONNECT = 'AQQ/System/Disconnect';
  // Notyfikacja: TAK wParam=0 lParam=PPluginAutomation Res=0
  // Opis: AQQ informuję, że auto-oddalenie zostało aktywowane.
  AQQ_SYSTEM_AUTOMATION_AUTOAWAY_ON = 'AQQ/System/Automation/AutoAway/On';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ informuję, że auto-oddalenie zostało deaktywowane.
  AQQ_SYSTEM_AUTOMATION_AUTOAWAY_OFF = 'AQQ/System/Automation/AutoAway/Off';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Wtyczka sieciowa, zaraz po połączeniu powinna wywołać tą funkcje. AQQ przez kolejne 10s nie będzie informować o żadnych zmianach stanu użytkowników na liście.
  AQQ_SYSTEM_ONCONNECT_SILENCE = 'AQQ/System/OnConnect/Silence';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID) Res=[PWideChar ścieżka do pliku graficznego; 0 nie dotyczy]
  // Opis: O ile wskazany kontakt pochodzi z odpytywanej wtyczki, powinna ona zwrócić ścieżkę do pliku graficznego skojarzonego z aktualnym stanem kontaktu.
  AQQ_SYSTEM_GETCURRENTSHOWTYPE_PATH = 'AQQ/System/GetCurrentShowType/Path';
  // Notyfikacja: TAK wParam=PPluginSmallInfo lParam=PWideChar (domena) Res=[0 nie dotyczy; PWidechar UID]
  // Opis: AQQ prosi o zwrot, lub wypełnienie mini struktury aktualnym UID-em, skojarzonym ze wskazaną domeną.
  AQQ_SYSTEM_GETCURRENTUID = 'AQQ/System/GetCurrentUID';
  // Notyfikacja: TAK wParam=[0 brak obramowania; 1 obramowanie aktywne] lParam=PWideChar (Ścieżka do aktywnej kompozycji wizualnej) Res=0
  // Opis: AQQ informuję, że aktywna kompozycja wizualna zmieniła się.
  AQQ_SYSTEM_THEMECHANGED = 'AQQ/System/ThemeChanged';
  // Notyfikacja: TAK wParam=[0 brak obramowania; 1 obramowanie aktywne; 2 styl natywny Windows] lParam=PWideChar (Ścieżka do aktywnej kompozycji wizualnej) Res=0
  // Opis: AQQ informuję przy starcie, jaka aktualnie wybrana jest kompozycja wizualna.
  AQQ_SYSTEM_THEMESTART = 'AQQ/System/ThemeStart';
  // Notyfikacja: TAK wParam=[0 brak obramowania; 1 obramowanie aktywne; 2 styl natywny Windows] lParam=PWideChar (Ścieżka do aktywnej kompozycji wizualnej) Res=0
  // Opis: Patrz AQQ_SYSTEM_THEMESTART. AQQ rozsyła tą notyfikacje gdy jakaś wtyczka zostanie ręcznie włączona np. w ustawieniach AQQ.
  AQQ_SYSTEM_THEMEINFO = 'AQQ/System/ThemeStart';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=[0; nie dotyczy; TResultEx = array of Integer gdzie poszczególne wartości zawierają wskaźniki na PPluginMaxStatus]
  // Opis: Wtyczka może podać informacje odtyczące nazw obsługiwanych kont, indeksów ikon, i max. długości obsługiwanego statusu opisowego. Dane umiszczone zostaną w oknie zmiany stanu.
  AQQ_SYSTEM_MAXSTATUSLENGTH = 'AQQ/System/MaxStatusLength';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0; nie dotyczy; Integer max. długość wiadomości tekstowej]
  // Opis: AQQ odpytuje wtyczkę sieciową, o max. długość wiadomości tekstowej jaką można wysłać do wskazanego kontaktu.
  AQQ_SYSTEM_MAXMSGLENGTH = 'AQQ/System/MaxMsgLength';
  // Funkcja: TAK wParam=PWideChar (nazwa pliku bez ścieżki) lParam=PWideChar (JID kontaktu) Res=PWideChar wygenerowana ścieżka do pliku
  // Opis: Tworzy nową ścieżkę, zawierającą zmodyfikowaną wskazaną nazwę pliku, której można użyć przy zapisie pliku. Stosowane np. w trakcie odbierania obrazków prosto do okna rozmowy.
  AQQ_SYSTEM_GETNEWCACHEITEMPATH = 'AQQ/System/GetNewCacheItemPath';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (dane) Res=[0; nie dotyczy; 1 wtyczka obsłużyła]
  // Opis: Obsługa URL Protocol. Notyfikacja wywoływana gdy jakaś akcja z ekploratora Windows zostanie przekazana do AQQ (np. skojarzenie z plikami .aqq).
  AQQ_SYSTEM_PERFORM_COPYDATA = 'AQQ/System/Perform/CopyData';
  // Notyfikacja: TAK wParam=Integer (stała stanu) lParam=0 Res=0
  // Opis: AQQ prosi wtyczkę sieciową o zmianę stanu konta. Wtyczka powinna się podporządkować.
  AQQ_SYSTEM_FORCESTATUS = 'AQQ/System/ForceStatus';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 nie dotyczy; PWideChar - JID/UID]
  // Opis: AQQ prosi wtyczkę o wskazanie, co powinno znaleźć się w schowku Windows, po użyciu funkcji kopiuj JID/UID (dla wskazanego kontaktu)
  AQQ_SYSTEM_CLIPBOARD_JID = 'AQQ/System/Clipboard/JID';
  // Notyfikacja: TAK wParam=0 lParam=Integer (stała zakładek ##003) Res=0
  // Opis: AQQ informuje która z zakładek w oknie głównym jest obecnie aktywna.
  AQQ_SYSTEM_TABCHANGE = 'AQQ/System/TabChange';
  // Funkcja: TAK wParam=Integer (ID konta) lParam=PWideChar (XML) Res=0
  // Opis: Interpretuje pakiet XML (tak jak by dotarł on prosta z serwera) dla wskazanego konta Jabberowego.
  AQQ_SYSTEM_INTERPRET_XML = 'AQQ/System/Interpret/XML';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTransfer Res=[0 nie dotyczy; 1 ok]
  // Opis: Informuje o zmianie stanu transferu pliku (np. odrzucenie).
  // Funkcja: TAK wParam=0 lParam=PPluginTransfer Res=0
  // Opis: Aktualizuje stan transferu pliku.
  AQQ_SYSTEM_TRANSFER_STATUS_CHANGE = 'AQQ/System/Transfer/Status/Change';
  // Notyfikacja: TAK wParam=0 lParam=PPluginAutomation Res=0
  // Opis: AQQ informuje o automatycznym przejściu w stan zabezpieczenia.
  AQQ_SYSTEM_AUTOMATION_AUTOSECURE = 'AQQ/System/Automation/AutoSecure';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ zostało zabezpieczone.
  AQQ_SYSTEM_AUTOSECURE_ON = 'AQQ/System/AutoSecure/On';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ zostało odbezpieczone.
  AQQ_SYSTEM_AUTOSECURE_OFF = 'AQQ/System/AutoSecure/Off';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTriple (Handle1 uchwyt okna; Handle2 uchwyt pola; Handle3 uchwyt menu; Param1 x; Param2 y] Res=0
  // Opis: Informuje o tym, że za chwilę pojawi się menu skojarzone z danym polem tekstowym.
  AQQ_SYSTEM_MSGCONTEXT_POPUP = 'AQQ/System/OnMsgContent/Popup';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTriple (Handle1 uchwyt okna; Handle2 uchwyt pola; Handle3 uchwyt menu; Param1 x; Param2 y] Res=0
  // Opis: Informuje o tym, że menu skojarzone z danym polem tekstowym zostało zamknięte. W pewnych sytuacjach, ta notyfikacja może się nie pojawić (np. przy otwieraniu menu przy pomocy skórtu na klawiaturze a nie przy pomocy myszy).
  AQQ_SYSTEM_MSGCONTEXT_CLOSE = 'AQQ/System/OnMsgContent/Close';
  // Funkcja: TAK wParam=Integer (stała stanu wiadomości sms ##020) lParam=PWideChar (ID SMS-a) Res=0
  // Opis: Aktualizuje status wiadomości SMS (np. SMS_STATUS_DELIVERED).
  AQQ_SYSTEM_CHANGESMS_STATUS = 'AQQ/System/ChangeSMS/Status';
  // Funkcja: TAK wParam=PWideChar (stare ID wiadomości SMS) lParam=PWideChar (nowe ID wiadomości SMS) Res=0
  // Opis: Aktualizuje ID wiadomości SMS (zmienia je na nowe).
  AQQ_SYSTEM_CHANGESMS_ID = 'AQQ/System/ChangeSMS/ID';
  // Notyfikacja: TAK wParam=0 lParam=PPluginToolTipID Res=[0 ok; 1 blokada tooltipa]
  // Opis: Informuje, że zaraz pojawi się tooltip kontaktu na liście. Można go teraz uzupełnić o dodatkowe dane.
  AQQ_SYSTEM_TOOLTIP_FILL = 'AQQ/System/ToolTip/Fill';
  // Notyfikacja: TAK wParam=PPluginTriple (Param1 typ dodatku TAddonType Param2 dodatek aktywny po instalacji 0/1] lParam=PWideChar (ścieżka do dodatku jeżeli możliwe jest jej podanie) Res=0
  // Opis: Informuje o tym, że dodatek został właśnie zainstalowany.
  AQQ_SYSTEM_ADDONINSTALLED = 'AQQ/System/AddonInstalled';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=[0 ok; 1 blokada]
  // Opis: Informuje, że zaraz pojawi się tooltip kontaktu na liście. Można go zablokować.
  AQQ_SYSTEM_TOOLTIP_BEFORESHOW = 'AQQ/System/ToolTip/BeforeShow';
  // Funkcja: TAK wParam=0 lParam=PPluginToolTipItem Res=0
  // Opis: Dodaje nowe pole do tooltipa kontaktu na liście.
  AQQ_SYSTEM_TOOLTIP_ADDITEM = 'AQQ/System/ToolTip/AddItem';
  // Funkcja: TAK wParam=Integer [delay 0, lub więcej w milisekundach] lParam=0 Res=0
  // Opis: Pokazuje przygotowany ostatnio tooltip.
  AQQ_SYSTEM_TOOLTIP_SHOW = 'AQQ/System/ToolTip/Show';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID) Res=[0 nie dotyczy; Integer stała połączenia np. CONNSTATE_CONNECTING ##008]
  // Opis: AQQ pyta o stan połączenia z siecią skojarzoną ze wskazanym kontaktem.
  AQQ_SYSTEM_CONNECTION_STATE = 'AQQ/System/Connection/State';
  // Funkcja: TAK wParam=0 lParam=PWideChar (ścieżka do nowej kompozycji wizualnej) Res=0
  // Opis: Zmiania aktywną kompozycje wizualną (ale nie aktywuje jej!)
  AQQ_SYSTEM_THEME_SET = 'AQQ/System/Theme/Set';
  // Funkcja: TAK wParam=0 lParam=[0 szybki refresh; 1 odświeża wszystko] Res=0
  // Opis: Aktywuje wybraną kompozycje wizualną.
  AQQ_SYSTEM_THEME_APPLY = 'AQQ/System/Theme/Apply';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Odświeża listę kontaktów względem aktywnej kompozycji wizualnej.
  AQQ_SYSTEM_THEME_REFRESH = 'AQQ/System/Theme/Refresh';
  // Funkcja: TAK wParam=0 lParam=PPluginChatPrep Res=0
  // Opis: Otwiera/Tworzy czat na kanale.
  AQQ_SYSTEM_CHAT = 'AQQ/System/Chat';
  // Funkcja: TAK wParam=0 lParam=PPluginStateChange Res=0
  // Opis: Zmienia status i stan wszystkich kont.
  AQQ_SYSTEM_SETSHOWANDSTATUS = 'AQQ/System/SetShowAndStatus';
  // Funkcja: TAK wParam=[0 dodanie; 1 usunięcie] lParam=PWideChar (odnośnik URL) Res=[0 dodano; 1 usunięto]
  // Opis: Dodaje, bądź usuwa odnośniki URL z których AQQ pobiera informacje na temat dostępnych aktualizacji.
  AQQ_SYSTEM_SETUPDATELINK = 'AQQ/System/SetUpdateLink';
  // Funkcja: TAK wParam=Integer (stała funkcyjna ##002) lParam=[0 wyłączono; 1 włączono] Res=0
  // Opis: Włącza lub wyłącza niektóre funkcje związane z działaniem AQQ. Np. SYS_FUNCTION_SEARCHONLIST
  AQQ_SYSTEM_FUNCTION_SETENABLED = 'AQQ/System/SetEnabled';
  // Funkcja: TAK wParam=Integer (0 jeżeli dodajemy; ID uzyskane poprzednim razem jeżeli edytujemy/usuwamy) lParam=PPluginAccountEvents Res=Integer (ID)
  // Opis: Ustala, jakie działania są obsługiwane we wtyczce sieciowej względem konta i dodaje je na listę kont wtyczek sieciowych w ustawieniach AQQ. Aby usunąć resetujemy wszystkie pola w PPluginAccountEvents, i ustalamy tylko ID.
  AQQ_SYSTEM_ACCOUNT_EVENTS = 'AQQ/System/Account/Events';
  // Notyfikacja: TAK wParam=Integer (ID) lParam=Integer (stała akcji konta ##001) Res=0
  // Opis: AQQ uruchamia wskazaną akcje dla wskazanego konta. Np. ACCOUNT_EVENT_CHANGEPASS
  AQQ_SYSTEM_ACCOUNT_RUNEVENT = 'AQQ/System/Account/RunEvent';
  // Funkcja: TAK wParam=[0 zwykły restart; 1 restart z dzierżawą uprawnień na 15s] lParam=0 Res=0
  // Opis: Restartuje AQQ.
  AQQ_SYSTEM_RESTART = 'AQQ/System/Restart';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informacja o rozpoczętym restarcie komunikatora.
  // Funkcja: TAK wParam=[0; bez wskazania; PWideChar ścieżka do wtyczki, spowoduje odświeżenie tylko jej] lParam=[0 odświeża wtyczki; 1 odświeża wtyczki również te niewczytane] Res=0
  // Opis: Odświeża listę wtyczek w ustawieniach AQQ.
  AQQ_SYSTEM_PLUGIN_REFRESHLIST = 'AQQ/System/Plugin/RefreshList';
  // Funkcja: TAK wParam=[0; dodajemy do listy 1 usuwamy z listy; 2 sprawdzmy czy wtyczka jest na liście] lParam=PWideChar (ściężka do wtyczki) Res=[0 ok; 1 wtyczka wyłączona (przy sprawdzaniu)]
  // Opis: Dodaje wtyczkę do listy wtyczek wykluczonych.
  AQQ_SYSTEM_PLUGIN_EXCLUDE = 'AQQ/System/Plugin/Exclude';
  // Funkcja: TAK wParam=[0; włączamy wtyczkę; 1 wyłączamy wtyczkę; 2 sprawdzamy czy wtyczka aktywna] lParam=PWideChar (ściężka do wtyczki) Res=[Integer uchwyt do wtyczki; 0 tylko przy wyłączeniu wtyczki]
  // Opis: Włącza lub wyłącza wtyczkę. Można sprawdzić też, czy dana wtyczka jest aktualnie włączona.
  AQQ_SYSTEM_PLUGIN_ACTIVE = 'AQQ/System/Plugin/Active';
  // Funkcja: TAK wParam=Integer (uchwyt do wtyczki) lParam=PWideChar (ściężka do wtyczki) Res=[0 domyślnie; 1 zablokuj wyładowanie]
  // Opis: Użytkownik próbuje wyładować wtyczkę z poziomu ustawień programu. Wtyczka może zablokować wyładowanie zwracając wartość 1.
  AQQ_SYSTEM_PLUGIN_BEFOREUNLOAD = 'AQQ/System/Plugin/BeforeUnload';
  // Notyfikacja: TAK wParam=PWideChar (ścieżka do dźwięku) lParam=Integer (stała dźwięku ##022) Res=Integer [0 - bez zmian; 1 - zablokowanie odegrania dźwięku]
  // Opis: Wskazuje, że zaraz zostanie odegrany dźwięk. Taki dźwięk można zablokować zwracając wartość 1.
  // Funkcja: TAK wParam=Integer (stała dźwięku ##022 / lub ścieżka do dźwięku) lParam=[0 domyślnie; 1 wymusza odegranie dźwięku; 2 odgrywa plik z podaje ścieżki] Res=0
  // Opis: Odgrywa wybrany dźwięk.
  AQQ_SYSTEM_PLAYSOUND = 'AQQ/System/Plugin/PlaySound';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (Adres URL) Res=PWideChar [0 nie robi nic; PWideChar nowy adres URL)
  // Opis: Informuję, że otwierany jest wew. browser dodatków dla AQQ. Notyfikacja podaje adres URL który zostanie otwarty. Można go zmienić zwracając nowy adres URL do notyfikacji.
  AQQ_SYSTEM_ADDONBROWSER_URL = 'AQQ/System/AddonBrowser/URL';
  // Funkcja: TAK wParam=[1; start rozmowy; 2 koniec rozmowy] lParam=[Handle uchwyt okna rozmowy] Res=0
  // Opis: Informuję AQQ, że rozpoczyna się lub kończy rozmowa w ramach sieci skype (audio bądź video).
  AQQ_SYSTEM_SKYPE_CONVSERSATION = 'AQQ/System/Skype/Conversation';
  // Notyfikacja: TAK wParam=Integer (stała myszy ##030) lParam=Integer [0; klik myszą; 1 emulowany klik myszą przez aplikacje] Res=Integer [0 nie robi nic; 1 blokuje domyślną akcje)
  // Opis: Informuję o kliknięciu w ikonkę AQQ w zasobniku systemowym.
  AQQ_SYSTEM_TRAY_CLICK = 'AQQ/System/Tray/Click';
  // Notyfikacja: TAK wParam=PWideChar (Adres URL) lParam=0 Res=0
  // Opis: Informuję o kliknięciu przez użytkownika we wskazany w parametrze wParam link.
  AQQ_SYSTEM_LASTURL = 'AQQ/System/LastURL';
  // Funkcja: TAK wParam=PWideChar (Wyrażenie regularne) lParam=[0 dodawanie; 1 usuwanie] Res=[0 ok; 1 błąd]
  // Opis: Dodaje lub usuwa nowe wyrażenia regularne.
  AQQ_SYSTEM_REGEXP = 'AQQ/System/RegExp';
  // Funkcja: TAK wParam= lParam= Res=[]
  // Opis: Funkcja nie gotowa.
  AQQ_SYSTEM_FRAMESETTINGS = 'AQQ/System/FrameSettings';
  // Notyfikacja: TAK wParam=PPluginColorChange lParam=0 Res=0
  // Opis: Informacja o zmianie kolorystyki stylów obramowań okien. Notyfikacja przestarzała, proszę korzystać z AQQ_SYSTEM_COLORCHANGEV2
  // Funkcja: TAK wParam=PPluginColorChange lParam=0 Res=0
  // Opis: Trwale zmienia kolorystykę stylów obramowań (barwa i nasycenie). Funkcja przestarzała, proszę korzystać z AQQ_SYSTEM_COLORCHANGEV2
  AQQ_SYSTEM_COLORCHANGEV2 = 'AQQ/System/ColorChangeV2';
  // Notyfikacja: TAK wParam=Integer (Hue zakres 0-360) lParam=Integer (Saturation wartości od -100 do 100) Res=0
  // Opis: Informacja o zmianie kolorystyki stylów obramowań okien.
  // Funkcja: TAK wParam=Integer (Hue) lParam=Integer (Saturation) Res=0
  // Opis: Trwale zmienia kolorystykę stylów obramowań (barwa i nasycenie).
  AQQ_SYSTEM_COLORCHANGE = 'AQQ/System/ColorChange';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (Wartość Hue zakres 0-360)
  // Opis: Zwraca wartość aktualnej barwy stylu obramowań okien.
  AQQ_SYSTEM_COLORGETHUE = 'AQQ/System/ColorGetHue';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (Wartość Saturation zakres od -100 do 100)
  // Opis: Zwraca wartość aktualnego nasycenia barw stylu obramowań okien.
  AQQ_SYSTEM_COLORGETSATURATION = 'AQQ/System/ColorGetSaturation';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (Wartość Brightness zakres od -100 do 100)
  // Opis: Zwraca wartość jasności stylu obramowań okien.
  AQQ_SYSTEM_COLORGETBRIGHTNESS = 'AQQ/System/ColorGetBrightness';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (Aktualna wersja AQQ)
  // Opis: Zwraca aktualnie zainstalowaną wersję AQQ w formacie np. 2.5.0.12
  AQQ_SYSTEM_APPVER = 'AQQ/System/AppVer';
  // Notyfikacja: TAK wParam=Integer (Index ikony) lParam=0 Res=[0 bez zmian; Integer nowy index ikony]
  // Opis: Informacja o zmianie ikony w zasobniku systemowym. Wtyczka może zmodyfikować indeks.
  AQQ_SYSTEM_TRAYICONIMAGE = 'AQQ/System/TrayIconImage';
  // Notyfikacja: TAK wParam=PWideChar (Ścieżka do ikony) lParam=0 Res=[0 bez zmian; PWideChar nowa ścieżka do ikony 16x16 pixeli]
  // Opis: Informacja o zmianie ikony w zasobniku systemowym. Wtyczka może zmodyfikować ścieżkę.
  AQQ_SYSTEM_TRAYICONIMAGEPATH = 'AQQ/System/TrayIconImagePath';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Odświeża ikonkę stanu w zasobniku systemowym
  AQQ_SYSTEM_TRAYICONREFRESH = 'AQQ/System/TrayIconRefresh';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (informuje o nowej zmienionej lokalizacji - kod języka) Res=0
  // Opis: Notyfikacja wysyłana jedynie gdy użytkownik zmieni lokalizacje programu w ustawieniach AQQ.
  AQQ_SYSTEM_LANGCODE_CHANGED = 'AQQ/System/LangCodeChanged';

  /// Windows - okna
  // Notyfikacja: TAK wParam=0 lParam=0 Res=PWideChar (status tekstowy)
  // Opis: Przed pokazaniem się okna ze zmianą stanu, o ile dotyczy ono wtyczki sieciowej, powinna ona zwrócić aktualnie ustawiony opis - który zostanie umieszczony w otwartym oknie.
  AQQ_WINDOW_SETNOTE_PUTNOTE = 'AQQ/Window/SetNote/PutNote';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=PWideChar (JID)
  // Opis: Przed pokazaniem się okna ze zmianą stanu, o ile dotyczy ono wtyczki sieciowej, powinna ona zwrócić JID konta którego dotyczy przyszła zmiana stanu.
  AQQ_WINDOW_SETNOTE_NOTEJID = 'AQQ/Window/SetNote/NoteJID';
  // Notyfikacja: TAK wParam=PPluginWindowStatus lParam=0 Res=Integer (stała zmiany statusu w oknie ##011)
  // Opis: Przed zmianą stanu za pomocą okna zmiany stanu/opisu, wtyczka jest o tym informowana. Jej odpowiedź (jedna z dostępnych stałych) będzie miała wpływ na dalsze losy ustalania opisu.
  AQQ_WINDOW_SETSTATUS = 'AQQ/Window/SetStatus';
  // Notyfikacja: TAK wParam=Integer (stała stanu) lParam=PWideChar (status tekstowy) Res=[0 ok; 1 blokada zmiany stanu]
  // Opis: Przestarzała notyfikacja - nie należy stosować. Informuje o zmianie stanu (podaje nowy stan i opis).
  AQQ_WINDOW_PRESETNOTE_NOTE = 'AQQ/Window/PreSetNote/Note';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Okno ze zmianą stanu zostało zamknięte.
  AQQ_WINDOW_SETNOTE_CLOSE = 'AQQ/Window/SetNote/Close';
  // Funkcja: TAK wParam=THandle (uchwyt do okna) lParam=Integer (Zakres przeźroczystości 0-255) Res=0
  // Opis: Ustawia przeźroczystość wskazanego okna, również jego obramowania pochodzącego z AlphaControls.
  AQQ_WINDOW_TRANSPARENT = 'AQQ/Window/Transparent';

const
  /// Paski narzędziowe
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=-1
  // Opis: Funkcja wykorzystywana do tworzenia, niszczenia, i aktualizacji elementów znajdujących się na paskach narzędziowych.
        // Wiadomość AQQ_CONTROLS_TOOLBAR nigdy nie jest wysyłana tylko i wyłącznie we własnej przestrzeni nazw tj. "AQQ/Controls/Toolbar/".
        // Prawidłowe wysłanie wiadomości, następuje przy stworzeniu konkretnej  przestrzeni nazw, według podanego wzorca:
        // AQQ_CONTROLS_TOOLBAR + [Nazwa paska narzędziowego] + AQQ_CONTROLS_CREATEBUTTON
        // Tego typu wywołanie tworzy nowy element.
        // AQQ_CONTROLS_TOOLBAR + [Nazwa paska narzędziowego] + AQQ_CONTROLS_DESTROYBUTTON
        // Tego typu wywołanie niszczy element.
        // AQQ_CONTROLS_TOOLBAR + [Nazwa paska narzędziowego] + AQQ_CONTROLS_UPDATEBUTTON
        // Tego typu wywołanie aktualizuję element.
  AQQ_CONTROLS_TOOLBAR = 'AQQ/Controls/Toolbar/';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Część przestrzeni nazw wykorzystywanej przy funkcji AQQ_CONTROLS_TOOLBAR.
  AQQ_CONTROLS_CREATEBUTTON = '/CreateButton';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Część przestrzeni nazw wykorzystywanej przy funkcji AQQ_CONTROLS_TOOLBAR.
  AQQ_CONTROLS_DESTROYBUTTON = '/DestroyButton';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Część przestrzeni nazw wykorzystywanej przy funkcji AQQ_CONTROLS_TOOLBAR.
  AQQ_CONTROLS_UPDATEBUTTON = '/UpdateButton';

  /// Kontrolki IE
  // Funkcja: TAK wParam=[0 - okno główne X - uchwyt do okna rozmowy lub innego okna] lParam=PPluginWebBrowser Res=[0 błąd; Integer uchwyt do kontrolki IE]
  // Opis: Funkcja służy do tworzenia, lub aktualizacji kontrolki typu TWebBrowser.
  AQQ_CONTROLS_WEBBROWSER_CREATE = 'AQQ/Controls/WebBrowser/Create';
  // Funkcja: TAK wParam=PwideChar (adres URL) lParam=PPluginWebBrowser Res=[0 błąd; 1 ok]
  // Opis: Funkcja służy do nawigacji na nową stronę we wskazanej kontrolce TWebBrowser.
  AQQ_CONTROLS_WEBBROWSER_NAVIGATE = 'AQQ/Controls/WebBrowser/Navigate';
  // Notyfikacja: TAK wParam=0 lParam=Integer (uchwyt kontrolki IE) Res=0
  // Opis: Informuje o zakończonym wczytywaniu strony HTML.
  AQQ_CONTROLS_WEBBROWSER_NAVCOMPLETE = 'AQQ/Controls/WebBrowser/NavComplete';
  // Funkcja: TAK wParam=PPluginWebItem lParam=PPluginWebBrowser (pole handle ustawione na 1 wskazywać będzie na listę kontaktów) Res=[0 błąd; 1 ok]
  // Opis: Funkcja służy zmiany zawartości elementu o podanym ID w kodzie strony HTML.
  AQQ_CONTROLS_WEBBROWSER_SETID = 'AQQ/Controls/WebBrowser/SetID';
  // Funkcja: TAK wParam=PPluginWebItem lParam=PPluginWebBrowser (pole handle ustawione na 1 wskazywać będzie na listę kontaktów) Res=[0 błąd; 1 ok]
  // Opis: Funkcja pobiera zawartość elementu o podanym ID w kodzie strony HTML.
  AQQ_CONTROLS_WEBBROWSER_GETID = 'AQQ/Controls/WebBrowser/GetID';
  // Notyfikacja: TAK wParam=0 lParam=PPluginWebBeforeNavEvent Res=0
  // Opis: Informuje o rozpoczęciu wczytywania nowej strony HTML.
  AQQ_CONTROLS_WEBBROWSER_BEFORENAV = 'AQQ/Controls/WebBrowser/BeforeNav';
  // Notyfikacja: TAK wParam=PwideChar (nowy status) lParam=Integer (uchwyt kontrolki IE) Res=0
  // Opis: Informuje o zmianie statusu w oknie kontrolki IE.
  AQQ_CONTROLS_WEBBROWSER_STATUSCHANGE = 'AQQ/Controls/WebBrowser/StatusChange';
  // Notyfikacja: TAK wParam=PwideChar (nowy tytuł) lParam=Integer (uchwyt kontrolki IE) Res=0
  // Opis: Informuje o zmianie tytułu w oknie kontrolki IE.
  AQQ_CONTROLS_WEBBROWSER_TITLECHANGE = 'AQQ/Controls/WebBrowser/TitleChange';
  // Funkcja: TAK wParam=0 lParam=PPluginWebBrowser Res=[0 błąd; 1 ok]
  // Opis: Funkcja służy niszczenia wskazanej kontrolki typu TWebBrowser.
  AQQ_CONTROLS_WEBBROWSER_DESTROY = 'AQQ/Controls/WebBrowser/Destroy';
  // Funkcja: TAK wParam=PPluginWebItem (pole Text powinno być puste) lParam=PPluginWebBrowser Res=[0 błąd; 1 ok]
  // Opis: Funkcja służy do wywołania wirtualnego kliknięcia na element o wskazanym ID.
  AQQ_CONTROLS_WEBBROWSER_CLICKID = 'AQQ/Controls/WebBrowser/ClickID';

  /// Menu
  // Funkcja: TAK wParam=0 lParam=PPluginItemDescriber Res=[0 błąd; PPluginAction]
  // Opis: Funkcja służy do pobierania danych na temat elementu znajdującego się na wskazanym komponencie PopUp (menu).
  AQQ_CONTROLS_GETPOPUPMENUITEM = 'AQQ/Controls/GetPopupMenuItem';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=0
  // Opis: Funkcja służy do tworzenia nowego menu (typu PopUp).
  AQQ_CONTROLS_EDITPOPUPMENUITEM = 'AQQ/Controls/EditPopUpMenuItem';
  // Funkcja: TAK wParam=0 lParam=PPluginActionEdit Res=0
  // Opis: Funkcja służy do uaktualnienia stanu elementu w menu (typu PopUp).
  AQQ_CONTROLS_CREATEPOPUPMENU = 'AQQ/Controls/CreatePopUpMenu';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=[0 błąd; 1 ok]
  // Opis: Funkcja służy do tworzenia nowego elementu na wskazanym menu typu PopUp.
  AQQ_CONTROLS_CREATEPOPUPMENUITEM = 'AQQ/Controls/CreatePopUpMenuItem';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=0
  // Opis: Funkcja służy do niszczenia wskazanego menu (typu PopUp).
  AQQ_CONTROLS_DESTROYPOPUPMENU = 'AQQ/Controls/DestroyPopUpMenu';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=0
  // Opis: Funkcja służy do niszczenia wskazanego elementu w menu typu PopUp.
  AQQ_CONTROLS_DESTROYPOPUPMENUITEM = 'AQQ/Controls/DestroyPopUpMenuItem';

  /// Inne kontrolki
  // Funkcja: TAK wParam=0 lParam=PwideChar (nowy status) Res=0
  // Opis: Funkcja zmienia status na dolnym panelu w głównym oknie rozmowy.
  AQQ_CONTROLS_MAINSTATUS_SETPANELTEXT = 'AQQ/Controls/MainStatus/SetPanelText';
  // Funkcja: TAK wParam=0 lParam=[0 wyłącz; 1 włącz] Res=0
  // Opis: Włącza lub wyłącza elementy w oknie głównym powiązane z zakładką SMS.
  AQQ_CONTROLS_SMSCONTROLS_ENABLE = 'AQQ/Controls/SMSControls/Enable';
  // Funkcja: TAK wParam=PPluginForm lParam=0 Res=[0 błąd; Integer uchwyt do stowrzonego okna]
  // Opis: Tworzy nowe okno dialogowe.
  AQQ_CONTROLS_FORM_CREATE = 'AQQ/Controls/Form/Create';
  // Funkcja: TAK wParam=Integer (uchwyt do okna) lParam=0 Res=0
  // Opis: Niszczy wskazane okno dialogowe.
  AQQ_CONTROLS_FORM_DESTROY = 'AQQ/Controls/Form/Destroy';
  // Funkcja: TAK wParam=Integer (uchwyt do okna) lParam=[0 ukryj; 1 pokaż; 2 pokaż modalnie] Res=0
  // Opis: Zmienisa stan widoczności okna.
  AQQ_CONTROLS_FORM_VISIBLE = 'AQQ/Controls/Form/Destroy';
  // Funkcja: TAK wParam=PPluginControl lParam=Integer (stała akcji ##031) Res=[0 błąd; 1 ok; Integer uchwyt do kontrolki jeżeli jest; PPluginControl jeżeli korzystamy z CONTROL_ACTION_GET]
  // Opis: Zmienia stan widoczności okna.
  AQQ_CONTROLS_FORM_CONTROL = 'AQQ/Controls/Form/Control';
  // Funkcja: TAK wParam=Integer (uchwyt do okna) lParam=0 Res=Integer biężący status modalny ##032
  // Opis: Pobiera wynik modalny okna dialogowego.
  AQQ_CONTROLS_FORM_GETMODALRESULT = 'AQQ/Controls/Form/GetModalResult';

  /// Kontakty
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje o zakończeniu ładowania listy kontaktów przy starcie AQQ.
  AQQ_CONTACTS_LISTREADY = 'AQQ/Contacts/ListReady';
  // Notyfikacja: TAK wParam=PwideChar (jid) lParam=0 Res=[0 nie; 1 tak]
  // Opis: AQQ odpytuje, czy wskazany JID pochodzi z którejś wtyczki sieciowej.
  AQQ_CONTACTS_FROMPLUGIN = 'AQQ/Contacts/FromPlugin';
  // Funkcja: TAK wParam=0 lParam=PPluginContactSimpleInfo Res=[0 brak kontaktu; 1 dane uzupełnione z pliku; 2 dane uzupełnione]
  // Opis: Funkcja uzupełnia w podanej strukturze dane na temat wskazanego kontaktu (JID).
  AQQ_CONTACTS_FILLSIMPLEINFO = 'AQQ/Contacts/FillSimpleInfo';
  // Funkcja: TAK wParam=0 lParam=PPluginContactSimpleInfo Res=0
  // Opis: Funkcja zmienia dane na temat wskazanego kontaktu (JID).
  AQQ_CONTACTS_SET_SIMPLEINFO = 'AQQ/Contacts/Set/SimpleInfo';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Zwraca dane na temat wskazanego kontaktu.
  // Funkcja: TAK wParam=Integer (ID konta) lParam=PWideChar (jid) Res=[0 brak błędu; 1 błąd, dane nie zostaną zwrócone]
  // Opis: Pobiera dane na temat wskazanego kontaktu.
  AQQ_CONTACTS_REQUESTBUDDY = 'AQQ/Contacts/RequestBuddy';
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=PPluginContactSimpleInfo
  // Opis: Funkcja wyparta przez AQQ_CONTACTS_FILLSIMPLEINFO. Pobiera dane na temat wskazanego kontaktu.
  AQQ_CONTACTS_GET_SIMPLEINFO = 'AQQ/Contacts/Get/SimpleInfo';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Informuje wtyczkę o dodaniu nowego bana dla wskazanego JID-u.
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Dodaję JID do listy zbanowanych kontaktów.
  AQQ_CONTACTS_ADD_BAN = 'AQQ/Contacts/Add/Ban';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Informuje wtyczkę o usunięciu bana dla wskazanego JID-u.
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Usuwa JID z listy zbanowanych kontaktów.
  AQQ_CONTACTS_REMOVE_BAN = 'AQQ/Contacts/Remove/Ban';
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=[0 kontakt bez bana; 1 kontakt ma bana]
  // Opis: Sprawdza czy dany JID jest na liście zablokowanych.
  AQQ_CONTACTS_HAVE_BAN = 'AQQ/Contacts/Have/Ban';
  // Notyfikacja: TAK wParam=0 lParam=PPluginAddUser Res=[0 jeżeli nie dotyczy wtyczki; 1 jeżeli wtyczka dodała kontakt]
  // Opis: Informuje wtyczkę o dodaniu nowego kontaktu.
  AQQ_CONTACTS_ADD = 'AQQ/Contacts/Add';
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Funkcja dodaję nowy kontakt do listy kontaktów.
  AQQ_CONTACTS_CREATE = 'AQQ/Contacts/Create';
  // Funkcja: TAK wParam=0 lParam=PPluginAddForm Res=0
  // Opis: Funkcja uruchamia okno dodawania nowego kontaktu. Można wskazać agenta który ma być domyślnie użyty.
  AQQ_CONTACTS_ADDFORM = 'AQQ/Contacts/AddForm';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 nie dotyczy; 1 wtyczka zmieniła przypisane grupy; 2 blokada]
  // Opis: Aktualizacja grup do których przynależy kontakt.
  AQQ_CONTACTS_UPDATEGROUPS = 'AQQ/Contacts/UpdateGroups';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 nie dotyczy; 1 kontakt usunięto; 2 blokada]
  // Opis: Informuje wtyczkę o usunięciu danego kontaktu.
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Usuwa kontakt z listy kontaktów.
  AQQ_CONTACTS_DELETE = 'AQQ/Contacts/Delete';
  // Funkcja: TAK wParam=0 lParam=PwideChar (domena np. @aqq.eu) Res=0
  // Opis: Usuwa z widocznej listy kontaktów kontakty ze wskazanej domeny.
  AQQ_CONTACTS_DESTROYDOMAIN = 'AQQ/Contacts/DestroyDomain';
  // Notyfikacja: TAK wParam=PPluginContact lParam=Integer [stała typu zmiany statusu np. CONTACT_UPDATE_NORMAL ##015] Res=0
  // Opis: Informuje wtyczkę o aktualizacji kontaktu. Wtyczka może zmodyfikować niektóre pola PPluginContact (nickname, showtype, status).
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Aktualizacja wskazanego kontaktu.
  AQQ_CONTACTS_UPDATE = 'AQQ/Contacts/Update';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PWideChar [HTML kontaktu] Res=[0 bez zmian; 1 zaszły zmiany]
  // Opis: Informuje wtyczkę tworzeniu wyglądu kontaktu na liście. Podana treść HTML może zostać zmodyfikowana.
  AQQ_CONTACTS_BUDDYFILL = 'AQQ/Contacts/BuddyFill';
  // Funkcja: TAK wParam=Integer (0 domyślnie; Indeks konta) lParam=PPluginMessage Res=0
  // Opis: Wtyczka sieciowa może poinformować o nowej wiadomośc i przychodzącej.
  AQQ_CONTACTS_MESSAGE = 'AQQ/Contacts/Message';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 bez zmian; 1 blokada 2; modyfikacja]
  // Opis: Informuje wtyczkę, że za moment zostanie wysłana wskazana wiadomość tekstowa do kontaktu.
  AQQ_CONTACTS_PRESENDMSG = 'AQQ/Contacts/PreSendMsg';
  // Funkcja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 domyślnie; Integer ID wiadomości > 100]
  // Opis: Wysyła wiadomość tekstową do wskazanego kontaktu.
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 nie dotyczy; 1 wiadomość wysłana; 2 wiadomość nie może być dostarczona]
  // Opis: Informuję o chęci wysłania wiadomości tekstowej. Odpowiedzialana za to wtyczka powinną ją wysłać.
  AQQ_CONTACTS_SENDMSG = 'AQQ/Contacts/SendMsg';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 bez zmian; 1 blokada 2; modyfikacja]
  // Opis: Informuję o otrzymaniu przez AQQ nowej wiadomości tekstowej.
  AQQ_CONTACTS_RECVMSG = 'AQQ/Contacts/RecvMsg';
  // Funkcja: TAK wParam=Integer [AckId] lParam=Integer [stała typu stanu doręczenia SMS ##020] Res=0
  // Opis: Stan wiadomości o podanym ID został zmieniony np. wiadomość została doręczona
  AQQ_CONTACTS_ACKMSG = 'AQQ/Contacts/AckMsg';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 bez zmian; 1 blokada 2; modyfikacja]
  // Opis: Informuję o dodawaniu nowej linii do okna rozmowy (kod HTML). Wtyczka może ją zmodyfikować. Jeżeli wstawiany jest jakikolwiek odnośnik URL należy go poprzedzić prefixem http://aqq-link/?url=
  AQQ_CONTACTS_ADDLINE = 'AQQ/Contacts/AddLine';
  // Funkcja: TAK wParam=PPluginContact lParam=PPluginMicroMsg Res=[0 - ok; 1 - brak otwartej rozmowy ze wskazanym kontaktem]
  // Opis: Pozwala dodać informacje HTML do okna rozmowy z kontaktem.
  AQQ_CONTACTS_ADDLINEINFO = 'AQQ/Contacts/AddLineInfo';
  // Funkcja: TAK wParam=Integer (ID wywołania, proszę generować przy pomocy systemowej funkcji WinAPI GetTickCount) lParam=0 Res=0
  // Opis: Funkcja wywołuje enumeracje listy kontaktów. Kolejne kontakty są wysyłane przy pomocy notyfikacji AQQ_CONTACTS_REPLYLIST.
  AQQ_CONTACTS_REQUESTLIST = 'AQQ/Contacts/RequestList';
  // Notyfikacja: TAK wParam=Integer (ID wywołania) lParam=PPluginContact Res=0
  // Opis: Notyfikacja wywoływana jest raz dla każdego kontaktu dostępnego na liście. Gdy enumeracja dobiegnie końca, AQQ poinfrmuje o tym za pomocą notyfikacji AQQ_CONTACTS_REPLYLISTEND.
  AQQ_CONTACTS_REPLYLIST = 'AQQ/Contacts/ReplyList';
  // Notyfikacja: TAK wParam=Integer (ID wywołania) lParam=0 Res=0
  // Opis: Informuje, że enumeracja kontaktów (o wskazanym ID) dobiegła końca.
  AQQ_CONTACTS_REPLYLISTEND = 'AQQ/Contacts/ReplyListEnd';
  // Funkcja: TAK wParam=PPluginContact lParam=PWideChar (sformatowana treść HTML aktualnego opisu) Res=0
  // Opis: Pozwala zmienić opis HTML na liście wskazanego kontaktu w dowolnym momencie.
  // Notyfikacja: TAK wParam=PPluginContact lParam=PWideChar (sformatowana treść HTML aktualnego opisu) Res=[0 bez zmian; PWideChar Zmodyfikowany status zgodny z HTML]
  // Opis: Notyfikacja pozwala podmienić wygląd statusu wskazanego kontaktu na liście. Modyfikowana jest treść HTML dlatego należy zachować szczególną ostrożność.
  AQQ_CONTACTS_SETHTMLSTATUS = 'AQQ/Contacts/SetHTMLStatus';
  // Funkcja: TAK wParam=PPluginContact lParam=0) Res=[0 błąd; PWideChar sformatowany aktualny status HTML użytkownika na liście]
  // Opis: Funkcja pozwala pobrać wygląd statusu wskazanego kontaktu na liście w formie HTML.
  AQQ_CONTACTS_GETHTMLSTATUS = 'AQQ/Contacts/GetHTMLStatus';
  // Funkcja: TAK wParam=PPluginContact lParam=PWideChar (ścieżka do pliku) Res=[0 błąd; 1 ok]
  // Opis: Rozpoczyna wysyłkę obrazka do wskazanego kontaktu. Musi on istnieć w otwartym oknie rozmowy.
  // Notyfikacja: TAK wParam=PPluginMsgPic lParam=PPluginContact Res=[0 domyślnie; -1 blokada ukrytych transferów]
  // Opis: Informuję o rozpoczęciu wysyłki obrazka do wskazanego kontaktu.
  AQQ_CONTACTS_SENDPIC = 'AQQ/Contacts/SendPic';
  // Notyfikacja: TAK wParam=Integer (wielkość pliku w bajtach) lParam=PPluginContact Res=[0 domyślnie; -1 kontakt nie połączony wysyłka zabroniona; -2 kontakt połączony ale nie obsługuję wysyłka zabroniona; Integer max. wielkość pliku w KB]
  // Opis: Informuję o rozpoczęciu wysyłki obrazka do wskazanego kontaktu.
  AQQ_CONTACTS_SENDPIC_SIZECHECK = 'AQQ/Contacts/SendPic/SizeCheck';
  // Notyfikacja: TAK wParam=PPluginFileTransfer lParam=PPluginContact Res=[0 domyślnie; Integer uchwyt wtyczki która odpowiada za transfer]
  // Opis: Informuję o rozpoczęciu wysyłki pliku do wskazanego kontaktu.
  AQQ_CONTACTS_SENDFILE = 'AQQ/Contacts/SendFile';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 domyślnie; PWideChar ścieżka do pliku]
  // Opis: AQQ prosi o ścieżkę do pliku graficznego, który odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPE_PATH = 'AQQ/Contacts/IconShowType/Path';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 domyślnie; Integer index grafiki]
  // Opis: AQQ prosi o index grafiki, która odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPE_INDEX = 'AQQ/Contacts/IconShowType/Index';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID) Res=[0 domyślnie; Integer index grafiki]
  // Opis: AQQ prosi o index grafiki, która odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPEJID_INDEX = 'AQQ/Contacts/IconShowTypeJID/Index';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 domyślnie; Integer index grafiki HD]
  // Opis: AQQ prosi o index grafiki HD, która odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPE_HDINDEX = 'AQQ/Contacts/IconShowType/HDIndex';
  // Funkcja: TAK wParam=0 lParam=PWideChar (domena np. @aqq.eu) Res=0
  // Opis: Ustawia kontakty we wskazanej domenie w trybie offline (rozłączone).
  AQQ_CONTACTS_SETOFFLINE = 'AQQ/Contacts/SetOffline';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: AQQ prosi o przestawienie wskazanych kontaktów w tryb offline (rozłączony).
  AQQ_CONTACTS_OFFLINE = 'AQQ/Contacts/Offline';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID agenta) Res=[0 nie dotyczy; XML]
  // Opis: AQQ prosi o podanie przez wtyczkę sieciową, opisu wyszukiwarki według standardu http://xmpp.org/extensions/xep-0055.html
  AQQ_CONTACTS_GETSEARCHXML = 'AQQ/Contacts/GetSearchXML';
  // Notyfikacja: TAK wParam=PWideChar (wypełniony XML) lParam=PWideChar (JID agenta) Res=[0 nie dotyczy; 1 ok]
  // Opis: Użytkownik szuka znajomych a AQQ zwraca do wtyczki wypełniony XML według standardu XEP-0055 - dla ponadego agenta.
  AQQ_CONTACTS_SETSEARCHXML = 'AQQ/Contacts/SetSearchXML';
  // Funkcja: TAK wParam=Integer (ID sesji wyszukiwania) lParam=PwideChar (xml) Res=0
  // Opis: Wtyczka sieciowa zwraca wyniki wyszukiwania kontaktów (według standardu XEP-0055).
  AQQ_CONTACTS_RESSEARCHXML = 'AQQ/Contacts/ResSearchXML';
  // Funkcja: TAK wParam=PwideChar (xml) lParam=0 Res=0
  // Opis: Wtyczka sieciowa zwraca błąd wynikły w trakcie wyszukiwania kontaktów (według standardu XEP-0055).
  AQQ_CONTACTS_ERRSEARCHXML = 'AQQ/Contacts/ErrSearchXML';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar ID Res=0
  // Opis: Przed rozpoczęciem wyszukiwania, AQQ nadaje mu nowe ID. I informuje o nim wtyczkę sieciową.
  AQQ_CONTACTS_LASTSEARCHID = 'AQQ/Contacts/LastSearchID';
  // Notyfikacja: TAK wParam=PWideChar (nowy pseudonim) lParam=PPluginContact ID Res=0
  // Opis: AQQ informuje o zmianie pseudonimu kontaktu. Wtyczka do której przynależy kontakt powinna uaktualnić pseudonim.
  AQQ_CONTACTS_CHANGENAME = 'AQQ/Contacts/ChangeName';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTwoFlagParams (Param1 nazwa grupy; Param2 nowa nazwa; Flag1 ID konta) Res=0
  // Opis: AQQ informuje o zmianie nazwy grupy. Wtyczka do której przynależy grupa powinna się dostosować do zmian.
  AQQ_CONTACTS_CHANGEGROUPNAME = 'AQQ/Contacts/ChangeGroupName';
  // Notyfikacja: TAK wParam=PWideChar (ID) lParam=PPluginContact Res=[0 nie dotyczy; 1 ok]
  // Opis: AQQ prosi o pobranie wizytówki kontaktu z serwera, przy zwrocie danych należy uwzględnić ID podane przez AQQ.
  AQQ_CONTACTS_GETVCARD = 'AQQ/Contacts/GetVCard';
  // Funkcja: TAK wParam=PwideChar (ID) lParam=PWideChar (xml) Res=0
  // Opis: Wtyczka sieciowa zwraca wizytówkę kontaktu w standardzie XEP-0054: vcard-temp
  AQQ_CONTACTS_RESVCARD = 'AQQ/Contacts/ResVCard';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie zaznaczonym kontakcie na liście.
  AQQ_CONTACTS_BUDDY_SELECTED = 'AQQ/Contacts/Buddy/Selected';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie wybranej zakładce w oknie rozmowy.
  AQQ_CONTACTS_BUDDY_ACTIVETAB = 'AQQ/Contacts/Buddy/ActiveTab';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie zamykanej zakładce w oknie rozmowy.
  // Funkcja: TAK wParam=[0 domyślnie; 1 kontakt pochodzi z wtyczki] lParam=PWideChar (JID kontaktu) Res=0
  // Opis: Zamyka wskazaną zakładkę z kontaktem w oknie rozmowy.
  AQQ_CONTACTS_BUDDY_CLOSETAB = 'AQQ/Contacts/Buddy/CloseTab';
  // Notyfikacja: TAK wParam=PWideChar (wiadomość) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie zamykanej zakładce w oknie rozmowy - i o nie wysłanej wiadomości której użytkownik nie wysłał. Jeżeli wiadomość jest pusta - notyfikacja nie wystąpi.
  AQQ_CONTACTS_BUDDY_CLOSETABMESSAGE = 'AQQ/Contacts/Buddy/CloseTabMessage';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie aktywnym oknie, i jaki kontakt w tym oknie jest aktywny obecnie.
  AQQ_CONTACTS_BUDDY_FORMACTIVATE = 'AQQ/Contacts/Buddy/FormActive';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Rozpoczyna wysyłanie notyfikacji na temat otwartych zakładek w oknie rozmowy.
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informuje, że taka zakładka w oknie rozmowy jest otwarta. Wymaga wcześniejszego użycia funkcji AQQ_CONTACTS_BUDDY_FETCHALLTABS.
  AQQ_CONTACTS_BUDDY_FETCHALLTABS = 'AQQ/Contacts/Buddy/FetchAllTabs';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informuje, że taka zakładka w oknie rozmowy jest otwarta i jest aktywna. Wymaga wcześniejszego użycia funkcji AQQ_CONTACTS_BUDDY_FETCHALLTABS.
  AQQ_CONTACTS_BUDDY_PRIMARYTAB = 'AQQ/Contacts/Buddy/PrimaryTab';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informuje, że taka zakładka w oknie rozmowy jest otwarta, i jest to czat. Wymaga wcześniejszego użycia funkcji AQQ_CONTACTS_BUDDY_FETCHALLTABS.
  AQQ_CONTACTS_BUDDY_CONFERENCETAB = 'AQQ/Contacts/Buddy/ConferenceTab';
  // Funkcja: TAK wParam=PWideChar (nowa etykieta zakładki) lParam=PPluginContact Res=0
  // Opis: Zmienia etykiete zakładki w otwartym oknie rozmowy.
  // Notyfikacja: TAK wParam=PWideChar (nowa etykieta zakładki) lParam=PPluginContact Res=[0 ok; PWideChar zmiana nazwy]
  // Opis: Informuje o zmianie etykiety zakładki w otwartym oknie rozmowy.
  AQQ_CONTACTS_BUDDY_TABCAPTION = 'AQQ/Contacts/Buddy/TabCaption';
  // Funkcja: TAK wParam=Integer (indeks nowej ikony) lParam=PPluginContact Res=0
  // Opis: Zmienia ikonkę zakładki w otwartym oknie rozmowy.
  // Notyfikacja: TAK wParam=Integer (nowy indeks ikony) lParam=PPluginContact Res=[0 ok; Integer nowa indeks ikony]
  // Opis: Informuje o zmianie ikony zakładki w otwartym oknie rozmowy.
  AQQ_CONTACTS_BUDDY_TABIMAGE = 'AQQ/Contacts/Buddy/TabImage';
  // Funkcja: TAK wParam=PWideChar (URL) lParam=PPluginContact Res=0
  // Opis: Pobiera awatara z podanego adres URL, i ustawia go dla wskazanego kontaktu.
  AQQ_CONTACTS_SETWEB_AVATAR = 'AQQ/Contacts/SetWeb/Avatar';
  // Funkcja: TAK wParam=PPluginAvatar lParam=PPluginContact Res=0
  // Opis: Ustawia nowy awatar dla wskazanego kontaktu.
  AQQ_CONTACTS_SET_AVATAR = 'AQQ/Contacts/Set/Avatar';
  // Notyfikacja: TAK wParam=PPluginContact lParam=0 Res=[0 domyślnie; PWideChar stanu kontaktu w formie tekstowej]
  // Opis: AQQ prosi o podanie stanu kontaktu w formie tekstowej dla podanego kontaktu (np. Połączony).
  AQQ_CONTACTS_STATUSCAPTION = 'AQQ/Contacts/StatusCaption';
  // Notyfikacja: TAK wParam=PPluginContact lParam=1 Res=0
  // Opis: AQQ prosi o zresetowanie awatara dla wskazanego kontaktu (należy go pobrać od nowa).
  AQQ_CONTACTS_RESETAVATAR = 'AQQ/Contacts/ResetAvatar';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 domyślnie; 1 blokada lub własna obsługa zdarzenia]
  // Opis: Wskazany kontakt prosi użytkownika o uwagę.
  AQQ_CONTACTS_ATTENTION = 'AQQ/Contacts/Attention';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=[0 domyślnie; 1 blokada lub własna obsługa zdarzenia]
  // Opis: AQQ pyta wtyczkę do której przynależy kontakt, czy podany JID jest zgodny ze standardami sieci którą ta wtyczka obsługuję (np. same liczby)
  AQQ_CONTACTS_VALIDATEJID = 'AQQ/Contacts/ValidateJID';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Rozpoczyna wysyłanie notyfikacji na temat zaznaczonych kontatów na głównej liście.
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Informuje, że wskazany kontakt jest zaznaczony na liście kontaktów. Wymaga wcześniejszego użycia funkcji AQQ_CONTACTS_BUDDY_FETCHSELECTED.
  AQQ_CONTACTS_BUDDY_FETCHSELECTED = 'AQQ/Contacts/Buddy/FetchSelected';
  // Notyfikacja: TAK wParam=Integer (0 wyłącz; 1 włącz) lParam=PWideChar (jid) Res=0
  // Opis: Informuje wtyczkę, że użytkownik chcę być niewidoczny dla wybranego kontaktu.
  // Funkcja: TAK wParam=Integer (0 wyłącz; 1 włącz) lParam=PWideChar (jid) Res=0
  // Opis: Wtyczka przełącza stan widzialności użytkownika dla danego kontaktu.
  AQQ_CONTACTS_SETINVISIBLE = 'AQQ/Contacts/SetInvisible';
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=[0 nieiwdoczność wyłączona; 1 niewidoczność włączona]
  // Opis: Sprawdza czy danego JID-u użytkownik chcę być niewidoczny.
  AQQ_CONTACTS_GETINVISIBLE = 'AQQ/Contacts/GetInvisible';

  /// Ikony
  // Funkcja: TAK wParam=0 lParam=PWideChar (ścieżka do ikony) Res=[-1 błąd; Integer index dodanej ikony]
  // Opis: Dodaje nową ikonę do zasobów AQQ. Ikona powinna być 32 bitowa + kanał alpha i w rozmiarach 16x16.
  AQQ_ICONS_LOADPNGICON = 'AQQ/Icons/LoadPNGIcon';
  // Funkcja: TAK wParam=Integer index ikony do zmiany lParam=PWideChar (ścieżka do ikony) Res=[-1 błąd; Integer index zmienionej ikony]
  // Opis: Dodaje nową ikonę do zasobów AQQ. Ikona powinna być 32 bitowa + kanał alpha i w rozmiarach 16x16.
  AQQ_ICONS_REPLACEPNGICON = 'AQQ/Icons/ReplacePNGIcon';
  // Funkcja: TAK wParam=0 lParam=Integer (index ikony do usunięcia) Res=0
  // Opis: Usuwa ikonę o podanym indexie z zasobów AQQ. Wtyczka powinna usuwać indexy które sama dodała wcześniej.
  AQQ_ICONS_DESTROYPNGICON = 'AQQ/Icons/DestroyPNGIcon';

  //// Funkcje
  // Funkcja: TAK wParam=0 lParam=0 Res=[0 nie; 1 tak]
  // Opis: Stwierdza, czy lista kontaktów jest gotowa na wykonywanie jakichkolwiek operacji np. dodawania kontaktów.
  AQQ_FUNCTION_ISLISTREADY = 'AQQ/Function/IsListReady';
  // Funkcja: TAK wParam=Integer (uchwyt wtyczki) lParam=0 Res=[0; błąd PWideChar ścieżka do katalogu wtyczki]
  // Opis: Zwraca katalog w którym znajduję się wtyczka o podanym uchwycie (hInstance).
  AQQ_FUNCTION_GETPLUGINDIR = 'AQQ/Function/GetPluginDir';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ścieżka)
  // Opis: Zwraca ścieżkę do profilu zalogowanego w danej chwili użytkownika.
  AQQ_FUNCTION_GETUSERDIR = 'AQQ/Function/GetUserDir';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ścieżka)
  // Opis: Zwraca ścieżkę do katalogu zawierającego wtyczki przypisane do zalogowanego w danej chwili użytkownika.
  AQQ_FUNCTION_GETPLUGINUSERDIR = 'AQQ/Function/GetPluginUserDir';
  // Funkcja: TAK wParam=0 lParam=PWideChar (stała tekstowa) Res=PWideChar (tekst)
  // Opis: Podając stała tekstową wybraną z pliku Const.lng, otrzymamy jej wartośc tekstową - użyty będzie język z którego korzysta zalogowany użytkownik.
  AQQ_FUNCTION_GETLANGSTR = 'AQQ/Function/GetLangStr';
  // Funkcja: TAK wParam=PWideChar (adres url) lParam=0 Res=PWideChar (token)
  // Opis: Funkcja zwraca przepisany przez użytkownika Token, pochodzący ze strony webowej wskazanej w parametrze wParam.
  AQQ_FUNCTION_GETTOKEN = 'AQQ/Function/GetToken';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ID)
  // Opis: Funkcja zwraca unikalne (na czas trwania sesji AQQ) ID tekstowe.
  AQQ_FUNCTION_GETSTRID = 'AQQ/Function/GetStrID';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (ID)
  // Opis: Funkcja zwraca unikalne (na czas trwania sesji AQQ) ID numeryczne.
  AQQ_FUNCTION_GETNUMID = 'AQQ/Function/GetNumID';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (JID/UID)
  // Opis: Funkcja zwraca JID/UID zalogowanego w AQQ użytkownika (główne konto).
  AQQ_FUNCTION_GETUSERUID = 'AQQ/Function/GetUserUID';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ścieżka)
  // Opis: Funkcja zwraca ścieżkę do katalogu kompozycji w której wtyczka będzie miała prawa zapisu. Jeżeli taki katalog jest nie dostępny - zwrócony zostanie pusty ciąg znaków!
  AQQ_FUNCTION_GETTHEMEDIRRW = 'AQQ/Function/GetThemeDirRW';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ścieżka)
  // Opis: Funkcja zwraca ścieżkę do katalogu używanej aktualnie kompozycji wizualnej.
  AQQ_FUNCTION_GETTHEMEDIR = 'AQQ/Function/GetThemeDir';
  // Funkcja: TAK wParam=0 lParam=0 Res=PPluginProxy
  // Opis: Funkcja zwraca aktualne ustawienia Proxy w AQQ.
  AQQ_FUNCTION_GETPROXY = 'AQQ/Function/GetProxy';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ścieżka)
  // Opis: Funkcja zwraca ścieżkę do katalogu instalacyjnego AQQ.
  AQQ_FUNCTION_GETAPPPATH = 'AQQ/Function/GetAppPath';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ścieżka)
  // Opis: Funkcja zwraca ścieżkę do pliku wykonywalnego AQQ (plik który został użyty do rozruchu aplikacji).
  AQQ_FUNCTION_GETAPPFILEPATH = 'AQQ/Function/GetAppFilePath';
  // Funkcja: TAK wParam=[0 domyślnie; 1 kontakt pochodzi z wtyczki; 2 kontakt to konferencja] lParam=PWideChar (JID) Res=0
  // Opis: Funkcja otwiera okno rozmowy ze wskazanym kontaktem. Okno rozmowy zostaje przywołane na pierwszy plan. wParam może być rózwne też indexowi konta, wówczas JID należy podawać z zasobem.
  AQQ_FUNCTION_EXECUTEMSG = 'AQQ/Function/ExecuteMsg';
  // Funkcja: TAK wParam=[0 domyślnie; 1 kontakt pochodzi z wtyczki; 2 kontakt to konferencja] lParam=PWideChar (JID) Res=0
  // Opis: Funkcja otwiera okno rozmowy ze wskazanym kontaktem. Stan okna rozmowy nie zmienia się (brak przywołania na pierwszy plan). wParam może być rózwne też indexowi konta, wówczas JID należy podawać z zasobem.
  AQQ_FUNCTION_EXECUTEMSG_NOPRIORITY = 'AQQ/Function/ExecuteMsg/NoPriority';
  // Funkcja: TAK wParam=[0 domyślnie] lParam=PPluginExecMsg Res=[-1 błąd; 0 ok; Integer numer zakładki]
  // Opis: Otwiera lub zamyka lub przywołuje okno rozmowy ze wskazanym kontaktem lub dodaje kontakt do listy ost. zamkniętych zakładek. Proszę korzystać z jednej akcji na jedno wywołanie funkcji.
  AQQ_FUNCTION_MSGWINDOW = 'AQQ/Function/Msgwindow';
  // Funkcja: TAK wParam=[0 domyślnie; 1 kontakt pochodzi z wtyczki] lParam=PWideChar (JID) Res=0
  // Opis: Funkcja dodaje kontakt do listy zamkniętych ostatnio zakładek. Lista ta wykorzystywana jest m.inn. w dymku ikony AQQ w zasobniku systemowym.
  AQQ_FUNCTION_TABWASCLOSED = 'AQQ/Function/TabWasClosed';
  // Funkcja: TAK wParam=PPluginTriple (Handle1 uchwyt okna; Param1 index zakładki do zmiany; Param2 nowy index) lParam=0 Res=[0 błąd; 1 ok]
  // Opis: Funkcja zamienia miejscami dwie wybrane zakładki.
  AQQ_FUNCTION_TABMOVE = 'AQQ/Function/TabMove';
  // Funkcja: TAK wParam=PPluginTriple (Handle1 uchwyt okna) lParam=0 Res=[0 błąd; Integer ilość otwartych zakładek]
  // Opis: Funkcja zwraca ilość otwartych zakładek we wskazanym oknie rozmowy.
  AQQ_FUNCTION_TABCOUNT = 'AQQ/Function/TabCount';
  // Funkcja: TAK wParam=[0 domyślnie; 1 kontakt pochodzi z wtyczki] lParam=PWideChar (JID) Res=[-1 błąd; Integer index]
  // Opis: Funkcja zwraca index zakładki dla wskazanego kontaktu.
  AQQ_FUNCTION_TABINDEX = 'AQQ/Function/TabIndex';
  // Funkcja: TAK wParam=0 lParam=PWideChar (adres url) Res=0
  // Opis: Funkcja otwiera domyślną przeglądarkę internetową, i przechodzi pod adres wskazany przez wtyczkę.
  AQQ_FUNCTION_OPENURL = 'AQQ/Function/OpenURL';
  // Funkcja: TAK wParam=0 lParam=PWideChar (adres url do filmu YT) Res=0
  // Opis: Funkcja otwiera odtwarzacz filmów YT i odtwarza wskazany materiał filmowy.
  AQQ_FUNCTION_OPENYTURL = 'AQQ/Function/OpenYTURL';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (adres zew. IP)
  // Opis: Funkcja zwraca zewnętrzny adres IP użytkownika. Aby został zwrócony, AQQ musi skutecznie pobrać te dane przy starcie aplikacji.
  AQQ_FUNCTION_GETEXTERNALIP = 'AQQ/Function/GetExternalIP';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (liczba kont)
  // Opis: Funkcja zwraca liczbę kont aktywnych na zalogowanym profilu użytkownika.
  AQQ_FUNCTION_GETUSEREXCOUNT = 'AQQ/Function/GetUserExCount';
  // Funkcja: TAK wParam=0 lParam=Integer (ID konta) Res=PPluginStateChange
  // Opis: Funkcja zastąpiona przez AQQ_FUNCTION_GETNETWORKSTATE. Nie stosować. Zwraca informacje na temat stanu wskazanego konta użytkownika.
  AQQ_FUNCTION_GETNETWORKINFO = 'AQQ/Function/GetNetworkInfo';
  // Funkcja: TAK wParam=PPluginStateChange (wyzerowana struktura) lParam=Integer (ID konta) Res=[0 błąd; 1 konto; 2; konto pochodzące z wtyczki]
  // Opis: Zwraca informacje na temat stanu wskazanego konta użytkownika. AQQ wypełnia strukturę podaną przez wtyczkę w parametrze wParam.
  AQQ_FUNCTION_GETNETWORKSTATE = 'AQQ/Function/GetNetworkState';
  // Funkcja: TAK wParam=0 lParam=PWideChar (tekst) Res=PWideChar (tekst zgodny z XML)
  // Opis: Funkcja zmienia znaki specjalne na zgodne ze specyfiką standardu XML.
  AQQ_FUNCTION_CONVERTTOXML = 'AQQ/Function/ConvertToXML';
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=Integer (index ikony)
  // Opis: Funkcja zwraca index ikony stanu, który jest przypisany aktualnie do wskazanego kontaktu.
  AQQ_FUNCTION_GETSTATEPNG_INDEX = 'AQQ/Function/GetStatePNG/Index';
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=PWideChar (ścieżka do ikony)
  // Opis: Funkcja zwraca ścieżkę do ikony stanu, która jest przypisana aktualnie do wskazanego kontaktu.
  AQQ_FUNCTION_GETSTATEPNG_FILEPATH = 'AQQ/Function/GetStatePNG/FilePath';
  // Funkcja: TAK wParam=Integer (index ikony) lParam=0 Res=PWideChar (ścieżka do ikony)
  // Opis: Funkcja zwraca ścieżkę do ikony o indexie podanym w parametrze wParam.
  AQQ_FUNCTION_GETPNP_FILEPATH = 'AQQ/Function/GetPNG/FilePath';
  // Funkcja: TAK wParam=0 lParam=Integer (index ikony) Res=PWideChar (ścieżka do ikony HD)
  // Opis: Funkcja zwraca ścieżkę do ikony HD o indexie podanym w parametrze lParam.
  AQQ_FUNCTION_GETPNGHD_FILEPATH = 'AQQ/Function/GetPNGHD/FilePath';
  // Funkcja: TAK wParam=Integer (typ wiadomości: 0 informacja; 1 ostrzeżenie; 2 pytanie ##010) lParam=PWideChar (tekst) Res=[1 ok; 2 analuj; 3 zaniechaj; 4 próbuj ponownie; 5 ignoruj; 6 tak; 7 nie; 8 zamknij; 9 pomoc]
  // Opis: Funkcja wyświetla okno dialogowe o wskazanym typie, i zwraca wynik akcji użytkownika wobec informacji zawartej w parametrze lParam.
  AQQ_FUNCTION_SHOWMESSAGE = 'AQQ/Function/ShowMessage';
  // Funkcja: TAK wParam=0 lParam=Integer (stała stanu np. CONTACT_OFFLINE ##016) Res=PWideChar (tekstowa reprezentacja stanu)
  // Opis: Funkcja zmienia stałą stanu, na tekst - np. "Rozłączony".
  AQQ_FUNCTION_STATETOSTR = 'AQQ/Function/StateToStr';
  // Funkcja: TAK wParam=[0 domyślnie; 1 dodaję tekst do pliku loga pomimo wyłączonego logowania] lParam=PWideChar (tekst) Res=0
  // Opis: Funkcja dodaję wskazany tekst do pliku log-a.
  AQQ_FUNCTION_LOG = 'AQQ/Function/Log';
  // Funkcja: TAK wParam=0 lParam=PPluginShowInfo Res=0
  // Opis: Funkcja wyświetla chmurkę informacyjną skonstruowaną przy pomocy wypełnionej struktury PPluginShowInfo.
  // Notyfikacja: TAK wParam=0 lParam=PPluginShowInfo Res=[0 ok; 1 blokada wyświetlenia]
  // Opis: Informuje o próbie wyświetlenia chmurki informacyjnej. Można ją zablokować zwracając wartość 1.
  AQQ_FUNCTION_SHOWINFO = 'AQQ/Function/ShowInfo';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer [kolejny numer startu AQQ]
  // Opis: Funkcja zwraca liczbę startów AQQ na danym profilu.
  AQQ_FUNCTION_GETSTARTCOUNT = 'AQQ/Function/GetStartCount';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar [plik INI]
  // Opis: Funkcja zwraca ustawienia AQQ zgodnie ze standardem pliku INI.
  AQQ_FUNCTION_FETCHSETUP = 'AQQ/Function/FetchSetup';
  // Funkcja: TAK wParam=[0 domyślnie; 1 odświeża ustawienia w AQQ] lParam=PSaveSetup Res=0
  // Opis: Funkcja zapisuje nowy element ustawień AQQ.
  AQQ_FUNCTION_SAVESETUP = 'AQQ/Function/SaveSetup';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Funkcja pobiera na nowo dane z pliku konfiguracyjnego AQQ. Następuje odświeżenie.
  AQQ_FUNCTION_REFRESHSETUP = 'AQQ/Function/RefreshSetup';
  // Funkcja: TAK wParam=0 lParam=[0 domyślnie; 1 wymusza sprawdzenie; 2; wymusza i sprawdza też dodatki] Res=0
  // Opis: Funkcja sprawdza dostępne aktualizacje.
  AQQ_FUNCTION_SILENTUPDATECHECK = 'AQQ/Function/SilentUpdateCheck';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (uchwyt do pliku DLL)
  // Opis: Funkcja zwraca uchwyt załadowanej biblioteki OpenSSL: libssl32.dll - wtyczka nie musi ładować jej sama ponownie.
  AQQ_FUNCTION_SSLHANDLE = 'AQQ/Function/SSLHandle';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (uchwyt do pliku DLL)
  // Opis: Funkcja zwraca uchwyt załadowanej biblioteki OpenSSL: libeay32.dll - wtyczka nie musi ładować jej sama ponownie.
  AQQ_FUNCTION_SSLLIBHANDLE = 'AQQ/Function/SSLLibHandle';
  // Funkcja: TAK wParam=PWideChar (jid) lParam=Integer (ID konta) Res=0
  // Opis: Funkcja wczytuje ostatnią rozmowę do otwartego już okna rozmowy ze wskazanym kontaktem.
  AQQ_FUNCTION_LOADLASTCONV = 'AQQ/Function/LoadLastConv';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Funkcja wywołuję funkcje Application.ProcessMessages po stronie AQQ.
  AQQ_FUNCTION_PROCESSMESSAGES = 'AQQ/Function/ProcessMessages';
  // Funkcja: TAK wParam=0 lParam=PWideChar (tekst) Res=PWideChar (znormalizowany tekst)
  // Opis: Funkcja normalizuje nazwy pokoi czatowych.
  AQQ_FUNCTION_NORMALIZE = 'AQQ/Function/Normalize';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (domyślny nick użytkownika)
  // Opis: Funkcja zwraca domyślny pseudonim użytkownika, pobrany z profilu głównego konta skonfigurowanego w AQQ.
  AQQ_FUNCTION_DEFAULTNICK = 'AQQ/Function/DefaultNick';
  // Funkcja: TAK wParam=PPluginHTTPRequest lParam=0 Res=PWideChar (zwrot z serwera)
  // Opis: Funkcja wysyła dane kanałem HTTP przy pomocy funkcji POST lub GET, i zwraca otrzymane informacje.
  AQQ_FUNCTION_URLEX = 'AQQ/Function/URLEx';
  // Funkcja: TAK wParam=PWideChar (adres URL) lParam=PWideChar (dane POST) Res=PWideChar (zwrot z serwera)
  // Opis: Funkcja wysyła dane kanałem HTTP przy pomocy funkcji POST, i zwraca otrzymane informacje.
  AQQ_FUNCTION_URLPOST = 'AQQ/Function/URLPost';
  // Funkcja: TAK wParam=PWideChar (adres URL) lParam=0 Res=PWideChar (zwrot z serwera)
  // Opis: Funkcja pobiera dane kanałem HTTP przy pomocy funkcji GET, i zwraca otrzymane informacje.
  AQQ_FUNCTION_URLGET = 'AQQ/Function/URLGet';
  // Funkcja: TAK wParam=PWideChar (ścieżka do pliku INI) lParam=0 Res=[0 błąd; Integer Id odnośnika do pliku INI]
  // Opis: Tworzy i/lub otwiera plik INI.
  AQQ_FUNCTION_INI_CREATE = 'AQQ/Function/INI/Create';
  // Funkcja: TAK wParam=Integer (ID odnośnika do pliku INI) lParam=0 Res=0
  // Opis: Zapisuje i zamyka plik INI.
  AQQ_FUNCTION_INI_FREE = 'AQQ/Function/INI/Free';
  // Funkcja: TAK wParam=Integer (ID odnośnika do pliku INI) lParam=PSaveSetup (sekcja, identyfikator i wartość) Res=PWideChar (wartość)
  // Opis: Odczytuje wartość tekstową z pliku INI.
  AQQ_FUNCTION_INI_READSTRING = 'AQQ/Function/INI/ReadString';
  // Funkcja: TAK wParam=Integer (ID odnośnika do pliku INI) lParam=PSaveSetup (sekcja, identyfikator i wartość) Res=0
  // Opis: Odczytuje wartość tekstową z pliku INI.
  AQQ_FUNCTION_INI_WRITESTRING = 'AQQ/Function/INI/WriteString';
  // Funkcja: TAK wParam=PWideChar (katalog) lParam=PWideChar (filtr wyszukiwania) Res=PWideChar (znalezione pliki)
  // Opis: Szuka plików we wskazanym katalogu, spełniające kryteria wyszukiwania.
  AQQ_FUNCTION_FINDFILES = 'AQQ/Function/FindFiles';
  // Funkcja: TAK wParam=TPluginTwoFlagParams (uchwyt do DLL, typ zasobu i nazwa zasobu) lParam=PWideChar (ścieżka do pliku docelowego) Res=0
  // Opis: Wyszukuje zasób w pliku EXE/DLL i zapisuje go do pliku.
  AQQ_FUNCTION_SAVERESOURCE = 'AQQ/Function/SaveResource';
  // Funkcja: TAK wParam=Integer lParam=0 Res=PWideChar (treść zawierająca cyfry)
  // Opis: Zmienia cyfrę w słowo.
  AQQ_FUNCTION_INTTOSTR = 'AQQ/Function/IntToStr';
  // Funkcja: TAK wParam=PWideChar (ścieżka) lParam=0 Res=PWideChar (ścieżka bez backslash-a)
  // Opis: Usuwa końcowy backslash ze wskazanej ścieżki.
  AQQ_FUNCTION_EXCLUDETRAILINGPATHDELIMITER = 'AQQ/Function/ExcludeTrailingPathDelimiter';
  // Funkcja: TAK wParam=PWideChar (ścieżka do pliku) lParam=0 Res=PWideChar (nazwa pliku)
  // Opis: Zwraca nazwę pliku ze ścieżki.
  AQQ_FUNCTION_EXTRACTFILENAME = 'AQQ/Function/ExtractFilename';
  // Funkcja: TAK wParam=PWideChar (ścieżka) lParam=0 Res=0
  // Opis: Tworzy katalog/katalogi zagnieżdżone.
  AQQ_FUNCTION_FORCEDIRECTORIES = 'AQQ/Function/ForceDirectories';
  // Funkcja: TAK wParam=PWideChar (tekst) lParam=PWideChar (delimiter) Res=Integer (pozycja)
  // Opis: Zwraca pozycje ostatniego "znaku" wskazanego w parametrze lParam.
  AQQ_FUNCTION_LASTDELIMITER = 'AQQ/Function/LastDelimiter';
  // Funkcja: TAK wParam=PWideChar (ścieżka do pliku) lParam=0 Res=[0 plik nie istnieje; 1 plik istnieje]
  // Opis: Zwraca pozycje ostatniego "znaku" wskazanego w parametrze lParam.
  AQQ_FUNCTION_FILEEXISTS = 'AQQ/Function/FileExists';
  // Funkcja: TAK wParam=PWideChar (tresć) lParam=Integer (liczba w przypadku błędu) Res=Integer (rezultat liczbowy)
  // Opis: Zmienia treść w liczbę, w przypadku błędu zwraca liczbę podaną w parametrze lParam.
  AQQ_FUNCTION_STRTOINTDEF = 'AQQ/Function/StrToIntDef';
  // Funkcja: TAK wParam=PWideChar (tresć) lParam=0 Res=PWideChar (treść w apostrofach '')
  // Opis: Dodaje apostrofy do wskazanej treści.
  AQQ_FUNCTION_QUOTEDSTR = 'AQQ/Function/QuotedStr';
  // Funkcja: TAK wParam=PWideChar (tresć) lParam=0 Res=PWideChar (hash SHA1)
  // Opis: Generuje hash SHA1 z podanej treści.
  AQQ_FUNCTION_SHA1 = 'AQQ/Function/Sha1';
  // Funkcja: TAK wParam=PWideChar (tresć) lParam=0 Res=[0 ok; 1 błąd]
  // Opis: Wykorzystuje Goole TTS, i wskazany tekst zostaje odegrany.
  AQQ_FUNCTION_SAY = 'AQQ/Function/Say';
  // Funkcja: TAK wParam=PWideChar (tresć wchodząca) lParam=[0; dekodowanie ANSI 1; kodowanie ANSI; 2 dekodowanie UTF8; 3 kodowanie UTF8] Res=PWideChar (treść wychodząca)
  // Opis: Koduje lub dekoduje ciąg znaków z lub do formatu Base64
  AQQ_FUNCTION_BASE64 = 'AQQ/Function/Base64';
  // Funkcja: TAK wParam=PWideChar (numer nowej wersji w formacie np. 2.5.0.12) lParam=PWideChar (numer starej wersji) Res=Integer (0 - wersja nie jest wyższa 1 - wersja jest wyższa)
  // Opis: Funkcja porównuje czy pierwszy numer wersji jest wyższy od drugiej.
  AQQ_FUNCTION_ISVERSIONHIGHER = 'AQQ/Function/IsVersionHigher';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (zwraca aktualnie ustawioną lokalizacje)
  // Opis: Funkcja zwraca kod kraju używanego w celu lokalizacji programu.
  AQQ_FUNCTION_GETLANGCODE = 'AQQ/Function/GetLangCode';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (zwraca domyślną - startową - lokalizacje)
  // Opis: Funkcja zwraca kod kraju używanego w celu lokalizacji programu zaraz po instalacji (domyślnie).
  AQQ_FUNCTION_GETDEFLANGCODE = 'AQQ/Function/GetDefLangCode';

/// !Consts - Stałe

const //##001 // Account Events - stałe akcji kont
  ACCOUNT_EVENT_DEFAULT = 0; // Domyślne zdarzenie przy edycji kont
  ACCOUNT_EVENT_NEW = 1; // Nowe konto
  ACCOUNT_EVENT_EDIT = 2; // Edycja konta
  ACCOUNT_EVENT_DELETE = 3; // Usunięcie konta
  ACCOUNT_EVENT_CHANGEPASS = 4; // Zmiana hasła dla konta

const //##002 // System Function - stałe funkcji systemowych
  SYS_FUNCTION_SEARCHONLIST = 1; // Wyłączenie funkcji szukania na liście kontaktów
  SYS_FUNCTION_ANTISPIM_LEN = 2; // Wyłączenie funkcji ochrony anty-spimowej
  SYS_FUNCTION_TASKBARPEN = 3; // Wyłączenie funkcji pisaka przy zminimalizowanym oknie rozmowy
  SYS_FUNCTION_CLOSEBTN = 4; // Wyłączenie przycisku "x" na zakładkach w oknie rozmowy
  SYS_FUNCTION_MSGCOUNTER = 5; // Wyłączenie graficznego licznika nieprzeczytanych wiadomości na zakładkach w oknie rozmowy
  SYS_FUNCTION_SKINSYSDLG = 6; // Wyłączenie skórkowania systemowych okien dialogowych

const //##003 // Tabs - stałe zakładek
  TAB_JABBER = 1; // Zakładka z listą kontaktów
  TAB_SMS = 2; // Zakładka SMS
  TAB_MULTICHAT = 3; // Zakładka czatów
  TAB_NEWS = 4; // Zakładka powiadomień
  TAB_DROPBOX = 5; // Zakładka Dropbox

const //##004
  SETNOTE_ABORT = 1; // wtyczka nie powinna zmieniać opisu, jeżeli to ona wywołała okno zmiany opisu.

const //##005
  CHATMODE_NORMAL = 0; // Tryb czatu normalny (z podziałem na grupy)
  CHATMODE_SIMPLE = 1; // Tryb czatu bez podziału na grupy

const //##006
  ROLE_VISITOR = 'visitor'; // Gość
  ROLE_MODERATOR = 'moderator'; // Moderator
  ROLE_PARTICIPANT = 'participant'; // Uczestnik
  ROLE_OBSERVER = 'none'; // Brak

const //##007
  AFFILIATION_ADMIN = 'admin'; // Administrator pokoju
  AFFILIATION_OUTCAST = 'outcast'; // Zbanowany
  AFFILIATION_MEMBER = 'member'; // Uczestnik
  AFFILIATION_OWNER = 'owner'; // Właściciel
  AFFILIATION_NONE = 'none'; // Brak

const //##008 // stała połączeń
  CONNSTATE_DISCONNECTED = 1; // Rozłączono
  CONNSTATE_CONNECTING = 2; // Trwa łączenie z siecią
  CONNSTATE_CONNECTED = 3; // Połączono

const //##009
  TOOLTIP_EVENT_TITLE = 0; // Tutył
  TOOLTIP_EVENT_DATA = 1; // Data
  TOOLTIP_EVENT_STATUS = 2; // Status tekstowy
  TOOLTIP_EVENT_AUTH = 3; // Stan subskrypcji
  TOOLTIP_EVENT_ACTIVITY = 4; // Ostatnia aktywność

const //##010
  SM_INFO = 0; // Informacja
  SM_WARN = 1; // Ostrzeżenie
  SM_QUESTION = 2; // Pytanie

const //##011 // stałe zmiany statusu w oknie
  WINDOW_STATUS_NONE = 0; // Żadna wtyczka nie zainteresowana.
  WINDOW_STATUS_SET = 1; // Wtyczka zainteresowana i ustawiła status.
  WINDOW_STATUS_SETEXIT = 2; // Wtyczka zainteresowana, ona wywołała okno i AQQ powinno je tylko zamknąć (brak zmian na kontach Jabber).

const //##012
  AQQ_CACHE_ITEM = 'AQQ_CACHE_ITEM'; // Używane w treści wiadomości jako stała obrazka.
  AQQ_CACHE_GHOSTITEM = 'AQQ_CACHE_GHOSTITEM'; // Nie wykorzystywane przez wtyczki aktualnie.

const //##013 // stałe sprawdzające
  AQQ_QUERY_DELETE = 1; // Można usunąć kontakt/grupę
  AQQ_QUERY_MESSAGE = 2; // Można wysłać wiadomość
  AQQ_QUERY_SENDFILE = 3; // Można wysłaać plik
  AQQ_QUERY_AUTHREQUEST = 4; // Można wysłać prośbę o subskrypcję
  AQQ_QUERY_SENDPIC = 5; // Można wysłać obrazek
  AQQ_QUERY_AUTH = 6; // Można ustawiać subskrypcje
  AQQ_QUERY_CANCELAUTH = 7; // Można wycofać subskrypcje
  AQQ_QUERY_SMS = 8; // Można wysyłać SMS
  AQQ_QUERY_VCARD = 9; // Można edytować wizytówkę
  AQQ_QUERY_ARCHIVE = 10; // Można korzystać z archiwum wiadomości
  AQQ_QUERY_NETWORK = 11; // Zastąpione przez AQQ_QUERY_NETWORKSTATE
  AQQ_QUERY_HAVEVCARD = 12; // Kontakt posiada wizytókę
  AQQ_QUERY_ATTENTION = 13; // Można prosić o uwagę
  AQQ_QUERY_NETWORKSTATE = 14; // Stan połączenia

const //##014 // stałe sprawdzające -> Stany i opisy
  AQQ_QUERY_STATE = 15; // Obsługa stanów
  AQQ_QUERY_NOTE = 16; // Obsługa opisów
  AQQ_QUERY_ONLINE = 17; // Obsługa stanu połączonego
  AQQ_QUERY_FFC = 18; // Obsługa stanu - chętny do rozmowy
  AQQ_QUERY_AWAY = 19; // Obsługa stanu oddalonego
  AQQ_QUERY_XA = 20; // Obsługa stanu nieobecnego
  AQQ_QUERY_DND = 21; // Obsługa stanu nie przeszkadzać
  AQQ_QUERY_OFFLINE = 22; // Obsługa stanu rozłączonego

const //##015 // stałe typu zmiany statusu
  CONTACT_UPDATE_OFFLINE = -1; // Kontakt zmienił stan na rozłączony
  CONTACT_UPDATE_NORMAL = 0; // Kontakt należy zaktualizować w normalnym trybie
  CONTACT_UPDATE_NOOFFLINE = 1; // Kontakt zmienił stan na inny niż rozłączony
  CONTACT_UPDATE_ONLINE = 2; // Kontakt właśnie się połączył
  CONTACT_UPDATE_ONLYSTATUS = 3; // Zaktualizować należy jedynie stan i status kontaktu

const //##016 // stałe stanu
  CONTACT_OFFLINE = 0; // Kontakt rozłączony
  CONTACT_ONLINE = 1; // Kontakt połączony
  CONTACT_FFC = 2; // Kontakt chętny do rozmowy
  CONTACT_AWAY = 3; // Kontakt oddalony
  CONTACT_NA = 4; // Kontakt nieobecny
  CONTACT_DND = 5; // Nie przeszkadzać
  CONTACT_INV = 6; // Kontakt niewidoczny
  CONTACT_NULL = 7; // Nie określony stan / zablokowany

const //##017
  DEBUG_XMLIN = 0; // Pakiet XML przychodzący
  DEBUG_XMLOUT = 1; // Pakiet XML wychodzący

const //##018 // stałe online
  ONCHECK_COPYTO = 1; // Wtyczka zezwala na kopiowanie kontaktów między grupami
  ONCHECK_DELETE = 2; // Wtyczka pozwala na usuwanie kontaktów
  ONCHECK_MOVETO = 3; // Wtyczka pozwala na przesuwanie kontaktów pomiędzy grupami
  ONCHECK_CHANGENAME = 4; // Wtyczka pozwala na zmianę nazwy kontaktu
  ONCHECK_SENDMESSAGE = 5; // Wtyczka pozwala na wysyłanie wiadomości
  ONCHECK_SENDIMAGE = 6; // Wtyczka pozwala na wysyłkę obrazków
  ONCHECK_SETGROUPS = 7; // Wtyczka pozwala na edycje grup
  ONCHECK_VCARD = 8; // Wtyczka posiada możliwość edycji wizytówek

const //##019
  CHAT_NONE = 0; // Stan użytkownika nieznany
  CHAT_ACTIVE = 1; // Użytkownik ma aktywne okno rozmowy z odbiorcą
  CHAT_COMPOSING = 2; // Użytkownik piszę wiadomość
  CHAT_GONE = 3; // Użytkownik odszedł / wyłączył okno rozmowy z odbiorcą
  CHAT_INACTIVE = 4; // Użytkownik nie aktywny
  CHAT_PAUSED = 5; // Uzytkownik na chwilę przestał pisać wiadomość

const //##020 // stałe stanu wiadomości SMS
  SMS_STATUS_FAILED = 0; // SMS nie wysłany
  SMS_STATUS_OK = 1; // SMS wysłany
  SMS_STATUS_DELIVERED = 2; // SMS dostarczono
  SMS_STATUS_NOTDELIVERED = 3; // SMS nie dostarczony

const //##021
  SMS_SEND_OK = 1; // SMS wysłano prawidłowo
  SMS_SEND_FAILED = 2; // SMS nie udało się wysłać
  SMS_SEND_FAILED_SET = 3; // SMS nie udało się wysłać, błędna konfiguracja
  SMS_SEND_FAILED_AUTH = 4; // SMS nie udało się wysłać, brak autoryzacji
  SMS_SEND_FAILED_CONNECT = 5; // SMS nie udało się wysłać, brak połączenia
  SMS_SEND_FAILED_LIMIT = 6; // SMS nie udało się wysłać, limit wiadomości przekroczono
  SMS_SEND_FAILED_TOKEN = 7; // SMS nie udało się wysłać, błędny token
  SMS_SEND_FAILED_NOSUPPORT = 8; // SMS nie udało się wysłać, bramka nie obsługuję nr. telefonu
  SMS_SEND_FAILED_UNKNOWN = 9; // SMS nie udało się wysłać, przyczyna nieznana
  SMS_SEND_OK_SILENT = 10; // SMS wysłano prawidłowo, nie pokazuj komunikatów
  SMS_SEND_OK_SILENT_NOENABLE = 11; // SMS wysłano prawidłowo, kontrolki SMS zostają jednak wciąż nieaktywne.
  SMS_SEND_FAILED_FILTER = 12; // SMS nie udało się wysłać, wiadomość zawiera niedozwolone znaki/słowa
  SMS_SEND_FAILED_TOOLONG = 13; // SMS nie udało się wysłać, wiadomość zbyt długa
  SMS_SEND_FAILED_ABORTED = 14; // SMS nie udało się wysłąć, użytkownik wycofał się

const //##022 // stałe dźwiękowe
  SOUND_FIRSTIN = 0; // Dźwięk pierwszej przychodzącej wiadomości
  SOUND_IN = 1; // Dźwięk przychodzącej wiadomości
  SOUND_OUT = 2; // Dźwięk wychodzącej wiadomości
  SOUND_STATUS = 3; // Dźwięk zmiany stanu
  SOUND_TRANSFER = 4; // Dźwięk nowego transferu
  SOUND_ENDTRANSFER = 5; // Dźwięk zakończonego transferu
  SOUND_LOCK = 6; // Dźwięk zabezpieczenia AQQ
  SOUND_HELP = 7; // Dźwięk pomocy
  SOUND_CLOSEHELP = 8; // Dźwięk zamknięcia pomocy
  SOUND_CHATIN = 9; // Dźwięk przychodzącej wiadomości na czacie
  SOUND_CHATOUT = 10; // Dźwięk wychodzącej wiadomości na czacie
  SOUND_ATTENTION = 11; // Dźwięk proszenia o uwagę
  SOUND_CALLIN = 12; // Dźwięk przychodzącej rozmowy głosowej
  SOUND_CALLEND = 13; // Dźwięk zakończonej rozmowy głosowej
  SOUND_INFOBOX = 14; // Dźwięk funkcji "Daj mi znać"
  SOUND_POINTS = 15; // Dźwięk awansu na wyższy poziom
  SOUND_AUTH = 16; // Dźwięk autoryzacji
  SOUND_NEWS = 17; // Dźwięk powiadomienia
  SOUND_CLICK = 18; // Dźwięk kliknięcia myszką komputerową

const //##023
  IMP_TYPE_EXPORT = 0; // Eksport
  IMP_TYPE_IMPORT = 1; // Import
  IMP_TYPE_OTHER = 2; // Inne

const //##024
  ALIGN_TOP = 0; // Rozciągnięcie do góry
  ALIGN_BOTTOM = 1; // Rozciągnięcie do dołu
  ALIGN_LEFT = 2; // Rozciągnięcie do lewej strony
  ALIGN_RIGHT = 3; // Rozciągnięcie do prawej strony
  ALIGN_CLIENT = 4; // Rozciągnięcie na cały dostępny obszar
  ALIGN_TOWINDOW = 5;

const //##025
  MSGKIND_CHAT = 0; // Wiadomość w oknie rozmowy
  MSGKIND_GROUPCHAT = 1; // Wiadomość na czacie
  MSGKIND_RTT = 2; // Wiadomość typu RTT

const //##026
  TRANSFER_DECLINE = 0; // Transfer odrzocono
  TRANSFER_ACCEPT = 1; // Transfer zaakceptowano
  TRANSFER_CONNECTING = 2; // Trwa łączenie
  TRANSFER_ACTIVE = 3; // Transfer w toku
  TRANSFER_DISCONNECT = 4; // Transfer rozłączony
  TRANSFER_FINISHED = 5; // Transfer zakończony pomyślnie
  TRANSFER_PAUSED = 6; // Pauza
  TRANSFER_ABORTED = 7; // Zarzucono transfer w trakcie jego trwania

const //##027
  WINDOW_EVENT_CREATE = 1; // Okno zostało stworzone
  WINDOW_EVENT_CLOSE = 2; // Okno zostało zamknięte

const //##028
  SUB_BOTH = 0; // Subskrypcja w obie strony
  SUB_NONE = 1; // Brak subskrypcji
  SUB_FROM = 2; // Subskrypcja od użytkownika
  SUB_TO = 3; // Subskryocja do użytkownika
  SUB_REMOVE = 4; // Usunięcie subskrypcji
  SUB_NONEASK = 5; // Brak odpowiedzi
  SUB_FROMASK = 6; // Zapytanie o subskrypcje

const //##029 // stałe proxy
  SOCKS_5 = 0; // Wersja proxy 5
  SOCKS_4A = 1; // Wersja proxy 4A
  SOCKS_4 = 2; // Wersja proxy 4

const //##030 // przyciski myszy
  MB_LEFT = 0; // lewy przycisk myszy
  MB_RIGHT = 1; // prawy przycisk myszy
  MB_DBLCLICK = 2; // podwójne kliknięcie lewym przyciskiem myszy

const //##031 // stałe akcji
  CONTROL_ACTION_CREATE = 0;
  CONTROL_ACTION_DESTROY = 1;
  CONTROL_ACTION_SET = 2;
  CONTROL_ACTION_GET = 3;
  CONTROL_ACTION_SETFOCUS = 4;

const //##032 // stałe modalnego zwrotu
  MODALRES_NONE = 0;
  MODALRES_OK = 1;
  MODALRES_CANCEL = 2;

/// # Structs - struktury
type
  THintEvent =
    (theAuth, // Autoryzacja
    theTitle, // Tytuł
    theData, // Data
    theStatus, // Status
    theActivity); // Ostatnia aktywność

type
  TMiniEvent =
    (tmeDeleted, // Zdarzenie usunięto
    tmeStatus, // Status
    tmePseudoStatus, // Wygląd statusu, ale nie traktowane jak status
    tmePseudoMsg, // Wygląd wiadomości, ale nie traktowane jako wiadomość
    tmeMsg,  // Wiadomość
    tmePseudoMsgCap, // Wygląd tytułu wiadomości, ale nie traktowane jak tytuł wiadomości
    tmeMsgCap, // Tytuł wiadomości
    tmeInfo,   // Informacja
    tmeAction, // Akcja
    tmeAbuse,  // Informacja o spimie
    tmeVoip);  // Rozmowa głosowa

type
  TAddonType =
    (tatUnCheck, // Dodatek jeszcze nie sprawdzony
    tatUnknown, // Dodatek nieznanego typu
    tatVisualStyle, // Dodatek to wizualna kompozycja
    tatVisualStylePatch, // Dodatek to modyfikacja kompozycji
    tatSmileys, // Dodatek to paczka emotek
    tatPlugin); // Dodtek to wtyczka

type
  PPluginChatPrep = ^TPluginChatPrep;

  TPluginChatPrep = record
    cbSize: Integer; // Wielkość struktury
    UserIdx: Integer; // ID sieci
    JID: PWideChar; // Pełny JID kanału
    Channel: PWideChar; // Nazwa kanału do wyświetlenia użytkownikowi
    CreateNew: Boolean; // Tworzymy nowy kanał na serwerze [tak/nie]
    Fast: Boolean; // Szybkie "OK", z domyślnymi ustawieniami.
  end;

type
  PPluginChatPresence = ^TPluginChatPresence;

  TPluginChatPresence = record
    cbSize: Integer; // Wielkość struktury
    UserIdx: Integer; // ID sieci
    JID: PWideChar; // JabberID na czacie
    RealJID: PWideChar; // Prawdziwe JabberID
    Affiliation: PWideChar; // Grupa np. moderatorzy, zbanowani itd. ##007
    Role: PWideChar; // Rola ##006
    Offline: Boolean; // Połączony z pokojem tak/nie
    Nick: PWideChar; // Pseudonim
    Kicked, Banned: Boolean; // Wykopany, zbanowany użytkownik
    Ver: PWideChar; // Wersja
    JIDToShow: PWideChar; // JID który należy pokazać
  end;

type
  PPluginChatOpen = ^TPluginChatOpen;

  TPluginChatOpen = record
    cbSize: Integer; // Wielkość struktury
    JID: PWideChar; // JabberID
    UserIdx: Integer; // ID sieci
    Channel: PWideChar; // Nazwa kanału
    Nick: PWideChar; // Pseudonim
    NewWindow: Boolean; // Nowe okno tak/nie
    IsNewMsg: Boolean; // Jest dostępna nowa wiadomość tak/nie
    Priority: Boolean; // Otwórz priorytetowo
    OriginJID: PWideChar; // Prawdziwy JID
    ImageIndex: Integer; // Index ikony
    AutoAccept: Boolean; // Automatycznie akceptuj zaproszenie do czat-u
    ChatMode: Byte; // Tryb czatu ##005
  end;

type
  PPluginHook = ^TPluginHook;

  TPluginHook = record
    HookName: PWideChar; // Nazwa uchwytu notyfikacji
    wParam: Windows.WPARAM; // Parametr wParam
    lParam: Windows.LPARAM; // Parametr lParam
  end;

type
  PSaveSetup = ^TSaveSetup;

  TSaveSetup = record
    Section: PWideChar; // Sekcja w pliku INI
    Ident: PWideChar; // Identyfikator pola w pliku INI
    Value: PWideChar; // Zawartość pola
  end;

type
  PPluginSong = ^TPluginSong;

  TPluginSong = record
    Title: PWideChar; // Tytuł piosenki
    Position: Integer; // Pozacja na playliście numeryczna
    Length: Integer; // Długość piosenki w sekundach
  end;

type
  PPluginAddForm = ^TPluginAddForm;

  TPluginAddForm = record
    UserIdx: Integer; // ID sieci
    JID: PWideChar; // JabberID
    Nick: PWideChar; // Pseudonim
    Agent: PWideChar; // Agent serwera
    Modal: Boolean; // Okno modalne tak/nie
    Custom: Boolean; // Nie bierz pod uwagę standardowych agentów
  end;

type
  PPluginShowInfo = ^TPluginShowInfo;

  TPluginShowInfo = record
    cbSize: Integer; // Wielkość struktury
    Event: TMiniEvent; // Typ zdarzenia
    Text: PWideChar; // Treść
    ImagePath: PWideChar; // Ścieżka do ikony
    TimeOut: Integer; // Czas wygaśnięcia w milisekundach
    ActionID: PWideChar; // Akcja do ewentualnego uruchomienia po kliknięciu
    Tick: Cardinal; // Tick z funkcji systemowej GetTickCount
  end;

type
  PPluginToolTipItem = ^TPluginToolTipItem;

  TPluginToolTipItem = record
    cbSize: Integer; // Wielkość struktury
    Tick: Cardinal; // Tick z funkcji systemowej GetTickCount
    Event: Integer; // Typ zdarzenia ##009
    Text: PWideChar; // Treść
    ImagePath: PWideChar; // Ścieżka do ikony
    ActionID: PWideChar; // ID akcji
    Y1, Y2: Integer; // Pozycja na ekranie
    TimeOut: Integer; // Czas wygaśnięcia w milisekundach
    Flag: Integer; // Dodatkowe flagi
  end;

type
  PPluginToolTipID = ^TPluginToolTipID;

  TPluginToolTipID = record
    cbSize: Integer; // Wielkość struktury
    ID: PWideChar; // ID tooltipa
    Name: PWideChar; // Nazwa tooltipa
  end;

type
  PPluginColorChange = ^TPluginColorChange;

  TPluginColorChange = record
    cbSize: Integer; // Wielkość struktury
    Hue: Integer; // Barwa zakres od [0 do 360]
    Saturation: Integer; // Nasycenie zakres od [-100 do 100]
    Brightness: Integer; // Jasność zakres od [-100 do 100]
  end;

type
  PPluginSMSResult = ^TPluginSMSResult;

  TPluginSMSResult = record
    cbSize: Integer; // Wielkość struktury
    ID: PWideChar; // ID wysłanej wiadomości
    Result: Integer; // Wynik wysyłki
  end;

type
  PPluginXMLChunk = ^TPluginXMLChunk;

  TPluginXMLChunk = record
    cbSize: Integer; // Wielkość struktury
    ID: PWideChar; // ID
    From: PWideChar; // Skąd pochodzi pakiet
    XML: PWideChar; // Pakiet XML
    UserIdx: Integer; // ID sieci
  end;

type
  PPluginContactSimpleInfo = ^TPluginContactSimpleInfo;

  TPluginContactSimpleInfo = record
    cbSize: Integer; // Wielkość struktury
    JID: PWideChar; // Jabber ID
    First: PWideChar; // Pierwsze imie
    Last: PWideChar; // Nazwisko
    Nick: PWideChar; // Pseudonim
    CellPhone: PWideChar; // Tel. komórkowy
    Phone: PWideChar; // Telefon stacjonarny
    Mail: PWideChar; // Adres e-mail
  end;

type
  PPluginItemDescriber = ^TPluginItemDescriber;

  TPluginItemDescriber = record
    cbSize: Integer; // Wielkość struktury
    FormHandle: Cardinal; // Uchwyt okna (zero - główne okno)
    ParentName: PWideChar; // Nazwa rodzica którego potomkiem jest element
    Name: PWideChar; // Nazwa elementu
  end;

type
  PPluginPopUp = ^TPluginPopUp;

  TPluginPopUp = record
    cbSize: Integer; // Wielkość struktury
    Name: PWideChar; // Nazwa menu
    ParentHandle: Cardinal; // Uchyt rodzica
    Handle: Cardinal; // Uchwyt
  end;

type
  PPluginTriple = ^TPluginTriple;

  TPluginTriple = record
    cbSize: Integer; // Wielkość struktury
    Handle1: Cardinal; // Zmienna pomocnicza 1
    Handle2: Cardinal; // Zmienna pomocnicza 2
    Handle3: Cardinal; // Zmienna pomocnicza 3
    Param1: Integer; // Zmienna pomocnicza 4
    Param2: Integer; // Zmienna pomocnicza 5
  end;

type
  PPluginError = ^TPluginError;

  TPluginError = record
    cbSize: Integer; // Wielkość struktury
    ID: PWideChar; // ID zdarzenia
    Desc: PWideChar; // Opis
    LangID: PWideChar; // ID lokalizacji językowej
    ErrorCode: PWideChar; // Kod błędu
  end;

type
  PPluginTransfer = ^TPluginTransfer;

  TPluginTransfer = record
    cbSize: Integer; // Wielkość struktury
    ID: PWideChar; // ID transferu
    FileName: PWideChar; // Nazwa pliku
    Location: PWideChar; // Lokalizacja pliku
    Status: Integer; // Status przesyłu ##026
    TextInfo: PWideChar; // Informacja tekstowa
    FileSize: Int64; // Wielkość pliku
    Position: Int64; // Aktualna pozycja w zapisie/odczycie
    LastDataSize: Int64; // Ostatnia wielkość otrzymanego/wysłanego pakietu
  end;

type
  PPluginTransferOld = ^TPluginTransferOld;

  TPluginTransferOld = record
    cbSize: Integer; // Wielkość struktury
    ID: PWideChar; // ID transferu
    FileName: PWideChar;  // Nazwa pliku
    Location: PWideChar; // Lokalizacja pliku
    Status: Integer; // Status przesyłu ##026
    TextInfo: PWideChar; // Tekstowa informacja
    FileSize: Cardinal; // Wielkość pliku
    Position: Cardinal; // Aktualna pozycja w zapisie
    LastDataSize: Cardinal; // Ostatnia wielkość otrzymanego/wysłanego pakietu
  end;

type
  PPluginWindowEvent = ^TPluginWindowEvent;

  TPluginWindowEvent = record
    cbSize: Integer; // Wielkość struktury
    ClassName: PWideChar; // Klasa okna
    Handle: Cardinal; // Uchwyt okna
    WindowEvent: Integer; // Zdarzenie ##027
  end;

type
  PPluginChatState = ^TPluginChatState;

  TPluginChatState = record
    cbSize: Integer; // Wielkość struktury
    Text: PWideChar; // Tekst
    Length: Integer; // Dł. tekstu
    SelStart: Integer; // Zaznaczenie start
    SelLength: Integer; // Zaznaczenie stop
    ParentHandle: Cardinal; // Uchwyt rodzica
    Handle: Cardinal; // Uchwyt
    ChatState: Integer; // stałe pisaka ##019
  end;

type
  PPluginWebItem = ^TPluginWebItem;

  TPluginWebItem = record
    cbSize: Integer; // Wielkość struktury
    ID: PWideChar; // ID zawarte w kodzie HTML
    Text: PWideChar; // Zawartość tekstowa
  end;

type
  PPluginSmallInfo = ^TPluginSmallInfo;

  TPluginSmallInfo = record
    cbSize: Integer; // Wielkość struktury
    Text: PWideChar; // Tekst
  end;

type
  PPluginWebBeforeNavEvent = ^TPluginWebBeforeNavEvent;

  TPluginWebBeforeNavEvent = record
    cbSize: Integer; // Wielkość struktury
    Handle: Cardinal; // Uchwyt do kontrolki IE
    URL: PWideChar; // Adres URL
    Flags: PWideChar; // Flagi
    TargetFrameName: PWideChar; // Ramka
    PostData: PWideChar; // Dane dla akcji POST
    Headers: PWideChar; // Nagłówki
  end;

type
  PPluginWebBrowser = ^TPluginWebBrowser;

  TPluginWebBrowser = record
    cbSize: Integer; // Wielkość struktury
    Handle: Cardinal; // Uchwyt do kontrolki IE (0 jeżeli tworzymy nową)
    Top, Left, Width, Height: Integer; // Pozycja
    Align: Integer; // Auto rozciąganie (typ) ##024
    RegisterAsDropTarget: Boolean; // Zarejestruj jako drop-target
    SetVisible: Boolean; // Kontroka IE widoczna tak/nie
    SetEnabled: Boolean; // Kontrolka IE aktywna tak/nie
  end;

type
  PPluginDebugInfo = ^TPluginDebugInfo;

  TPluginDebugInfo = record
    cbSize: Integer; // Wielkość struktury
    JID: PWideChar; // JabberID
    XML: PWideChar; // Pakiet XML
    Mode: Byte; // Tryb do/od ##017
  end;

type
  PPluginMaxStatus = ^TPluginMaxStatus;

  TPluginMaxStatus = record
    cbSize: Integer; // Wielkość struktury
    IconIndex: Integer; // Ikona sieci
    Name: PWideChar; // Nazwa
    Max: Integer; // Maksymalna długośc opisu tekstowego
  end;

type
  PPluginProxy = ^TPluginProxy;

  TPluginProxy = record
    cbSize: Integer; // Wielkość struktury
    ProxyEnabled: Boolean; // Proxy aktywne tak/nie
    ProxyServer: PWideChar; // Adres serwera proxy
    ProxyPort: Integer; // Adres portu
    ProxyType: Integer; // Typ Proxy (stałe proxy ##029)
    ProxyAuth: Boolean; // Proxy wymaga autoryzacji
    ProxyLogin: PWideChar; // Login
    ProxyPass: PWideChar; // Hasło
  end;

type
  PPluginImpExp = ^TPluginImpExp;

  TPluginImpExp = record
    cbSize: Integer; // Wielkość struktury
    ImportType: Integer; // Tym import / eskport / inne ##023
    Name: PWideChar; // Nazwa
    Service: PWideChar; // Serwis do uruchomienia we wtyczce
    IconIndex: Integer; // Przypisany index ikony
  end;

type
  PPluginAutomation = ^TPluginAutomation;

  TPluginAutomation = record
    cbSize: Integer; // Wielkość struktury
    Flags: Integer; // Flagi
    NewState: Integer; // Nowy stan
    LastActive: Double; // Ostatnia aktywność
  end;

type
  PPluginStateChange = ^TPluginStateChange;

  TPluginStateChange = record
    cbSize: Integer; // Wielkość struktury
    OldState: Integer; // Poprzedni stan
    NewState: Integer; // Nowy stan
    Status: PWideChar; // Status tekstowy
    ByHand: Boolean; // Użytkownik zmienia status ręcznie
    UserIdx: Integer; // ID sieci skojarzonej
    JID: PWideChar; // JabberID
    Force: Boolean; // Zmiana przymusowa
    Server: PWideChar; // Adres serwera
    Authorized: Boolean; // Użytkownik jest zalogowany tak/nie
    FromPlugin: Boolean; // Request pochodzi z wtyczki sieciowej tak/nie
    Resource: PWideChar; // Zasób
  end;

type
  PPluginSMS = ^TPluginSMS;

  TPluginSMS = record
    cbSize: Integer; // Wielkość struktury
    CellPhone: PWideChar; // Numer telefonu
    Msg: PWideChar; // Wiaodmość SMS
    Sign: PWideChar; // Podpis
    GateID: Integer; // ID bramki SMS
  end;

type
  PPluginTwoFlagParams = ^TPluginTwoFlagParams;

  // Struktura wykorzystywana w kilku miejscach - pomocnicza
  TPluginTwoFlagParams = record
    cbSize: Integer; // Wielkośc struktury
    Param1: PWideChar; // Parametr pomocniczy 1
    Param2: PWideChar; // Parametr pomocniczy 2
    Flag1: Integer; // Parametr pomocniczy 3
    Flag2: Integer; // Parametr pomocniczy 4
  end;

type
  PPluginAddUser = ^TPluginAddUser;

  TPluginAddUser = record
    cbSize: Integer; // Wielkość struktury
    UID: PWideChar; // UID/JID kontaktu
    Server: PWideChar; // Serwer
    Nick: PWideChar; // Pseudonim
    Group: PWideChar; // Grupy
    Reason: PWideChar; // Powód dodania
    Service: PWideChar; // Serwis z którego korzysta użytkownik w celu dodania kontaktu
  end;

type
  PPluginSMSGate = ^TPluginSMSGate;

  TPluginSMSGate = record
    cbSize: Integer; // Wielkość struktury
    Name: PWideChar; // Nazwa
    Prompt: PWideChar; // Krótka informacja dla użytkownika dotycząca obsługi
    IsConfig: Boolean; // Bramka posiada możliwość dodatkowej konfiguracji
    MaxLength: Integer; // Maksymalna długość wiadomości w bramce SMS
    SignMaxLength: Integer; // Maksymalna długość podpisu w bramce SMS
  end;

type
  PPluginAgent = ^TPluginAgent;

  TPluginAgent = record
    cbSize: Integer; // Wielkość struktury
    JID: PWideChar; // JabberID
    Name: PWideChar; // Nazwa funkcjonalności serwera Jabberowego (agent)
    Prompt: PWideChar; // Etykieta przy rejestracji
    Transport: Boolean; // Agent to transport tak/nie
    Search: Boolean; // Agent to wyszukiwarka kontaktów tak/nie
    Groupchat: Boolean; // Agent to czaty (MUC)
    Agents: Boolean; // Posiada agentów tak/nie
    Service: PWideChar; // Nazwa serwisu
    CanRegister: Boolean; // Można się rejestrować tak/nie
    Description: PWideChar; // Opis
    RequiredID: Boolean; // Wymaga ID
    IconIndex: Integer; // Index ikony skojarzonej z agentem
    PluginAccountName: PWideChar; // Nazwa konta np. Sieć GG
  end;

type
  PPluginAvatar = ^TPluginAvatar;
  TPluginAvatar = record
    FileName: PWideChar; // Ścieżka do pliku
    XEPEmpty: Boolean; // Pusty awatar tak/nie
    SilentMode: Boolean; // Nie wyświetlaj żadnych komunikatów
    JID: PWideChar; // JID konta
  end;

type
  PPluginMessage = ^TPluginMessage;

  TPluginMessage = record
    cbSize: Integer; // Wielkość struktury
    JID: PWideChar; // JabberID
    Date: Double; // Data
    ChatState: Integer; // Stan pisaka ##019
    Body: PWideChar; // Treść wiadomości ##012
    Offline: Boolean; // Wiadomość typu offline
    DefaultNick: PWideChar; // Domyślny nick skojarzony z wiadomością
    Store: Boolean; // Zachowana na dysku tak/nie
    Kind: Byte; // Rodzaj wiadomości ##025
    ShowAsOutgoing: Boolean; // Pokaż wiadomość jako wychodzącą
  end;

type
  PPluginMsgPic = ^TPluginMsgPic;

  TPluginMsgPic = record
    cbSize: Integer; // Wielkość struktury
    FilePath: PWideChar; // Ścieżka do pliku
    Description: PWideChar; // Opis
    ID: PWideChar; // ID
  end;

type
  PPluginFileTransfer = ^TPluginFileTransfer;

  TPluginFileTransfer = record
    cbSize: Integer; // Wielkość struktury
    FilePath: PWideChar; // Ścieżka do pliku
    Description: PWideChar; // Opis
    ID: PWideChar; // ID
  end;

type
  PPluginMicroMsg = ^TPluginMicroMsg;

  TPluginMicroMsg = record
    cbSize: Integer; // Wielkość struktury
    Msg: PWideChar; // Wiadomość sofrmatowana do postaci zgodnej z HTML.
    SaveToArchive: Boolean; // Czy ma zostaćzapisana do archiwum TRUE/FALSE
  end;

type
  PPluginInfo = ^TPluginInfo;

  TPluginInfo = record
    cbSize: Integer; // Wielkość struktury
    ShortName: PWideChar; // Krótka nazwa wtyczki
    Version: DWORD; // Wersja wtyczki
    Description: PWideChar; // Krótki opis wtyczki
    Author: PWideChar; // Imie i nazwisko autora
    AuthorMail: PWideChar; // Kontakt do autora (e-mail)
    Copyright: PWideChar; // Do kogo należą prawa autorskie
    Homepage: PWideChar; // Strona domowa projektu
    Flag: Byte; // Dodatkowe flagi
    ReplaceDefaultModule: Integer; // Nie używane
  end;

type
  PPluginActionEdit = ^TPluginActionEdit;

  TPluginActionEdit = record
    cbSize: Integer; // Wielkość struktury
    pszName: PWideChar; // Nazwa akcji
    Caption: PWideChar; // Etykieta
    Hint: PWideChar; // Hint, pomoc.
    Enabled: Boolean; // Akcja aktywna tak/nie
    Visible: Boolean; // Akcja widoczna tak/nie
    IconIndex: Integer; // Index ikony
    Checked: Boolean; // Zaznaczenie tak/nie
  end;

type
  PPluginAccountInfo = ^TPluginAccountInfo;
  TPluginAccountInfo = record
    cbSize: Integer; // Wielkość struktury
    Name: PWideChar;  // Nazwa konta
    JID: PWideChar; // JabberID
    Status: PWideChar; // Status tekstowy
    ShowType: Integer; // Status
    IconIndex: Integer;  // Aktualny index ikony stanu
  end;

type
  PPluginWindowStatus = ^TPluginWindowStatus;

  TPluginWindowStatus = record
    cbSize: Integer; // Wielkość struktury
    Status: Integer;  // Status
    TextStatus: PWideChar; // Status tekstowy
    AllAccounts: Boolean; // Zastosuj dla wszystkich kont tak/nie
    OnlyNote: Boolean; // Zmień tylko opis tak/nie
  end;

type
  PPluginAction = ^TPluginAction;

  TPluginAction = record
    cbSize: Integer; // Wielkość struktury
    Action: PWideChar; // Nazwa akcji
    pszName: PWideChar; // Nazwa unikatowa
    pszCaption: PWideChar; // Etykieta
    Flags: DWORD; // Dodatkowe flagi
    Position: Integer; // Index pozycji w menu
    IconIndex: Integer; // Index ikony
    pszService: PWideChar; // Nazwa serwisu do uruchomienia
    pszPopupName: PWideChar; // Nazwa menu nadrzędnego
    popupPosition: Integer; // Pozycja w menu nadrzędnym
    hotKey: DWORD; // HotKey
    pszContactOwner: PWideChar;
    GroupIndex: Integer; // Index grupy
    Grouped: Boolean; // Czy w grupie tak/nie
    AutoCheck: Boolean; // Auto zaznaczanie tak/nie
    Checked: Boolean; // Zaznaczenie aktywne tak/nie
    Handle: Cardinal; // Uchwyt
    ShortCut: PWideChar; // Skrót
    Hint: PWideChar; // Pomoc, hint.
    PositionAfter: PWideChar; // Ustaw w menu po podanej innej akcji
  end;

type
  PPluginHTTPRequest = ^TPluginHTTPRequest;

  TPluginHTTPRequest = record
    cbSize: Integer; // Wielkość struktury
    ProtocolVersion: Byte; // Wersja protokołu 0 = HTTP 1.0; 1 = HTTP 1.1
    Method: String; // Metoda POST / GET
    URL: String; // Adres URL
    AllowCookies: Boolean; // Obsługa ciasteczek
    ForceEncodeParams: Boolean; // Wymuszanie encji HTML przy metodzie GET
    HandleRedirects: Boolean; // Obsługa przekierowań
    HeaderUserAgent: PWidechar; // HTTP UserAgent
    HeaderAccept: PWideChar; // HTTP Accept
    HeaderAcceptLanguage: PWideChar; // HTTP Accept-language
    HeaderAcceptEncoding: PWideChar; // HTTP Accep-encoding
    HeaderAcceptCharset: PWideChar; // HTTP Accept-charset
    HeaderContentType: PWideChar; // HTTP content-type
    HeaderReferer: PWideChar; // HTTP Referer
    ResultLocation: Boolean; // Zwrot w postaci ostatniej lokalizacji
    CustomHeaders: PWideChar; // Własne nagłówki oddzielone od siebie znakiem |
    HTTPPayLoad: PWideChar; // Dane do wysyłki w trkacie korzystania z meotdy POST
    BasicAuth: Boolean; // Jeżeli korzystamy z autoryzacji ustawiamy na True.
    BasicAuthLogin: PWideChar; // Login potrzebny do autoryzacji.
    BasicAuthPass: PWideChar; // Hasło potrzebne przy autoryzacji.
  end;

type
  PPluginAccountEvents = ^TPluginAccountEvents;

  TPluginAccountEvents = record
    cbSize: Integer; // Wielkość struktury
    DisplayName: PWideChar; // Nazwa konta
    IconIndex: Integer; // Index ikony
    EventNew: Boolean; // Można tworzyć nowe konta tak/nie
    EventEdit: Boolean; // Można edytować konto tak/nie
    EventDelete: Boolean; // Można usuwać konto tak/nie
    EventPassChange: Boolean; // Można zmieniać hasło tak/nie
    EventDefault: Boolean; // Domyślny event np. uruchomienie ustawień konta tak/nie
  end;

type
  PPluginContact = ^TPluginContact;

  TPluginContact = record
    cbSize: Integer; // Wielkość struktury
    JID: PWideChar; // JabberID kontaktu
    Nick: PWideChar; // Pseudonim
    Resource: PWideChar; // Zasób z którego łączy się kontakt
    Groups: PWideChar; // Grupy do których przynależy kontakt
    State: Integer; // Status kontaktu ##016
    Status: PWideChar; // Status opisowy kontaktu
    Temporary: Boolean; // Kontakt tymczasowy (nie jest zapisany na liście kontaktów)
    FromPlugin: Boolean;  // Kontakt pochodzi z wtyczki tak/nie
    UserIdx: Integer; // ID sieci do której przynależy kontakt
    Subscription: Byte; // Rodzaj subskrybcji ##028
    IsChat: Boolean; // Kontakt czatowy tak/nie
  end;

type
  PPluginNewsData = ^TPluginNewsData;
  TPluginNewsData = record
    Kind: PWideChar; // Unikatowy identyfikator (stały)
    Title: PWideChar; // Nazwa źródła powiadomień
    ID: PWideChar; // Unikatowe ID
    Active: Boolean; // Źródło aktywne tak/nie
    ImageIndex: Integer; // Indeks ikony, lub -1 gdy AQQ ma zdecydować o wyglądzie ikony
  end;

type
  PPluginForm = ^TPluginForm;
  TPluginForm = record
    cbSize: Integer; // wielkość struktury
    Handle: Integer; // uchwyt do okna dialogowego
    Name: PWideChar; // nazwa okna dialogowego
    Caption: PWideChar; // tytuł okna dialogowego
    Width: Integer; // szerokość okna
    Height: Integer; // wysokość okna
  end;

type
  TPluginFont = record
    UseCustomFontSize: Boolean; // czy korzystać z wielkości cziocnki wskazanej przez wtyczkę
    CustomFontSize: Integer; // wielkość czcionki
    UseCustomFontFace: Boolean; // czy korzystać z rodzaju czcionki wskazanej przez wtyczkę
    CustomFontFace: PWideChar; // rodzaj czcionki
    UseCustomFontColor: Boolean; // czy korzystać z koloru czcionki wskazanej przez wtyczkę
    CustomFontColor: Integer; // kolor czcionki
    FontBold: Boolean; // czcionka pogrubiona
    FontUnderLine: Boolean; // czcionka podkreślona
  end;

type
  TPluginRect = record
    Left: Integer; // wymiar od lewej
    Top: Integer;  // wymiar od góry
    Right: Integer; // wymiar od lewej + długość
    Bottom: Integer; // wymiar od góry + długość
  end;

type
  PPluginControl = ^TPluginControl;
  TPluginControl = record
    cbSize: Integer; // wielkość struktury
    Handle: Integer; // uchyt do kontrolki (jeżeli posiada)
    FormParentHandle: Integer; // uchwyt do rodzica (okno dialogowe na którym znajduję się kontrolka)
    ClassName: PWideChar; // klasa kontrolki np. TLabel, TCheckBox, TButton itd.
    Name: PWideChar; // nazwa kontrolki
    Text: PWideChar; // tekst w kontrolce
    Caption: PWideChar; // etykieta kontrolki
    Font: TPluginFont; // informacje o czcionce
    ModalResult: Integer; // stała modalnego zwrotu ##032
    Checked: Boolean; // zaznacozny [tak/nie] - o ile możliwe
    Enabled: Boolean; // kontrolka aktywna [tak/nie]
    BoundsRect: TPluginRect; // Wymiary i pozycja kontrolki
    AutoSize: Boolean; // Długość i wysokość niektórych kontrolek może być automatyczna.
    PassField: Boolean; // Czy poled edycyjne TEdit jest polem zawierającym hasło?
  end;

type
  PPluginNewsItem = ^TPluginNewsItem;
  TPluginNewsItem = record
    Date: TDatetime; // Data publikacji powiadomienia
    News: PWideChar; // Treść powiadomienia
    Title: PWideChar; // Tytuł powiadomienia
    Source: PWideChar;  // Źródło np. link URL
    ParentIndex: Integer; // ID otrzymane w notyfikacji
  end;

type
  PPluginExecMsg = ^TPluginExecMsg;
  TPluginExecMsg = record
    JID: PWideChar; // JID kontaktu
    UserIdx: Integer; // Index konta
    ActionSwitchTo: Boolean; // Domyślna akcja, przełącza zakładkę lub otwiera okno rozmowy
    ActionCloseWindow: Boolean; // Akcja ma zamknąć okno rozmowy a nie je otworzyć
    ActionTabIndex: Boolean; // Akcja zwraca index karty na zakładkach w oknie rozmowy
    ActionTabWasClosed: Boolean; // Akcja dodaje kontakt do listy zamkniętych ostatnio zakładek. Lista ta wykorzystywana jest m.inn. w dymku ikony AQQ w zasobniku systemowym.
    IsPriority: Boolean; // Okno powinno zostać wywołane na pierwszy plan. Dotyczy akcji ActionSwitchTo.
    IsFromPlugin: Boolean; // Czy kontakt pochodzi z wtyczki? (wymagane!)
  end;

type
  PPluginPrivacyItem = ^TPluginPrivacyItem;
  TPluginPrivacyItem = record
    ItemType: Byte; // Typ elementu: 0 brak 1 JID 2 Grupa 3 Subskrypcje
    Value: PWideChar; // Wartość elementu
    Msg: Byte; // Pakiety typu message: 0 nie ustawione 1 pozwalaj 2 blokuj
    PresIn: Byte; // Pakiety typu presence przychodzące: 0 nie ustawione 1 pozwalaj 2 blokuj
    PresOut: Byte; // Pakiety typu presence wychodzące: 0 nie ustawione 1 pozwalaj 2 blokuj
    IQ: Byte; // Pakiety typu IQ: 0 nie ustawione 1 pozwalaj 2 blokuj
  end;

type
  PAQQHook = ^TAQQHook;
  TAQQHook = function(wParam: Windows.WPARAM; lParam: Windows.LPARAM): NativeInt; stdcall; // Uchwyt do notyfikacji

type
  PAQQService = ^TAQQService;
  TAQQService = function(wParam: Windows.WPARAM; lParam: Windows.LPARAM): NativeInt; stdcall; // Funkcja serwisowa

type
  PPluginLink = ^TPluginLink;

  TPluginLink = record
    Path: PWideChar; // Wskaźnik do ścieżki (razem z nazwą pliku), wczytanej wtyczki.
    CreateHookableEvent: function(Name: PWideChar): THandle; stdcall;
    DestroyHookableEvent: function(hEvent: THandle): NativeInt; stdcall;
    NotifyEventHooks: function(hEvent: THandle; wParam, lParam: DWORD): NativeInt; stdcall;
    HookEvent: function(Name: PWideChar; HookProc: TAQQHook): THandle; stdcall; // Funkcja podłączająca interfejs wtyczki pod wskazaną notyfikację w interfejsie AQQ.
    HookEventMessage: function(Name: PWideChar; Handle: HWND; Msg: Cardinal): THandle; stdcall;
    UnhookEvent: function(hHook: THandle): Integer; stdcall; // Funkcja odłączająca notyfikacje w interfejsie AQQ od interfejsu wtyczki.
    CreateServiceFunction: function(Name: PWideChar; ServiceProc: TAQQService): THandle; stdcall; // Tworzy funkcję serwisową (serwis).
    CreateTransientServiceFunction: Pointer;
    DestroyServiceFunction: function(hService: THandle): NativeInt; stdcall; // Niszczy funkcję serwisową (serwis).
//    CallService: function(Name: PWideChar; wParam, lParam: DWORD): Integer; stdcall; // Wywołuję serwis.
    CallService: function(Name: PWideChar; wParam: Windows.LPARAM; lParam: Windows.WPARAM): NativeInt; stdcall;
    ServiceExists: function(Name: PWideChar): NativeInt; stdcall;
  end;

/// @---------------------------------------------------------------------------

const
  CALLSERVICE_NOTFOUND = $80000000;
  MAXMODULELABELLENGTH = 64;

type
  TVersionInfo = record
    a, b, c, d: Integer end;

  type
    TAQQPluginInfo = function(AQQVersion: DWORD): PPluginInfo; stdcall;
    TLoad = function(Link: PPluginLink): NativeInt; stdcall;
    TUnload = function: NativeInt; stdcall;
    TSettings = function: NativeInt; stdcall;

    // ------------------------------------------------------------------------------

  function PLUGIN_MAKE_VERSION(a, b, c, d: Integer): DWORD;
  function PLUGIN_GET_VERSION(Ver: DWORD): TVersionInfo;
  function CompareVersion(vera, verb: DWORD): Integer;

implementation

function PLUGIN_MAKE_VERSION(a, b, c, d: Integer): DWORD;
begin
  Result := (((a and $FF) shl 24) or ((b and $FF) shl 16) or ((c and $FF) shl 8) or (d and $FF));
end;

function PLUGIN_GET_VERSION(Ver: DWORD): TVersionInfo;
begin
  Result.d := (Ver and $000000FF);
  Result.c := (Ver and $0000FF00) shr 8;
  Result.b := (Ver and $00FF0000) shr 16;
  Result.a := (Ver and $FF000000) shr 24;
end;

function CompareVersion(vera, verb: DWORD): Integer;
{ compares 2 versions. if versiona is newer than version b then the result is
  greater than 0, otherwise it's smaller }
begin
  Result := 0;
  if PLUGIN_GET_VERSION(vera).a > PLUGIN_GET_VERSION(verb).a then
    Inc(Result, 100);
  if PLUGIN_GET_VERSION(vera).a < PLUGIN_GET_VERSION(verb).a then
    Dec(Result, 100);

  if PLUGIN_GET_VERSION(vera).b > PLUGIN_GET_VERSION(verb).b then
    Inc(Result, 50);
  if PLUGIN_GET_VERSION(vera).b < PLUGIN_GET_VERSION(verb).b then
    Dec(Result, 50);

  if PLUGIN_GET_VERSION(vera).c > PLUGIN_GET_VERSION(verb).c then
    Inc(Result, 10);
  if PLUGIN_GET_VERSION(vera).c < PLUGIN_GET_VERSION(verb).c then
    Dec(Result, 10);

  if PLUGIN_GET_VERSION(vera).d > PLUGIN_GET_VERSION(verb).d then
    Inc(Result, 1);
  if PLUGIN_GET_VERSION(vera).d < PLUGIN_GET_VERSION(verb).d then
    Dec(Result, 1);
end;

end.
