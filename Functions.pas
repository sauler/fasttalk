unit Functions;

interface

uses
    Windows, SysUtils, StrUtils, Forms, Registry, ShellAPI, ShlObj, ActiveX,
    comobj,
    PluginAPI, LangAPI, AQQFunctions, Varibles, StyleManagerUnit,
    UpdateManagerUnit, ImageToolUnit;

{ ITEM IN CONTACT POPUP MENU }
procedure AddPopupItem();
procedure EditPopupItem(name: String);
procedure DeletePopupItem();

{ LANGUAGE FUNCTIONS }
procedure SetupLanguage();

{ ICONS FUNCTIONS }
procedure LoadIcons();
procedure UnloadIcons();
procedure CheckIcons();

{ DIRECTORIES FUNCTIONS }
function GetDirectories(): TFastTalkDir;

{ VARIBLES }
procedure InitVaribles();
procedure FreeVaribles();

{ STINGS FUNCTIONS }
function NormalizeNick(nick: String): String;
function NormalizeJID(jid: String): String;

{ PLUGIN INSTALL FUNCTIONS }
procedure VerifyFiles();
procedure UpdatePlugin();
procedure InstallDefaultStyles();

{ TOOLS }
procedure Log(t: String);
function FileNameWithoutExt(const FileName: string): string;

{ DESKTOP FUNCTIONS }
function GetDesktopPath(): String;
procedure CreateLink(Target, Args, WorkDir, ShortcutName, iconPath: String);
procedure RefreshDesktop();

implementation

{ *********************** ITEM IN CONTACT POPUP MENU ************************* }

// Add item in contact popup
procedure AddPopupItem();
var
    Item: PPluginAction;
begin
    PopupItem.cbSize := SizeOf(TPluginAction);
    PopupItem.IconIndex := Icons[ITEM_ICON];
    PopupItem.pszName := 'FastTalkPopUpItem';
    PopupItem.pszService := FASTTALK_SERVICE;
    PopupItem.pszCaption := PWideChar(GetLangStr('PopUpItem'));
    PopupItem.pszPopupName := 'muItem';
    PopupItem.PositionAfter := 'N4';
    CreatePopUpMenuItem(PopupItem);
end;

// Edit item in contact popup
procedure EditPopupItem(name: String);
begin
    ZeroMemory(@PopupItemEdit, SizeOf(TPluginActionEdit));
    PopupItemEdit.cbSize := SizeOf(TPluginActionEdit);
    PopupItemEdit.IconIndex := Icons[ITEM_ICON];
    PopupItemEdit.pszName := 'FastTalkPopUpItem';
    if (name = '') then
        PopupItemEdit.Caption := PWideChar(GetLangStr('PopUpItem'))
    else
        PopupItemEdit.Caption := PWideChar(name);
    PopupItemEdit.Enabled := true;
    PopupItemEdit.Visible := true;
    PopupItemEdit.Checked := false;
    PluginLink.CallService(AQQ_CONTROLS_EDITPOPUPMENUITEM, 0,
        lParam(@PopupItemEdit));
end;

// Delete item in contact popup
procedure DeletePopupItem();
begin
    ZeroMemory(@PopupItem, SizeOf(TPluginAction));
    PopupItem.cbSize := SizeOf(TPluginAction);
    PopupItem.pszName := 'FastTalkPopUpItem';
    DestroyPopUpMenuItem(PopupItem);
end;

{ *************************** LANGUAGE FUNCTIONS ***************************** }

// Set lang file directory
procedure SetupLanguage();
var
    LangCode: String;
    PlugPath: String;
begin
    PlugPath := GetPluginDir();

    LangCode := PWideChar
        (PluginLink.CallService(AQQ_FUNCTION_GETLANGCODE, 0, 0));
    LangPath := PlugPath + '\Languages\FastTalk\' + LangCode + '\';
    if not(DirectoryExists(LangPath)) then
    begin
        LangCode :=
            PWideChar(PluginLink.CallService
            (AQQ_FUNCTION_GETDEFLANGCODE, 0, 0));
        LangPath := PlugPath + '\Languages\FastTalk\' + LangCode + '\';
    end;
end;

{ **************************** ICONS FUNCTIONS ******************************* }

// Load plugin icons to AQQ
procedure LoadIcons();
begin
    Icons[ITEM_ICON] :=
        LoadPNG(PWideChar(GetPluginDir() + '\FastTalk\item_icon.png'));
end;

// Unload plugin icons from AQQ
procedure UnloadIcons();
begin
    UnloadPNG(Icons[ITEM_ICON]);
end;

// Checks if active theme have FastTalk icons
procedure CheckIcons();
begin
    if (DirectoryExists(GetThemeDir() + '\FastTalk\Icons')) and
        (FileExists(GetThemeDir() + '\FastTalk\Icons\item_icon.png')) then
        ReplacePNG(Icons[ITEM_ICON],
            PWideChar(GetThemeDir() + '\FastTalk\Icons\item_icon.png'))
    else
        ReplacePNG(Icons[ITEM_ICON],
            PWideChar(GetPluginDir() + '\FastTalk\item_icon.png'));
end;

{ ************************* DIRECTORIES FUNCTIONS **************************** }

// Get all FastTalk directories
function GetDirectories(): TFastTalkDir;
begin
    Result.Default := String(GetPluginDir() + '\FastTalk');
    Result.Avatars := String(GetPluginDir() + '\FastTalk\Avatars');
    Result.Styles := String(GetPluginDir() + '\FastTalk\Styles');
    Result.Utils := String(GetPluginDir() + '\FastTalk\Utils');
end;

{ ******************************** VARIBLES ********************************** }

// Init varibles
procedure InitVaribles();
begin
    StyleManager := TStyleManager.Create;
    StyleManager.StylesDir := GetDirectories().Styles;
    StyleManager.DataFile := 'Styles.ini';

    UpdateManager := TUpdateManager.Create;
    UpdateManager.FilesDir := GetDirectories().Default + '\UpdateList';
    UpdateManager.GetUpdateList();

    ImageTool := TImageTool.Create;
    ImageTool.toolPath := GetDirectories().Utils + '\convert.exe';
    ImageTool.avatarsDir := GetDirectories().Avatars;
end;

// FreeVaribles
procedure FreeVaribles();
begin
    StyleManager.Free;
    UpdateManager.Free;
    ImageTool.Free;
end;

{ *************************** STRINGS FUNCTIONS ****************************** }

// Delete illegal characters from string
function NormalizeNick(nick: String): String;
var
    temp: String;
begin

    temp := StringReplace(nick, '/', '', [rfReplaceAll]);
    temp := StringReplace(temp, '\', '', [rfReplaceAll]);
    temp := StringReplace(temp, ':', '', [rfReplaceAll]);
    temp := StringReplace(temp, '*', '', [rfReplaceAll]);
    temp := StringReplace(temp, '?', '', [rfReplaceAll]);
    temp := StringReplace(temp, '"', '', [rfReplaceAll]);
    temp := StringReplace(temp, '<', '', [rfReplaceAll]);
    temp := StringReplace(temp, '>', '', [rfReplaceAll]);
    Result := temp;
end;

// Delete illegal characters from string
function NormalizeJID(jid: String): String;
begin
    if (AnsiContainsStr(jid, '-')) then
    begin
        jid := AnsiReplaceText(jid, '-', ' ');
        jid := Trim(jid);
        Result := jid;
    end;
end;

{ *********************** PLUGIN INSTALL FUNCTIONS *************************** }

// Check if plugin files exists on disk. If these doesn't exist this function
// can create it.
procedure VerifyFiles();
begin
    if (FileExists(GetPluginDir() + '\FastTalk\Log.txt')) then
        DeleteFile(GetPluginDir() + '\FastTalk\Log.txt');

    if not(DirectoryExists(GetPluginDir() + '\Shared')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\Shared'));

    if not(DirectoryExists(GetPluginDir() + '\Languages')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\Languages'));

    if not(DirectoryExists(GetPluginDir() + '\Languages\FastTalk')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\Languages\FastTalk'));

    if not(DirectoryExists(GetPluginDir() + '\Languages\FastTalk\EN')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\Languages\FastTalk\EN'));

    if not(DirectoryExists(GetPluginDir() + '\Languages\FastTalk\PL')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\Languages\FastTalk\PL'));

    if not(DirectoryExists(GetPluginDir() + '\FastTalk')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\FastTalk'));

    if not(DirectoryExists(GetPluginDir() + '\FastTalk\Avatars')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\FastTalk\Avatars'));

    if not(DirectoryExists(GetPluginDir() + '\FastTalk\Styles')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\FastTalk\Styles'));

    if not(DirectoryExists(GetPluginDir() + '\FastTalk\Utils')) then
        ForceDirectories(PWideChar(GetPluginDir() + '\FastTalk\Utils'));

    if not(FileExists(GetPluginDir() + '\Shared\FastTalk.dll.png')) then
        ExtractRes(PWideChar(GetPluginDir() + '\Shared\FastTalk.dll.png'),
            PWideChar('MAIN_ICON'), RT_RCDATA);

    if not(FileExists(GetPluginDir() + '\FastTalk\item_icon.png')) then
        ExtractRes(PWideChar(GetPluginDir() + '\FastTalk\item_icon.png'),
            PWideChar('ITEM_ICON'), RT_RCDATA);

    if not(FileExists(GetPluginDir() + '\FastTalk\Utils\convert.exe')) then
        ExtractRes(PWideChar(GetPluginDir() + '\FastTalk\Utils\convert.exe'),
            PWideChar('CONV'), RT_RCDATA);
    if not(FileExists(GetDirectories.Styles + '\Styles.ini')) then
    begin
        if not(FileExists(GetPluginDir() + '\FastTalk\Styles\Messenger.zip'))
        then
            ExtractRes(PWideChar(GetPluginDir() +
                '\FastTalk\Styles\Messenger.zip'), PWideChar('MESSENGER'),
                RT_RCDATA);

        if not(FileExists(GetPluginDir() + '\FastTalk\Styles\Square.zip')) then
            ExtractRes(PWideChar(GetPluginDir() +
                '\FastTalk\Styles\Square.zip'), PWideChar('SQUARE'), RT_RCDATA);
    end;
    if not(FileExists(GetPluginDir() + '\Languages\FastTalk\PL\Const.lng')) then
        ExtractRes(PWideChar(GetPluginDir() +
            '\Languages\FastTalk\PL\Const.lng'), PWideChar('ConstPL'),
            RT_RCDATA);

    if not(FileExists(GetPluginDir() + '\Languages\FastTalk\EN\Const.lng')) then
        ExtractRes(PWideChar(GetPluginDir() +
            '\Languages\FastTalk\EN\Const.lng'), PWideChar('ConstEN'),
            RT_RCDATA);

    if not(FileExists(GetPluginDir() + '\Languages\FastTalk\PL\TfrmChoose.lng'))
    then
        ExtractRes(PWideChar(GetPluginDir() +
            '\Languages\FastTalk\PL\TfrmChoose.lng'), PWideChar('TfrmChoosePL'),
            RT_RCDATA);

    if not(FileExists(GetPluginDir() + '\Languages\FastTalk\EN\TfrmChoose.lng'))
    then
        ExtractRes(PWideChar(GetPluginDir() +
            '\Languages\FastTalk\EN\TfrmChoose.lng'), PWideChar('TfrmChooseEN'),
            RT_RCDATA);
end;

// Function called when plugin is updated from AQQ
procedure UpdatePlugin();
var
    pluginDir: String;
begin
    pluginDir := GetPluginDir();
    DeleteFile(pluginDir + '\Languages\FastTalk\EN\Const.lng');
    DeleteFile(pluginDir + '\Languages\FastTalk\PL\Const.lng');
    DeleteFile(pluginDir + '\Languages\FastTalk\EN\TfrmChoose.lng');
    DeleteFile(pluginDir + '\Languages\FastTalk\PL\TfrmChoose.lng');
    VerifyFiles();
end;

// Function called when default FastTalk styles aren't installed
procedure InstallDefaultStyles();
var
    StyleManager: TStyleManager;
    stylePath: String;
begin
    StyleManager := TStyleManager.Create;
    try
        StyleManager.StylesDir := GetDirectories().Styles;
        StyleManager.DataFile := 'Styles.ini';
        stylePath := GetDirectories().Styles + '\Messenger.zip';
        if not(DirectoryExists(StyleManager.StylesDir + '\' +
            AnsiReplaceText(ExtractFileName(stylePath),
            ExtractFileExt(stylePath), ''))) then
        begin
            StyleManager.InstallStyle(stylePath);
            DeleteFile(stylePath);
        end;

        stylePath := GetDirectories().Styles + '\Square.zip';
        if not(DirectoryExists(StyleManager.StylesDir + '\' +
            AnsiReplaceText(ExtractFileName(stylePath),
            ExtractFileExt(stylePath), ''))) then
        begin
            StyleManager.InstallStyle(stylePath);
            DeleteFile(stylePath);
        end;
    finally
        StyleManager.Free;
    end;
end;

{ ********************************* TOOLS ************************************ }

// Log text to file
procedure Log(t: String);
var
    f: TextFile;
begin
    AssignFile(f, GetPluginDir() + '\FastTalk\log.txt');
    if not(FileExists(GetPluginDir() + '\FastTalk\log.txt')) then
        Rewrite(f)
    else
        Append(f);

    writeln(f, TimeToStr(Now) + ': ' +t);
    CloseFile(f);
end;

// Function removes file extansion
function FileNameWithoutExt(const FileName: string): string;
begin
    Result := ChangeFileExt(ExtractFileName(FileName), '');
end;

{ *************************** DESKTOP FUNCTIONS ****************************** }

// Function return desktop path
function GetDesktopPath(): String;
var
    reg: TRegistry;
begin
    reg := TRegistry.Create;
    try
        reg.OpenKeyReadOnly
            ('Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders');
        Result := reg.ReadString('Desktop');
    finally
        reg.Free;
    end;
end;

// Create shortcut on desktop
procedure CreateLink(Target, Args, WorkDir, ShortcutName, iconPath: String);
var
    IObj: IUnknown;
    Link: IShellLink;
    IPFile: IPersistFile;
    TargetW: WideString;
begin
    IObj := CreateComObject(CLSID_ShellLink);
    Link := IObj as IShellLink;
    IPFile := IObj as IPersistFile;
    with Link do
    begin
        SetPath(PChar(Target));
        SetArguments(PChar(Args));
        SetShowCmd(SW_SHOW);
        SetWorkingDirectory(PChar(WorkDir));
        SetIconLocation(PWideChar(iconPath), 0);
    end;
    TargetW := ShortcutName;
    IPFile.Save(PWChar(TargetW), false);
end;

// Refresh desktop
procedure RefreshDesktop();
begin
    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, 0, 0);
end;

end.
