library FastTalk;
{$R 'resources.res' 'resources.rc'}
{$R *.res}

uses
    Windows,
    System.SysUtils,
    System.Classes,
    PluginAPI,
    Varibles in 'Varibles.pas',
    Hooks in 'Hooks.pas',
    Functions in 'Functions.pas',
    frmChooseStyle in 'frmChooseStyle.pas' {frmChoose} ,
    ImageToolUnit in 'ImageToolUnit.pas',
    AvatarFunctions in 'AvatarFunctions.pas';

function AQQPluginInfo(AQQVersion: DWord): PPluginInfo; stdcall;
begin
    PluginInfo.cbSize := SizeOf(TPluginInfo);
    PluginInfo.ShortName := PLUGIN_SHORTNAME;
    PluginInfo.Version := PLUGIN_MAKE_VERSION(1, 1, 0, 6);
    PluginInfo.Description := PLUGIN_DESCRIPTION;
    PluginInfo.Author := PLUGIN_AUTHOR;
    PluginInfo.AuthorMail := PLUGIN_AUTHORMAIL;
    PluginInfo.Copyright := PLUGIN_COPYRIGHT;
    PluginInfo.Homepage := '';
    PluginInfo.Flag := 0;
    PluginInfo.ReplaceDefaultModule := 0;

    Result := @PluginInfo;
end;

function Load(Link: PPluginLink): Integer; stdcall;
begin
    Result := 0;
    PluginLink := Link^;

    VerifyFiles();
    SetupLanguage();
    LoadIcons();
    CheckIcons();
    InitVaribles();
    AddHooks();
    RegisterServices();
    AddPopupItem();

    if not(FileExists(GetDirectories().Styles + '\Styles.ini')) then
        InstallDefaultStyles();

end;

function Unload: Integer; stdcall;
begin
    Result := 0;
    DeletePopupItem();
    UnregisterServices();
    DeleteHooks();
    FreeVaribles();
    UnloadIcons();
end;

exports
    Load,
    Unload,
    AQQPluginInfo;

begin

end.
