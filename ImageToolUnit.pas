unit ImageToolUnit;

interface

uses
    Windows, ShellAPI, SysUtils, PNGImage, Math,
    Classes, FMX.Graphics, Vcl.Graphics, acAlphaImageList, Dialogs;

const
    AQQ_OFF = 255; // const for status varible, used when AQQ is disabled

type
    TImageTool = class
        avatarPath: String;
        avatarsDir: String;
        toolPath: String;
        JID: String;
        styleName: String;
        procedure MakeIconFromImage(status: Integer);
        procedure MakeLivePreview(status: Integer; imageList: TsAlphaImageList);
    private
        function GetStatusIconFile(status: Integer): String;
        function GetMaskFile(): String;
        function GetOffMaskFile(): String;
        function GetOutputDir(): String;
        procedure ApplyMask(name: String; mask: String);
        procedure PNG2ICO(pngPath, icoPath: String);
    end;

    { SOME WTF FUNCTIONS }
procedure DrawPngWithAlpha(Src, Dest: TPngImage; const R: TRect);
procedure LoadPNGAndDrawIcon(tPath, avatar, icon, save_path: string);
procedure ResizePng(apng: TPngImage; NuWidth, NuHeight: Integer);
procedure ConvertPngToBmp(Src: TPngImage; Dest: TBitmap);
procedure LoadPNGAndApplyMask(avatar, mask, save_path: string);

implementation

uses
    AQQFunctions, Functions;

// Make icon from image
procedure TImageTool.MakeIconFromImage(status: Integer);
var
    savePath: String;
    saveIcoPath: String;
begin
    if (status = AQQ_OFF) then
    begin
        savePath := GetOutputDir() + '\off.png';
        saveIcoPath := GetOutputDir() + '\off.ico';
    end
    else
    begin
        savePath := GetOutputDir() + '\' + IntToStr(status) + '.png';
        saveIcoPath := GetOutputDir() + '\' + IntToStr(status) + '.ico';
    end;

    if (status = AQQ_OFF) then
    begin
        savePath := GetOutputDir() + '\off.png';
        saveIcoPath := GetOutputDir() + '\off.ico';
        // For no_avatar file we doesn't use transpanency
        if (ExtractFileName(avatarPath) = 'no_avatar.png') then
            LoadPNGAndApplyMask(avatarPath, GetOffMaskFile(), savePath)
        else
            ApplyMask('off', GetOffMaskFile());

    end
    else
    begin
        savePath := GetOutputDir() + '\' + IntToStr(status) + '.png';
        saveIcoPath := GetOutputDir() + '\' + IntToStr(status) + '.ico';
        // For no_avatar file we doesn't use transpanency
        if (ExtractFileName(avatarPath) = 'no_avatar.png') then
            LoadPNGAndApplyMask(avatarPath, GetMaskFile(), savePath)
        else
            ApplyMask(IntToStr(status), GetMaskFile());

        LoadPNGAndDrawIcon(toolPath, savePath, GetStatusIconFile(status),
            savePath);
    end;

    PNG2ICO(savePath, saveIcoPath);
end;

// Make live preview to show in shortcut creating window
procedure TImageTool.MakeLivePreview(status: Integer;
    imageList: TsAlphaImageList);
var
    savePath: String;
begin
    savePath := GetOutputDir() + '\preview.png';

    if (status = AQQ_OFF) then
    begin
        // For no_avatar file we doesn't use transpanency
        if (ExtractFileName(avatarPath) = 'no_avatar.png') then
            LoadPNGAndApplyMask(avatarPath, GetOffMaskFile(), savePath)
        else
            ApplyMask('preview', GetOffMaskFile());

    end
    else
    begin
        // For no_avatar file we doesn't use transpanency
        if (ExtractFileName(avatarPath) = 'no_avatar.png') then
            LoadPNGAndApplyMask(avatarPath, GetMaskFile(), savePath)
        else
            ApplyMask('preview', GetMaskFile());

        LoadPNGAndDrawIcon(toolPath, savePath, GetStatusIconFile(status),
            savePath);
    end;

    imageList.Items.Clear;
    imageList.LoadFromFile(savePath);
end;

// Get path to status icon file
// status - contact status
function TImageTool.GetStatusIconFile(status: Integer): String;
begin
    Result := GetDirectories().Styles + '\' + styleName + '\Icons\' +
        IntToStr(status) + '.png';
end;

// Get path to mask file
function TImageTool.GetMaskFile(): String;
begin
    Result := GetDirectories().Styles + '\' + styleName + '\mask.png';
end;

// Get path to off mask file
function TImageTool.GetOffMaskFile(): String;
begin
    Result := GetDirectories().Styles + '\' + styleName + '\mask_off.png';
end;

// Get output dir for JID
function TImageTool.GetOutputDir(): String;
var
    output: String;
begin
    output := avatarsDir + '\' + JID + '\' + styleName;
    if not(DirectoryExists(output)) then
        ForceDirectories(output);
    Result := output;
end;

// Apply mask on the image
procedure TImageTool.ApplyMask(name: String; mask: String);
var
    tempFile: String;
    sei: TShellExecuteInfo;
    ExitCode: DWORD;
begin

    tempFile := GetOutputDir() + '\temp.png';

    ZeroMemory(@sei, SizeOf(sei));
    sei.cbSize := SizeOf(TShellExecuteInfo);
    sei.Wnd := 0;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or
        SEE_MASK_NOCLOSEPROCESS;
    sei.lpVerb := PChar('open');
    sei.lpFile := PChar(toolPath);

    sei.lpParameters := PWideChar('"' + avatarPath + '" -resize 128x128! "' +
        tempFile + '"' + ' & "' + toolPath + '" "' + tempFile +
        '" -alpha on ( +clone -channel a -fx 0 ) +swap "' + mask +
        '" -composite "' + GetOutputDir() + '\' + name + '.png"');

    sei.nShow := SW_HIDE;

    if ShellExecuteEx(@sei) then
    begin
        repeat
            AQQProcessMessages;
            GetExitCodeProcess(sei.hProcess, ExitCode);
        until (ExitCode <> STILL_ACTIVE);
    end;
end;

// Convert PNG file to icon (ICO file)
procedure TImageTool.PNG2ICO(pngPath, icoPath: String);
begin
    ShellExecute(0, 'open', PWideChar(toolPath),
        PWideChar('"' + pngPath +
        '" -define icon:auto-resize=256,128,64,48,32,16 "' + icoPath + '"'),
        nil, SW_HIDE);
end;

{ *************************** SOME WTF FUNCTIONS ***************************** }

// Draw png image from src to dest png file with transpanency
procedure DrawPngWithAlpha(Src, Dest: TPngImage; const R: TRect);
var
    X, Y: Integer;
    Alpha: PByte;
begin
    Src.Draw(Dest.Canvas, R);

    for Y := R.Top to R.Bottom - 1 do
        for X := R.Left to R.Right - 1 do
        begin
            Alpha := @Dest.AlphaScanline[Y]^[X];
            Alpha^ := Min(255, Alpha^ + Src.AlphaScanline[Y - R.Top]^
                [X - R.Left]);
        end;
end;

// Load PNG image and draw PNG icon on it
procedure LoadPNGAndDrawIcon(tPath, avatar, icon, save_path: string);
var
    av, i: TPngImage;
begin
    av := TPngImage.Create;
    try
        i := TPngImage.Create;
        try
            av.LoadFromFile(avatar);
            i.LoadFromFile(icon);
            DrawPngWithAlpha(i, av, Rect(0, 0, 128, 128));
            av.SaveToFile(save_path);
        finally
            av.Free;
        end;
    finally
        i.Free;
    end;
end;

// WTF function - used to resize png image
procedure ResizePng(apng: TPngImage; NuWidth, NuHeight: Integer);
var
    xscale, yscale: Single;
    sfrom_y, sfrom_x: Single;
    ifrom_y, ifrom_x: Integer;
    to_y, to_x: Integer;
    weight_x, weight_y: array [0 .. 1] of Single;
    weight: Single;
    new_red, new_green: Integer;
    new_blue, new_alpha: Integer;
    new_colortype: Integer;
    total_red, total_green: Single;
    total_blue, total_alpha: Single;
    IsAlpha: Boolean;
    ix, iy: Integer;
    bTmp: TPngImage;
    sli, slo: pRGBLine;
    ali, alo: PNGImage.pbytearray;
begin
    if not(apng.Header.ColorType in [COLOR_RGBALPHA, COLOR_RGB]) then
        raise Exception.Create('Only COLOR_RGBALPHA and COLOR_RGB formats' +
            ' are supported');
    IsAlpha := apng.Header.ColorType in [COLOR_RGBALPHA];
    if IsAlpha then
        new_colortype := COLOR_RGBALPHA
    else
        new_colortype := COLOR_RGB;
    bTmp := tpngobject.CreateBlank(new_colortype, 8, NuWidth, NuHeight);
    xscale := bTmp.Width / (apng.Width - 1);
    yscale := bTmp.Height / (apng.Height - 1);
    for to_y := 0 to bTmp.Height - 1 do
    begin
        sfrom_y := to_y / yscale;
        ifrom_y := Trunc(sfrom_y);
        weight_y[1] := sfrom_y - ifrom_y;
        weight_y[0] := 1 - weight_y[1];
        for to_x := 0 to bTmp.Width - 1 do
        begin
            sfrom_x := to_x / xscale;
            ifrom_x := Trunc(sfrom_x);
            weight_x[1] := sfrom_x - ifrom_x;
            weight_x[0] := 1 - weight_x[1];

            total_red := 0.0;
            total_green := 0.0;
            total_blue := 0.0;
            total_alpha := 0.0;
            for ix := 0 to 1 do
            begin
                for iy := 0 to 1 do
                begin
                    sli := apng.Scanline[ifrom_y + iy];
                    if IsAlpha then
                        ali := apng.AlphaScanline[ifrom_y + iy];
                    new_red := sli[ifrom_x + ix].rgbtRed;
                    new_green := sli[ifrom_x + ix].rgbtGreen;
                    new_blue := sli[ifrom_x + ix].rgbtBlue;
                    if IsAlpha then
                        new_alpha := ali[ifrom_x + ix];
                    weight := weight_x[ix] * weight_y[iy];
                    total_red := total_red + new_red * weight;
                    total_green := total_green + new_green * weight;
                    total_blue := total_blue + new_blue * weight;
                    if IsAlpha then
                        total_alpha := total_alpha + new_alpha * weight;
                end;
            end;
            slo := bTmp.Scanline[to_y];
            if IsAlpha then
                alo := bTmp.AlphaScanline[to_y];
            slo[to_x].rgbtRed := Round(total_red);
            slo[to_x].rgbtGreen := Round(total_green);
            slo[to_x].rgbtBlue := Round(total_blue);
            if IsAlpha then
                alo[to_x] := Round(total_alpha);
        end;
    end;
    apng.Assign(bTmp);
    bTmp.Free;
end;

// Convert PNG image to BMP
procedure ConvertPngToBmp(Src: TPngImage; Dest: TBitmap);
begin
    Dest.Height := Src.Height;
    Dest.Width := Src.Width;
    Dest.PixelFormat := pf32bit;
    Dest.AlphaFormat := afIgnored;
    Dest.TransparentMode := tmFixed;
    Dest.Transparent := true;
    Dest.Canvas.Draw(0, 0, Src);
end;

// Another WTF - Load PNG file and apply masko on it without transpanency
procedure LoadPNGAndApplyMask(avatar, mask, save_path: string);
var
    av, ma, wy: FMX.Graphics.TBitmap;
    tmp: TPngImage;
    tmp1: TBitmap;
    s: TMemoryStream;
begin
    av := FMX.Graphics.TBitmap.Create;
    try
        ma := FMX.Graphics.TBitmap.Create;
        try
            wy := FMX.Graphics.TBitmap.Create;
            try
                s := TMemoryStream.Create;
                try
                    tmp := TPngImage.Create;
                    try
                        tmp1 := TBitmap.Create;
                        try
                            tmp.LoadFromFile(avatar);
                            ResizePng(tmp, 128, 128);
                            ConvertPngToBmp(tmp, tmp1);
                            tmp1.PixelFormat := pf32bit;
                            tmp1.SaveToStream(s);
                            av.LoadFromStream(s);
                            ma.LoadFromFile(mask);
                            wy.CreateFromBitmapAndMask(av, ma);
                            wy.SaveToFile(save_path);
                        finally
                            tmp1.Free;
                        end;
                    finally
                        tmp.Free;
                    end;
                finally
                    s.Free;
                end;
            finally
                wy.Free;
            end;
        finally
            ma.Free;
        end;
    finally
        av.Free;
    end;
end;

end.
