unit UpdateManagerUnit;

interface

uses
    INIFiles, SysUtils, StrUtils;

type
    TUpdateFile = record
        jid: String;
        name: String;
        style: String;
        status: Boolean;
        res: String;
    end;

    TUpdateManager = class
        FilesDir: String;
        count: Integer;
        Files: array [0 .. 100] of TUpdateFile;
        procedure AddToUpdateList(info: TUpdateFile);
        function IsOnUpdateList(jid: String): Boolean;
        procedure RemoveFromUpdateList(jid: String);
        procedure GetUpdateList();
        function GetInfoFromJID(jid: String): TUpdateFile;
    end;

implementation

procedure TUpdateManager.AddToUpdateList(info: TUpdateFile);
var
    INI: TINIFile;
begin
    if not (DirectoryExists(FilesDir)) then
        ForceDirectories(FilesDir);
    INI := TINIFile.Create(FilesDir + '\' + info.jid + '.ini');
    try
        INI.WriteString('INFO', 'name', info.name);
        INI.WriteString('INFO', 'style', info.style);
        INI.WriteBool('INFO', 'status', info.status);
        INI.WriteString('INFO', 'res', info.res);
    finally
        INI.Free;
    end;
end;

function TUpdateManager.IsOnUpdateList(jid: String): Boolean;
var
    INI: TINIFile;
begin
    jid := AnsiReplaceText(jid, '-', '');
    if (FileExists(FilesDir + '\' + jid + '.ini')) then
    begin
        INI := TINIFile.Create(FilesDir + '\' + jid + '.ini');
        try
            if (INI.ReadString('INFO', 'name', '') <> '') and
                (INI.ReadString('INFO', 'style', '') <> '') and
                (INI.ValueExists('INFO', 'status') and
                (INI.ReadString('INFO', 'res', '') <> '')) then
                Result := true;
        finally
            INI.Free;
        end;
    end
    else
        Result := false;
end;

procedure TUpdateManager.RemoveFromUpdateList(jid: String);
begin
    jid := AnsiReplaceText(jid, '-', '');
    if (FileExists(FilesDir + '\' + jid + '.ini')) then
    begin
        DeleteFile(FilesDir + '\' + jid + '.ini');
    end;
end;

procedure TUpdateManager.GetUpdateList();
var
    SR: TSearchRec;
    Fik: Integer;
    pDir: String;
    i: Integer;
    INI: TINIFile;
begin
    i := 0;
    count := 0;
    Fik := FindFirst(FilesDir + '\' + '*.ini', faAnyFile, SR);
    while (Fik = 0) do
    begin
        Files[i].jid := ExtractFileName(SR.name);
        INI := TINIFile.Create(FilesDir + '\' + Files[i].jid + '.ini');
        try
            Files[i].name := INI.ReadString('INFO', 'name', '');
            Files[i].style := INI.ReadString('INFO', 'style', '');
            Files[i].status := INI.ReadBool('INFO', 'status', true);
            Files[i].res := INI.ReadString('INFO', 'res', '');
        finally
            INI.Free;
        end;
        Fik := FindNext(SR);
        i := i + 1;
    end;
    count := i;
    FindClose(SR);
end;

function TUpdateManager.GetInfoFromJID(jid: string): TUpdateFile;
var
    INI: TINIFile;
begin
    jid := AnsiReplaceText(jid, '-', '');
    if (FileExists(FilesDir + '\' + jid + '.ini')) then
    begin
        INI := TINIFile.Create(FilesDir + '\' + jid + '.ini');
        try
            if (INI.ReadString('INFO', 'name', '') <> '') and
                (INI.ReadString('INFO', 'style', '') <> '') and
                (INI.ValueExists('INFO', 'status') and
                (INI.ReadString('INFO', 'res', '') <> '')) then
            begin
                Result.jid := jid;
                Result.name := INI.ReadString('INFO', 'name', '');
                Result.style := INI.ReadString('INFO', 'style', '');
                Result.status := INI.ReadBool('INFO', 'status', true);
                Result.res := INI.ReadString('INFO', 'res', '');
            end;
        finally
            INI.Free;
        end;
    end;
end;

end.
