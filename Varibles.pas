unit Varibles;

interface

uses
    PluginAPI, Messages, StyleManagerUnit, UpdateManagerUnit, ImageToolUnit;

const
    { PLUGIN INFO }
    PLUGIN_SHORTNAME = 'FastTalk';
    PLUGIN_DESCRIPTION = 'Tw�rz skr�ty do ulubionych kontakt�w na pulpicie. ' +
        'Wystarczy wybra� kontakt, styl ikony oraz nazw�.';
    PLUGIN_AUTHOR = 'Rafa� Babiarz (Sauler)';
    PLUGIN_AUTHORMAIL = 'sauler1995@gmail.com';
    PLUGIN_COPYRIGHT = 'Sauler';

    { ALPHAWINDOWS PLUGIN SUPPORT }
    WM_USER = $0400;
    WM_ALPHAWINDOWS = WM_USER + 666;

    { ICONS ID }
    ITEM_ICON = 0;

    { SERVICES }
    FASTTALK_SERVICE = 'FastTalk/PopUpItemService';

    { POPUP NAMES }
    CONTACT_POPUP = 'muItem';

    { CONTACT STATES }
    CONTACT_OFFLINE = 0;
    CONTACT_ONLINE = 1;
    CONTACT_FFC = 2;
    CONTACT_AWAY = 3;
    CONTACT_NA = 4;
    CONTACT_DND = 5;
    CONTACT_INV = 6;
    CONTACT_NULL = 7;
    AQQ_OFF = 255; // const for status varible, used when AQQ is disabled

type
    { THIS TYPE CONTAINS FASTTALK DIRECTORIES }
    TFastTalkDir = record
        Default: String;
        Avatars: String;
        Styles: String;
        Utils: String;
    end;

    { TYPE FOR STORING CONTACT DATA }
    TContactData = record
        JID: String;
        Name: String;
        State: Integer;
        Resource: String;
    end;

var
    { STANDARD AQQ PLUGIN VARIBLES }
    PluginInfo: TPluginInfo;
    PluginLink: TPluginLink;

    Icons: array [0 .. 1] of Integer; // icons array
    useCustomAvatar: Boolean;
    facebookAvatar: Boolean;

    runJID, runRes: String; // shortcut functionality
    userIdx: Integer; // shortcut functionality

    { CONTACT POPUP MENU ITEM }
    PopupItem: TPluginAction;
    PopupItemEdit: TPluginActionEdit;

    { CONTACT DATA VARIBLES }
    contact: TContactData; // contains info about contact that is right clicked
    updateContact: TContactData; // same. Used in contact update event
    contactInfo: TUpdateFile; // used if contact exists on updatelist

    { MANAGERS }
    UpdateManager: TUpdateManager;
    StyleManager: TStyleManager;
    ImageTool: TImageTool;

implementation

end.
