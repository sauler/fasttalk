unit LangAPI;

interface

uses
    Dialogs, SysUtils, Forms, StrUtils, Classes,
    // Kontrolki AlphaControls - w przypadku nie korzystania nale�y zakomentowa�.
    sLabel, sEdit, sComboBox, sPageControl, sListView,
    sSpeedButton, sButton, sRadioButton, sStatusBar, sCheckBox, sMemo,
    sTrackBar,
    // Kontrolki Standardowe
    ExtCtrls, Controls, Menus, StdCtrls, ActnList;

type
    TLangCache = packed record
        StrID: String;
        StrEntry: String;
    end;

    // Pobranie komunikat�w np. dla message-box�w
function GetLangStr(ID: String): String;
// Lokalizowanie form
procedure LangForm(Form: TForm);
// Lokalizowanie ramek
procedure LangFrame(Form: TFrame);
// Czyszczenie pami�ci podr�cznej po zmianie lokalizacji
procedure ClearLngCache;

var
    LangCache: array of TLangCache;
    LangPath: String;

implementation

procedure ClearLngCache;
begin
    SetLength(LangCache, 0);
end;

function GetLangStr(ID: String): String;

var
    FileName: String;
    // FileTxt: TextFile;
    LangFile: TStringList;
    i: Integer;

begin
    for i := 0 to Length(LangCache) - 1 do
    begin
        if LangCache[i].StrID = ID then
        begin
            Result := LangCache[i].StrEntry;
            Exit;
        end;
    end;

    // Je�eli danych nie ma w buforze to szukamy w pliku.

    FileName := IncludeTrailingPathDelimiter(LangPath) + 'Const.lng';
    if not FileExists(FileName) then
        Exit;

    try
        LangFile := TStringList.Create;
        LangFile.LoadFromFile(FileName);

        for i := 0 to LangFile.Count - 1 do
        begin
            if LeftStr(LangFile[i], Length(ID) + 1) = ID + '=' then
            begin
                Result := MidStr(LangFile[i], Pos('=', LangFile[i]) + 1,
                    Length(LangFile[i]));
                Break;
            end;
        end;
    finally
        LangFile.Free;
    end;

    // Dopisujemy dane do bufora.

    SetLength(LangCache, Length(LangCache) + 1);
    LangCache[Length(LangCache) - 1].StrID := ID;
    LangCache[Length(LangCache) - 1].StrEntry := Result;
end;

procedure LangForm(Form: TForm);
// ZMIENIASZ CO� TUTAJ? LUSTRZENIE ZR�B TE SAME ZMIANY DLA LANGFRAME
var
    Idx, j, i: Integer;
    Plik: TStringList;
    FileName: String;

    function GetID(var Plik: TStringList; Idx: Integer): String;
    begin
        if Idx > Plik.Count then
        begin
            Result := '???';
            Exit;
        end;
        Result := MidStr(Plik[Idx - 1], Pos('=', Plik[Idx - 1]) + 1,
            Length(Plik[Idx - 1]));
        Result := StringReplace(Result, '#', #13#10, [rfReplaceAll]);
    end;

begin
    UseLatestCommonDialogs := True;

    Plik := TStringList.Create;

    FileName := IncludeTrailingPathDelimiter(LangPath) + Form.ClassName
        + '.lng';
    if not FileExists(FileName) then
        Exit;

    Plik.LoadFromFile(FileName);
    Plik.Sort;

    Idx := Form.Tag;
    if Idx > 0 then
    begin
        Form.Caption := GetID(Plik, Idx);
    end;

    for i := 0 to Form.ComponentCount - 1 do
    begin
        if Form.Components[i] is TsPageControl then
        begin
            for j := 0 to (Form.Components[i] as TsPageControl).PageCount - 1 do
            begin
                Idx := ((Form.Components[i] as TsPageControl).Pages[j]).Tag;
                if Idx > 0 then
                begin
                    (Form.Components[i] as TsPageControl).Pages[j].Caption :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TMainMenu then
        begin
            for j := 0 to (Form.Components[i] as TMainMenu).Items.Count - 1 do
            begin
                Idx := ((Form.Components[i] as TMainMenu).Items[j]).Tag;
                if Idx > 0 then
                begin
                    (Form.Components[i] as TMainMenu).Items[j].Caption :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TsListView then
        begin
            for j := 0 to (Form.Components[i] as TsListView)
                .Columns.Count - 1 do
            begin
                Idx := ((Form.Components[i] as TsListView).Columns[j]).Tag;
                if Idx > 0 then
                begin
                    (Form.Components[i] as TsListView).Columns[j].Caption :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TsComboBox then
        begin
            Idx := (Form.Components[i] as TsComboBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsComboBox).Items.Text :=
                    StringReplace(GetID(Plik, Idx), ';', #13#10,
                    [rfReplaceAll]);
            end;
        end;
        if Form.Components[i] is TsTrackBar then
        begin
            Idx := (Form.Components[i] as TsTrackBar).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsTrackBar).Hint := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TGroupBox then
        begin
            Idx := (Form.Components[i] as TGroupBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TGroupBox).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TCategoryPanel then
        begin
            Idx := (Form.Components[i] as TCategoryPanel).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TCategoryPanel).Caption :=
                    GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsLabel then
        begin
            Idx := (Form.Components[i] as TsLabel).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsLabel).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsLabelFX then
        begin
            Idx := (Form.Components[i] as TsLabelFX).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsLabelFX).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TLabel then
        begin
            Idx := (Form.Components[i] as TLabel).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TLabel).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TLabeledEdit then
        begin
            Idx := (Form.Components[i] as TLabeledEdit).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TLabeledEdit).EditLabel.Caption :=
                    GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsMemo then
        begin
            Idx := (Form.Components[i] as TsMemo).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsMemo).Text := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsCheckBox then
        begin
            Idx := (Form.Components[i] as TsCheckBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsCheckBox).Caption := GetID(Plik, Idx);
                (Form.Components[i] as TsCheckBox).ShowFocus := False;
            end;
        end;
        if Form.Components[i] is TCheckBox then
        begin
            Idx := (Form.Components[i] as TCheckBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TCheckBox).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsStatusBar then
        begin
            Idx := (Form.Components[i] as TsStatusBar).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsStatusBar).SimpleText :=
                    GetID(Plik, Idx);
                if not(Form.Components[i] as TsStatusBar).SimplePanel then
                begin
                    if (Form.Components[i] as TsStatusBar).Panels.Count > 0 then
                    begin
                        (Form.Components[i] as TsStatusBar).Panels[0].Text :=
                            GetID(Plik, Idx);
                    end;
                end;
            end;
        end;
        if Form.Components[i] is TsRadioButton then
        begin
            Idx := (Form.Components[i] as TsRadioButton).Tag;
            (Form.Components[i] as TsRadioButton).ShowFocus := False;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsRadioButton).Caption :=
                    GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsEdit then
        begin
            Idx := (Form.Components[i] as TsEdit).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsEdit).Text := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsButton then
        begin
            Idx := (Form.Components[i] as TsButton).Tag;
            (Form.Components[i] as TsButton).ShowFocus := False;
            if (Idx > 0) and
                (not Assigned((Form.Components[i] as TsButton).Action)) then
            begin
                if Length((Form.Components[i] as TsButton).Caption) > 0 then
                    (Form.Components[i] as TsButton).Caption := GetID(Plik, Idx)
                else
                    (Form.Components[i] as TsButton).Hint := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TButton then
        begin
            Idx := (Form.Components[i] as TButton).Tag;
            if (Idx > 0) and
                (not Assigned((Form.Components[i] as TButton).Action)) then
            begin
                if Length((Form.Components[i] as TButton).Caption) > 0 then
                    (Form.Components[i] as TButton).Caption := GetID(Plik, Idx)
                else
                    (Form.Components[i] as TButton).Hint := GetID(Plik, Idx)
            end;
        end;
        if Form.Components[i] is TsSpeedButton then
        begin
            Idx := (Form.Components[i] as TsSpeedButton).Tag;
            if (Idx > 0) and
                (not Assigned((Form.Components[i] as TsSpeedButton).Action))
            then
            begin
                if Length((Form.Components[i] as TsSpeedButton).Caption) > 0
                then
                begin
                    (Form.Components[i] as TsSpeedButton).Caption :=
                        GetID(Plik, Idx);
                end;
                if (Form.Components[i] as TsSpeedButton).ShowHint then
                begin
                    (Form.Components[i] as TsSpeedButton).Hint :=
                        GetID(Plik, Idx);
                end;
            end
            else
            begin
                if ((Form.Components[i] as TsSpeedButton).ShowHint) and (Idx > 0)
                then
                begin
                    (Form.Components[i] as TsSpeedButton).Hint :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TActionList then
        begin
            for j := 0 to (Form.Components[i] as TActionList).ActionCount - 1 do
            begin
                Idx := (Form.Components[i] as TActionList).Actions[j].Tag;
                if Idx > 0 then
                begin
                    ((Form.Components[i] as TActionList).Actions[j] as TAction)
                        .Caption := GetID(Plik, Idx);
                    if Length(((Form.Components[i] as TActionList).Actions[j]
                        as TAction).Hint) > 0 then
                    begin
                        ((Form.Components[i] as TActionList).Actions[j]
                            as TAction).Hint := GetID(Plik, Idx);
                    end;
                end;
            end;
        end;
    end;

    FreeAndNil(Plik);

end;

procedure LangFrame(Form: TFrame);
// ZMIENIASZ CO� TUTAJ? LUSTRZENIE ZR�B TE SAME ZMIANY DLA LANGFORM
var
    Idx, j, i: Integer;
    Plik: TStringList;
    FileName: String;

    function GetID(var Plik: TStringList; Idx: Integer): String;
    begin
        if Idx > Plik.Count then
        begin
            Result := '???';
            Exit;
        end;
        Result := MidStr(Plik[Idx - 1], Pos('=', Plik[Idx - 1]) + 1,
            Length(Plik[Idx - 1]));
        Result := StringReplace(Result, '#', #13#10, [rfReplaceAll]);
    end;

begin
    Plik := TStringList.Create;

    FileName := IncludeTrailingPathDelimiter(LangPath) + Form.ClassName
        + '.lng';
    if not FileExists(FileName) then
        Exit;

    Plik.LoadFromFile(FileName);
    Plik.Sort;

    for i := 0 to Form.ComponentCount - 1 do
    begin
        if Form.Components[i] is TsPageControl then
        begin
            for j := 0 to (Form.Components[i] as TsPageControl).PageCount - 1 do
            begin
                Idx := ((Form.Components[i] as TsPageControl).Pages[j]).Tag;
                if Idx > 0 then
                begin
                    (Form.Components[i] as TsPageControl).Pages[j].Caption :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TMainMenu then
        begin
            for j := 0 to (Form.Components[i] as TMainMenu).Items.Count - 1 do
            begin
                Idx := ((Form.Components[i] as TMainMenu).Items[j]).Tag;
                if Idx > 0 then
                begin
                    (Form.Components[i] as TMainMenu).Items[j].Caption :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TsListView then
        begin
            for j := 0 to (Form.Components[i] as TsListView)
                .Columns.Count - 1 do
            begin
                Idx := ((Form.Components[i] as TsListView).Columns[j]).Tag;
                if Idx > 0 then
                begin
                    (Form.Components[i] as TsListView).Columns[j].Caption :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TsComboBox then
        begin
            Idx := (Form.Components[i] as TsComboBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsComboBox).Items.Text :=
                    StringReplace(GetID(Plik, Idx), ';', #13#10,
                    [rfReplaceAll]);
            end;
        end;
        if Form.Components[i] is TsTrackBar then
        begin
            Idx := (Form.Components[i] as TsTrackBar).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsTrackBar).Hint := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TGroupBox then
        begin
            Idx := (Form.Components[i] as TGroupBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TGroupBox).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TCategoryPanel then
        begin
            Idx := (Form.Components[i] as TCategoryPanel).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TCategoryPanel).Caption :=
                    GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsLabel then
        begin
            Idx := (Form.Components[i] as TsLabel).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsLabel).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsLabelFX then
        begin
            Idx := (Form.Components[i] as TsLabelFX).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsLabelFX).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TLabel then
        begin
            Idx := (Form.Components[i] as TLabel).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TLabel).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TLabeledEdit then
        begin
            Idx := (Form.Components[i] as TLabeledEdit).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TLabeledEdit).EditLabel.Caption :=
                    GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsMemo then
        begin
            Idx := (Form.Components[i] as TsMemo).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsMemo).Text := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsCheckBox then
        begin
            Idx := (Form.Components[i] as TsCheckBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsCheckBox).Caption := GetID(Plik, Idx);
                (Form.Components[i] as TsCheckBox).ShowFocus := False;
            end;
        end;
        if Form.Components[i] is TCheckBox then
        begin
            Idx := (Form.Components[i] as TCheckBox).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TCheckBox).Caption := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsStatusBar then
        begin
            Idx := (Form.Components[i] as TsStatusBar).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsStatusBar).SimpleText :=
                    GetID(Plik, Idx);
                if not(Form.Components[i] as TsStatusBar).SimplePanel then
                begin
                    if (Form.Components[i] as TsStatusBar).Panels.Count > 0 then
                    begin
                        (Form.Components[i] as TsStatusBar).Panels[0].Text :=
                            GetID(Plik, Idx);
                    end;
                end;
            end;
        end;
        if Form.Components[i] is TsRadioButton then
        begin
            Idx := (Form.Components[i] as TsRadioButton).Tag;
            (Form.Components[i] as TsRadioButton).ShowFocus := False;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsRadioButton).Caption :=
                    GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsEdit then
        begin
            Idx := (Form.Components[i] as TsEdit).Tag;
            if Idx > 0 then
            begin
                (Form.Components[i] as TsEdit).Text := GetID(Plik, Idx);
            end;
        end;
        if Form.Components[i] is TsButton then
        begin
            Idx := (Form.Components[i] as TsButton).Tag;
            (Form.Components[i] as TsButton).ShowFocus := False;
            if (Idx > 0) and
                (not Assigned((Form.Components[i] as TsButton).Action)) then
            begin
                if Length((Form.Components[i] as TsButton).Caption) > 0 then
                    (Form.Components[i] as TsButton).Caption := GetID(Plik, Idx)
                else
                    (Form.Components[i] as TsButton).Hint := GetID(Plik, Idx)
            end;
        end;
        if Form.Components[i] is TButton then
        begin
            Idx := (Form.Components[i] as TButton).Tag;
            if (Idx > 0) and
                (not Assigned((Form.Components[i] as TButton).Action)) then
            begin
                if Length((Form.Components[i] as TButton).Caption) > 0 then
                    (Form.Components[i] as TButton).Caption := GetID(Plik, Idx)
                else
                    (Form.Components[i] as TButton).Hint := GetID(Plik, Idx)
            end;
        end;
        if Form.Components[i] is TsSpeedButton then
        begin
            Idx := (Form.Components[i] as TsSpeedButton).Tag;
            if (Idx > 0) and
                (not Assigned((Form.Components[i] as TsSpeedButton).Action))
            then
            begin
                if Length((Form.Components[i] as TsSpeedButton).Caption) > 0
                then
                begin
                    (Form.Components[i] as TsSpeedButton).Caption :=
                        GetID(Plik, Idx);
                end;
                if (Form.Components[i] as TsSpeedButton).ShowHint then
                begin
                    (Form.Components[i] as TsSpeedButton).Hint :=
                        GetID(Plik, Idx);
                end;
            end;
        end;
        if Form.Components[i] is TActionList then
        begin
            for j := 0 to (Form.Components[i] as TActionList).ActionCount - 1 do
            begin
                Idx := (Form.Components[i] as TActionList).Actions[j].Tag;
                if Idx > 0 then
                begin
                    ((Form.Components[i] as TActionList).Actions[j] as TAction)
                        .Caption := GetID(Plik, Idx);
                    if Length(((Form.Components[i] as TActionList).Actions[j]
                        as TAction).Hint) > 0 then
                    begin
                        ((Form.Components[i] as TActionList).Actions[j]
                            as TAction).Hint := GetID(Plik, Idx);
                    end;
                end;
            end;
        end;
    end;

    FreeAndNil(Plik);

end;

end.
