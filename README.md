# FastTalk #

FastTalk jest wtyczką do komunikatora [AQQ](http://www.aqq.eu/). Pozwala na tworzenie skrótów do ulubionych kontaktów na pulpicie. Wystarczy, że wybierzesz kontakt, styl ikony oraz nazwę skrótu.

### Wymagania ###

Aby skompilować wtyczkę FastTalk potrzebujesz:

* Embarcadero Delphi 10 Seattle
* Pełna wersja komponentów [AlphaControls](http://www.alphaskins.com/)
* Plik [PluginAPI.pas](http://www.aqq.eu/sdk/data/files/PluginAPI.zip) oraz plik [LangAPI.pas](http://www.aqq.eu/sdk/data/files/LangAPI.zip)
* Plik convert.exe z pakietu [ImageMagick](http://www.imagemagick.org/script/index.php)
* Opcjonalnie [UPX](http://upx.sourceforge.net/) dla zmniejszenia rozmiaru pliku wynikowego (w szczególności wersji x64)

### Przygotowanie projektu ###

* Pliki PluginAPI.pas i LangAPI.pas umieszczamy w głównym katalogu projektu
* Plik convert.exe umieszczamy w folderze Data w katalogu projektu

### Błędy ###

Znalezione błędy wtyczki należy zgłaszać na [tracekerze](http://forum.aqq.eu/tracker/project-150-fasttalk/) znajdującym na oficjalnym forum komunikatora AQQ lub pisząc bezpośrednio do autora wtyczki (preferowany kontakt poprzez Jabber).

### Kontakt z autorem ###

Autorem wtyczki FastTalk jest Rafał Babiarz. Możesz skontaktować się z nim drogą mailową (sauler1995@gmail.com) lub poprzez Jabber (sauler@jix.im).

### Licencja ###

Wtyczka FastTalk objęta jest licencją [GNU General Public License 3](http://www.gnu.org/copyleft/gpl.html).

```
#!

FastTalk
Copyright © 2014-2015  Rafał Babiarz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```