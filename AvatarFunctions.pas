unit AvatarFunctions;

interface

uses
    StrUtils, SysUtils, URLMon, INIFiles, AQQFunctions, Varibles;

{ AVATAR GET WAYS }
function GetAvatarPath(JID, styleName: String): String;
function DownloadFacebookAvatar(JID, styleName: String): String;

{ UTILS FUNCTIONS }
function DownloadFile(src, dest: string): Boolean;

implementation

uses
    Functions;

{ ******************************* AVATAR GET WAYS ****************************** }

function GetAvatarPath(JID, styleName: String): String;
var
    contactsDir: PWideChar;
    userDir: PWideChar;
    contactFile: TIniFile;
    pResult: String;
begin
    if (AnsiContainsStr(JID, '@chat.facebook.com')) then
    begin
        if not(DirectoryExists(GetDirectories.Avatars + '\' + JID + '\' +
            styleName)) then
            ForceDirectories(GetDirectories.Avatars + '\' + JID + '\' +
                styleName);
        facebookAvatar := true;
        Result := DownloadFacebookAvatar(JID, styleName);
    end
    else
    begin
        userDir := GetUserDir();
        contactsDir := PWideChar(userDir + '\Data\Contacts');

        contactFile := TIniFile.Create(contactsDir + '\' + JID + '.ini');
        try
            Result := PWideChar(contactFile.ReadString('Other', 'Avatar',
                'Not found'));
            if (Result <> 'Not found') then
                if (Result <> '') then
                    Result := Base64(PWideChar(Result), 2)
                else
                    Result := 'Not found';
            facebookAvatar := false;
        finally
            contactFile.Free;
        end;
    end;
end;

// download avatar from facebook
function DownloadFacebookAvatar(JID, styleName: String): String;
var
    fbAPI: String;
    destPath: String;
    JIDBackup: String;
begin
    JIDBackup := JID;
    JID := AnsiReplaceText(JID, '@chat.facebook.com', ' ');
    JID := Trim(JID);
    if (AnsiContainsStr(JID, '-')) then
    begin
        JID := AnsiReplaceText(JID, '-', ' ');
        JID := Trim(JID);
    end;

    fbAPI := 'https://graph.facebook.com/' + JID +
        '/picture?width=128&height=128';

    destPath := GetDirectories().Avatars + '\' + JIDBackup + '\' + styleName +
        '\avatar.jpg';

    DownloadFile(fbAPI, destPath);
    Result := destPath;
end;

{ ******************************* UTILS FUNCTIONS ****************************** }

// download file from src and save it in dest
function DownloadFile(src, dest: string): Boolean;
begin
    try
        Result := UrlDownloadToFile(nil, PChar(src), PChar(dest), 0, nil) = 0;
    except
        Result := false;
    end;
end;

end.
