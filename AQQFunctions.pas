unit AQQFunctions;

interface

uses
    PluginAPI, Windows, SysUtils, Varibles, Classes, IniFiles, Dialogs;

{ APP FUNCTIONS }
function GetAppPath(): PWideChar;
function GetAppFilePath(): PWideChar;
function GetAppType(): PWideChar;
function GetAppVersion(): PWideChar;
function GetUserDir(): PWideChar;
function RestartApp(): Integer;

{ THEMES FUNCTIONS }
function GetHue(): Integer;
function GetSaturation(): Integer;
function GetBrightness(): Integer;
function GetThemeDir(): PWideChar;
function GetDefaultThemeDir(): PWideChar;
function SetDefaultTheme(): Integer;
function ActivateTheme(): Integer;
function ChkSkinEnabled(): Boolean;
function ChkThemeAnimateWindows(): Boolean;
function ChkThemeGlowing(): Boolean;

{ PLUGINS FUNCTIONS }
function GetPluginDir(): PWideChar;
function IsPluginEnabled(pluginPath: PWideChar): Boolean;
function EnablePlugin(pluginPath: PWideChar): Integer;
function DisablePlugin(pluginPath: PWideChar): Integer;
function RefreshPluginList(): Integer;
function AddToExclude(pluginPath: PWideChar): Integer;
function DeleteFromExclude(pathToPlugin: PWideChar): Integer;

{ INI FUNCTIONS }
function OpenINI(filePath: PWideChar): Integer;
function CloseINI(ID: Integer): Integer;
function WriteINI(ID: Integer; settings: PSaveSetup): Integer;
function ReadINI(ID: Integer; settings: PSaveSetup): PWideChar;

{ PNG FUNCTIONS }
function LoadPNG(iconPath: PWideChar): Integer;
function UnloadPNG(iconID: Integer): Integer;
function ReplacePNG(iconID: Integer; iconPath: PWideChar): Integer;

{ POPUP FUNCTIONS }
function CreatePopUp(PlgAct: TPluginAction): Integer;
function CreatePopUpMenuItem(PlgAct: TPluginAction): Integer;
function EditPopUpMenuItem(PlgAct: TPluginAction): Integer;
function DestroyPopUp(PlgAct: TPluginAction): Integer;
function DestroyPopUpMenuItem(PlgAct: TPluginAction): Integer;

{ BUTTON FUNCTIONS }
function CreateButton(TB: PWideChar; PlgAct: TPluginAction): Integer;
function DestroyButton(TB: PWideChar; PlgAct: TPluginAction): Integer;
function UpdateButton(TB: PWideChar; PlgAct: TPluginAction): Integer;

{ ACCOUNT FUNCTIONS }
function CreateAccountEvents(PlgAEv: TPluginAccountEvents): Integer;
function DestroyAccountEvents(ID: Integer;
    PlgAEv: TPluginAccountEvents): Integer;
function CreateAgent(PlgAg: TPluginAgent): Integer;
function DestroyAgent(jid: PWideChar): Integer;

{ CHAT FUNCTIONS }
function OpenChat(chat: TPluginChatOpen): Integer;
function ChatPresence(ChatPres: TPluginChatPresence): Integer;
function AddMessageToChat(ChatMessage: TPluginMessage): Integer;
function ExecuteMsg(ID: Integer; jid: PWideChar): Integer;

{ OTHER FUNCTIONS }
function CreateDir(dir: PWideChar): Integer;
function RunAction(action: PWideChar): Integer;
function GetFileNameFromDir(dir: PWideChar): PWideChar;
function ExtractRes(filename: PWideChar; resName: PWideChar;
    resType: PWideChar): Integer;
function ExcludeTralingPathDelimiter(dir: PWideChar): PWideChar;
function AQQShowMessage(typ: Integer; msg: PWideChar): Integer;
function GetNetworkState(StateChange: TPluginStateChange;
    UserIdx: Integer): Integer;
function IsListReady(): Integer;
function AQQProcessMessages(): Integer;
function Base64(text: PWideChar; mode: Integer):String;

implementation

{ APP FUNCTIONS }

// Get AQQ install path
function GetAppPath(): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService(AQQ_FUNCTION_GETAPPPATH, 0, 0));
end;

// Get AQQ.exe file path
function GetAppFilePath(): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService
        (AQQ_FUNCTION_GETAPPFILEPATH, 0, 0));
end;

// Get AQQ binary type 32 or 64 bit
function GetAppType(): PWideChar;
var
    appType: Cardinal;
begin
    GetBinaryType(GetAppFilePath(), appType);
    case appType of
        SCS_32BIT_BINARY:
            Result := '32';
        6:
            Result := '64'; // 6 - SCS_64BIT_BINARY
    end;
end;

// Get AQQ version
function GetAppVersion(): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService(AQQ_SYSTEM_APPVER, 0, 0));
end;

// Get directory of current AQQ user
function GetUserDir(): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService(AQQ_FUNCTION_GETUSERDIR, 0, 0));
end;

// restart AQQ
function RestartApp(): Integer;
begin
    PluginLink.CallService(AQQ_SYSTEM_RESTART, 0, 0);
end;

{ THEMES FUNCTIONS }

// Get Hue
function GetHue(): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_COLORGETHUE, 0, 0);
end;

// Get Saturation
function GetSaturation(): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_COLORGETSATURATION, 0, 0);
end;

// Get Brightness
function GetBrightness(): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_COLORGETBRIGHTNESS, 0, 0);
end;

// Get active theme path
function GetThemeDir(): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService(AQQ_FUNCTION_GETTHEMEDIR, 0, 0));
end;

function GetDefaultThemeDir(): PWideChar;
begin
    Result := PWideChar(GetAppPath() + 'System\Shared\Themes\Standard');
end;

// Set default theme - "Standard"
function SetDefaultTheme(): Integer;
var
    defaultThemePath: PWideChar;
begin
    defaultThemePath := GetDefaultThemeDir();
    Result := PluginLink.CallService(AQQ_SYSTEM_THEME_SET, 0,
        lParam(defaultThemePath));
end;

// Active selected theme
function ActivateTheme(): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_THEME_APPLY, 0, 1);
end;

// advanced styles enabled ?
function ChkSkinEnabled(): Boolean;
var
    IniList: TStrings;
    settings: TMemIniFile;
    SkinsEnabled: String;
    text: PWideChar;
begin
    IniList := TStringList.Create();
    text := PWideChar(PluginLink.CallService(AQQ_FUNCTION_FETCHSETUP, 0, 0));
    IniList.SetText(text);

    settings := TMemIniFile.Create('AQQ.ini');
    settings.SetStrings(IniList);
    IniList.Destroy;
    SkinsEnabled := settings.ReadString('Settings', 'UseSkin', '1');
    settings.Destroy;
    Result := StrToBool(SkinsEnabled);
end;

// check theme animate
function ChkThemeAnimateWindows(): Boolean;
var
    IniList: TStrings;
    settings: TMemIniFile;
    AnimateWindowsEnabled: String;
    text: PWideChar;
begin
    IniList := TStringList.Create();
    text := PWideChar(PluginLink.CallService(AQQ_FUNCTION_FETCHSETUP, 0, 0));
    IniList.SetText(text);

    settings := TMemIniFile.Create('AQQ.ini');
    settings.SetStrings(IniList);
    IniList.Destroy;
    AnimateWindowsEnabled := settings.ReadString('Theme',
        'ThemeAnimateWindows', '1');
    settings.Destroy;

    Result := StrToBool(AnimateWindowsEnabled);
end;

// check theme glowing
function ChkThemeGlowing(): Boolean;
var
    IniList: TStrings;
    settings: TMemIniFile;
    GlowingEnabled: String;
    text: PWideChar;
begin
    IniList := TStringList.Create();
    text := PWideChar(PluginLink.CallService(AQQ_FUNCTION_FETCHSETUP, 0, 0));
    IniList.SetText(text);

    settings := TMemIniFile.Create('AQQ.ini');
    settings.SetStrings(IniList);
    IniList.Destroy;
    GlowingEnabled := settings.ReadString('Theme', 'ThemeGlowing', '1');
    settings.Destroy;

    Result := StrToBool(GlowingEnabled);
end;

{ PLUGINS FUNCTIONS }

// Get plugin directory
function GetPluginDir(): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService
        (AQQ_FUNCTION_GETPLUGINUSERDIR, 0, 0));
end;

// Plugin is enabled?
function IsPluginEnabled(pluginPath: PWideChar): Boolean;
begin
    Result := false;
    if (PluginLink.CallService(AQQ_SYSTEM_PLUGIN_ACTIVE, 2, lParam(pluginPath)
        ) = 0) then
        Result := false
    else
        Result := true;
end;

// Enable plugin
function EnablePlugin(pluginPath: PWideChar): Integer;
begin
    if not(IsPluginEnabled(pluginPath)) then
        Result := PluginLink.CallService(AQQ_SYSTEM_PLUGIN_ACTIVE, 1,
            lParam(pluginPath));
end;

// Disable plugin
function DisablePlugin(pluginPath: PWideChar): Integer;
begin
    if (IsPluginEnabled(pluginPath)) then
        Result := PluginLink.CallService(AQQ_SYSTEM_PLUGIN_ACTIVE, 0,
            lParam(pluginPath));
end;

// refresh plugins list in AQQ settings
function RefreshPluginList(): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_PLUGIN_REFRESHLIST, 0, 1);
end;

function AddToExclude(pluginPath: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_PLUGIN_EXCLUDE, 0,
        lParam(pluginPath));
end;

function DeleteFromExclude(pathToPlugin: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_PLUGIN_EXCLUDE, 1,
        lParam(pathToPlugin));
end;

{ INI FUNCTIONS }

// Create or open ini file
function OpenINI(filePath: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_INI_CREATE,
        wParam(filePath), 0);
end;

// Close ini file
function CloseINI(ID: Integer): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_INI_FREE, ID, 0);
end;

// Write string to ini file
function WriteINI(ID: Integer; settings: PSaveSetup): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_INI_WRITESTRING, ID,
        lParam(settings));
end;

// Read string from ini file
function ReadINI(ID: Integer; settings: PSaveSetup): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService(AQQ_FUNCTION_INI_READSTRING, ID,
        lParam(settings)));
end;

{ PNG FUNCTIONS }

// load PNG icon to aqq
function LoadPNG(iconPath: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_ICONS_LOADPNGICON, 0,
        lParam(iconPath));
end;

// unload PNG icon from aqq
function UnloadPNG(iconID: Integer): Integer;
begin
    Result := PluginLink.CallService(AQQ_ICONS_DESTROYPNGICON, 0, iconID);
end;

// replace icon in aqq
function ReplacePNG(iconID: Integer; iconPath: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_ICONS_REPLACEPNGICON, iconID,
        lParam(iconPath));
end;

{ POPUP FUNCTIONS }

// create popup menu
function CreatePopUp(PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService(AQQ_CONTROLS_CREATEPOPUPMENU, 0,
        lParam(@PlgAct));
end;

// create item in popup menu
function CreatePopUpMenuItem(PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService(AQQ_CONTROLS_CREATEPOPUPMENUITEM, 0,
        lParam(@PlgAct));
end;

// edit item in popup menu
function EditPopUpMenuItem(PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService(AQQ_CONTROLS_EDITPOPUPMENUITEM, 0,
        lParam(@PlgAct));
end;

// destroy popup menu
function DestroyPopUp(PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService(AQQ_CONTROLS_DESTROYPOPUPMENU, 0,
        lParam(@PlgAct));
end;

// destroy popup menu item
function DestroyPopUpMenuItem(PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService(AQQ_CONTROLS_DESTROYPOPUPMENUITEM, 0,
        lParam(@PlgAct));
end;

{ BUTTON FUNCTIONS }

// creating button in toolbar
function CreateButton(TB: PWideChar; PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService
        (PWideChar(AQQ_CONTROLS_TOOLBAR + TB + AQQ_CONTROLS_CREATEBUTTON), 0,
        lParam(@PlgAct));
end;

// destroy button in toolbar
function DestroyButton(TB: PWideChar; PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService
        (PWideChar(AQQ_CONTROLS_TOOLBAR + TB + AQQ_CONTROLS_DESTROYBUTTON), 0,
        lParam(@PlgAct));
end;

// update button in toolbar
function UpdateButton(TB: PWideChar; PlgAct: TPluginAction): Integer;
begin
    Result := PluginLink.CallService
        (PWideChar(AQQ_CONTROLS_TOOLBAR + TB + AQQ_CONTROLS_UPDATEBUTTON), 0,
        lParam(@PlgAct));
end;

{ ACCOUNT FUNCTIONS }

// sets account events
function CreateAccountEvents(PlgAEv: TPluginAccountEvents): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_ACCOUNT_EVENTS, 0,
        lParam(@PlgAEv));
end;

// delete account events
function DestroyAccountEvents(ID: Integer;
    PlgAEv: TPluginAccountEvents): Integer;
begin
    ZeroMemory(@PlgAEv, SizeOf(TPluginAccountEvents));
    Result := PluginLink.CallService(AQQ_SYSTEM_ACCOUNT_EVENTS, ID,
        lParam(@PlgAEv));
end;

// create agent
function CreateAgent(PlgAg: TPluginAgent): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_SETAGENT, 0, lParam(@PlgAg));
end;

// delete agent
function DestroyAgent(jid: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_REMOVEAGENT, wParam(jid), 0);
end;

{ CHAT FUNCTIONS }

// open chat
function OpenChat(chat: TPluginChatOpen): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_CHAT_OPEN, 0, lParam(@chat));
end;

// chat presence
function ChatPresence(ChatPres: TPluginChatPresence): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_CHAT_PRESENCE, 0,
        lParam(@ChatPres));
end;

// add message to chat window
function AddMessageToChat(ChatMessage: TPluginMessage): Integer;
begin
    Result := PluginLink.CallService(AQQ_CONTACTS_MESSAGE, 0,
        lParam(@ChatMessage));
end;

function ExecuteMsg(ID: Integer; jid: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_EXECUTEMSG, ID, lParam(jid));
end;

{ OTHER FUNCTIONS }

// create force directories
function CreateDir(dir: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_FORCEDIRECTORIES,
        wParam(dir), 0);
end;

// run interial aqq action
function RunAction(action: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_SYSTEM_RUNACTION, 0, lParam(action));
end;

// returns filename from dir
function GetFileNameFromDir(dir: PWideChar): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService(AQQ_FUNCTION_EXTRACTFILENAME,
        wParam(dir), 0));
end;

// extract res
function ExtractRes(filename: PWideChar; resName: PWideChar;
    resType: PWideChar): Integer;
var
    ResStream: TResourceStream;
begin
    ResStream := TResourceStream.Create(HInstance, resName, resType);
    try
        ResStream.Position := 1;
        ResStream.SaveToFile(filename);
    finally
        ResStream.Free;
    end;
end;

function ExcludeTralingPathDelimiter(dir: PWideChar): PWideChar;
begin
    Result := PWideChar(PluginLink.CallService
        (AQQ_FUNCTION_EXCLUDETRAILINGPATHDELIMITER, wParam(dir), 0));
end;

function AQQShowMessage(typ: Integer; msg: PWideChar): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_SHOWMESSAGE, typ,
        lParam(msg));
end;

function GetNetworkState(StateChange: TPluginStateChange;
    UserIdx: Integer): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_GETNETWORKSTATE,
        wParam(@StateChange), UserIdx);
end;

function IsListReady(): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_ISLISTREADY, 0, 0);
end;

function AQQProcessMessages(): Integer;
begin
    Result := PluginLink.CallService(AQQ_FUNCTION_PROCESSMESSAGES, 0, 0);
end;

function Base64(text: PWideChar; mode: Integer):String;
begin
    Result := String(PluginLink.CallService(AQQ_FUNCTION_BASE64, wParam(text), mode));
end;

end.
