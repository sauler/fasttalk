object frmChoose: TfrmChoose
  Tag = 1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Tworzenie skr'#243'tu'
  ClientHeight = 327
  ClientWidth = 360
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001002000680400001600000028000000100000002000
    0000010020000000000000000000000000000000000000000000000000007976
    76FF767575FF767575FF767575FF767575FF767575FF767575FF767575FF7675
    75FF80817FF3A1A1A588A0A3AB7A9FA4A66EA1A1A4579C9C9C0D000000007675
    75FFFFFFFFBAF0EFEAD8E8E2D3F8E9DEC8FFEBDFC7FFECE0C9FFEEE2CBFFEEE2
    CBFF837A6AFFBC9037FFB28F48FEA99570B3A29C8B2C7F7F7F02000000007675
    75FFF4EEDFDADFCEB8FFC29D86FF853800FFD3B9A4FFEFE4CEFFF0E4CFFFF0E4
    CFFF817A6FFFC19848FFBB9241FFB58C37FFAF862EF7A882252F000000007675
    75FFF5EFE2D9CDAF9DFF873A02FFD3B9A3FFF0E6D4FFEFE4CEFFF1E5CFFFF0E3
    CAFF867D6EFFC69C4CFFBF9746FFB89140FFB08A39FFBD943875000000007675
    75FFFDFDFCBCDFCEB7FE853800FF9C5D30FFC0997AFFF0E4CFFF853800FFF3E7
    D1FF8A7F6DFFC99F4FFFC19949FFB99240FFB89141FFA6792C17000000007675
    75FFFFFFFFBAFCFAF7C2C29D81FC853800FF853800FF853800FF853800FFF9ED
    D8FF8B806DFFC69A4AFFC09746FFC39C4EEFC9A04D2B00000000000000007675
    75FFFFFFFFBAFDFDFDBAFFFFFFBAC29D83F2853800FF853800FF853800FFF7F4
    EEFF958C7FFFF1C980FFD1AA5687000000000000000000000000000000007675
    75FFFFFFFFBAFDFDFDBAB78A6DE6853800FF853800FF853800FF853800FFEAF4
    FCFF838990F5D4AA7F0600000000000000000000000000000000000000007675
    75FFFFFFFFBAFFFFFFBAFFFFFFBAFFFFFFBAFFFFFFBAF4FAFFDDF3F8FCFFF3F7
    FCFF7B7C7ED30000000000000000000000000000000000000000000000007776
    74FF767575E6767575DA767575CF767575C4767575C48B8E91F78B8F92FF8B8F
    92FF8E9090F40000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000AFCCE547C0DCF5FFBDD8F2FFBDD8
    F2FFC4E2FBFF70839A4200000000000000000000000000000000000000000000
    00000000000000000000000000000000000080A0C2C5ACD3F2FFA7CEEEFFA8CE
    EEFFB2DBFBFF4F627BD600000000000000000000000000000000000000000000
    0000000000000000000000000000000024076586ADFF9BCCF6FF98C8F3FF96C7
    F3FF89B3DEFF2B354DFF1212240E000000000000000000000000000000000000
    00000000000000000000000000001E283D192D3F60FF668BB6FF4F6A8FFF2229
    40FF222A40FF2B364DFF1C233124000000000000000000000000000000000000
    000000000000000000000000000000000000354866C2232C42FF27334BFF2A37
    50FF2A3751FF303C55E500000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000363F537435435ACD3542
    5ADB38445C8D00003F040000000000000000000000000000000000000000001F
    00000007000000030000000300000003000000070000000F0000003F0000003F
    0000003F0000FC3F0000F81F0000F81F0000F81F0000F81F0000FE3F0000}
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object titleLabel: TsLabel
    Tag = 2
    Left = 8
    Top = 8
    Width = 173
    Height = 13
    Caption = 'Tworzenie skr'#243'tu dla kontaktu'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
  end
  object titleDescLabel: TsLabel
    Tag = 3
    Left = 8
    Top = 30
    Width = 317
    Height = 13
    Caption = 
      'Wybierz styl skr'#243'tu i wybierz jego nazw'#281', a nast'#281'pnie naci'#347'nij O' +
      'K.'
  end
  object Bevel1: TsBevel
    Left = -4
    Top = 54
    Width = 378
    Height = 2
  end
  object shortcutNameLabel: TsLabel
    Tag = 4
    Left = 8
    Top = 131
    Width = 69
    Height = 13
    Caption = 'Nazwa skr'#243'tu:'
  end
  object tip1Label: TsLabel
    Tag = 5
    Left = 24
    Top = 159
    Width = 299
    Height = 26
    Caption = 
      'Aby ustawi'#263' w'#322'asny awatar dla tego kontaktu kliknij w podgl'#261'd aw' +
      'atara, a nast'#281'pnie wybierz odpowiedni obrazek.'
    Color = clBtnFace
    Enabled = False
    ParentColor = False
    WordWrap = True
  end
  object Bevel2: TsBevel
    Left = 11
    Top = 205
    Width = 337
    Height = 2
  end
  object tip2Label: TsLabel
    Tag = 7
    Left = 24
    Top = 247
    Width = 260
    Height = 26
    Caption = 
      'Zaznaczenie tej opcji pozwala na podgl'#261'danie statusu kontaktu na' +
      ' ikonie.'
    Enabled = False
    WordWrap = True
  end
  object StylesList: TsComboBox
    Left = 6
    Top = 62
    Width = 250
    Height = 60
    Alignment = taLeftJustify
    DropDownCount = 20
    VerticalAlignment = taAlignTop
    Style = csOwnerDrawFixed
    ItemHeight = 54
    ItemIndex = -1
    TabOrder = 0
    OnSelect = StylesListSelect
    OnDrawItem = StylesListDrawItem
  end
  object AddStyleButton: TsBitBtn
    Left = 262
    Top = 62
    Width = 26
    Height = 27
    Hint = 'Dodaj nowy styl'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = AddStyleButtonClick
    ImageIndex = 0
    Images = Icons
  end
  object RemoveStyleButton: TsBitBtn
    Left = 262
    Top = 95
    Width = 26
    Height = 27
    Hint = 'Usu'#324' ten styl'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = RemoveStyleButtonClick
    ImageIndex = 1
    Images = Icons
  end
  object previewPanel: TsPanel
    Left = 294
    Top = 62
    Width = 60
    Height = 60
    BevelInner = bvSpace
    TabOrder = 3
    TabStop = True
    OnClick = previewImageClick
    SkinData.SkinSection = 'PANEL'
    object previewImage: TsImage
      Left = 7
      Top = 7
      Width = 46
      Height = 46
      Picture.Data = {07544269746D617000000000}
      OnClick = previewImageClick
      ImageIndex = 0
      Images = StyleLivePreview
      SkinData.SkinSection = 'CHECKBOX'
    end
  end
  object shortcutNameEdit: TsEdit
    Left = 83
    Top = 128
    Width = 173
    Height = 21
    TabOrder = 4
    OnKeyPress = shortcutNameEditKeyPress
    SkinData.SkinSection = 'EDIT'
  end
  object statusIconCheckbox: TsCheckBox
    Tag = 6
    Left = 3
    Top = 221
    Width = 180
    Height = 20
    Caption = 'Pokazuj status kontaktu na ikonie'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = statusIconCheckboxClick
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sBitBtn1: TsBitBtn
    Left = 166
    Top = 287
    Width = 91
    Height = 32
    Caption = 'OK'
    Default = True
    TabOrder = 6
    OnClick = sBitBtn1Click
    ImageIndex = 2
    Images = Icons
    TextAlignment = taRightJustify
  end
  object cancelButton: TsBitBtn
    Left = 263
    Top = 287
    Width = 91
    Height = 32
    Caption = 'Anuluj'
    TabOrder = 7
    OnClick = cancelButtonClick
    ImageIndex = 3
    Images = Icons
  end
  object Icons: TsAlphaImageList
    Items = <>
    Left = 320
    Top = 128
  end
  object StyleLivePreview: TsAlphaImageList
    Height = 46
    Width = 46
    Items = <>
    Left = 280
    Top = 128
  end
  object SkinManager: TsSkinManager
    ExtendedBorders = True
    InternalSkins = <>
    MenuSupport.IcoLineSkin = 'ICOLINE'
    MenuSupport.ExtraLineFont.Charset = DEFAULT_CHARSET
    MenuSupport.ExtraLineFont.Color = clWindowText
    MenuSupport.ExtraLineFont.Height = -11
    MenuSupport.ExtraLineFont.Name = 'Tahoma'
    MenuSupport.ExtraLineFont.Style = []
    SkinDirectory = 'c:\Skins'
    SkinInfo = 'N/A'
    ThirdParty.ThirdEdits = ' '
    ThirdParty.ThirdButtons = 'TButton'
    ThirdParty.ThirdBitBtns = ' '
    ThirdParty.ThirdCheckBoxes = ' '
    ThirdParty.ThirdGroupBoxes = ' '
    ThirdParty.ThirdListViews = ' '
    ThirdParty.ThirdPanels = ' '
    ThirdParty.ThirdGrids = ' '
    ThirdParty.ThirdTreeViews = ' '
    ThirdParty.ThirdComboBoxes = ' '
    ThirdParty.ThirdWWEdits = ' '
    ThirdParty.ThirdVirtualTrees = ' '
    ThirdParty.ThirdGridEh = ' '
    ThirdParty.ThirdPageControl = ' '
    ThirdParty.ThirdTabControl = ' '
    ThirdParty.ThirdToolBar = ' '
    ThirdParty.ThirdStatusBar = ' '
    ThirdParty.ThirdSpeedButton = ' '
    ThirdParty.ThirdScrollControl = ' '
    ThirdParty.ThirdUpDown = ' '
    ThirdParty.ThirdScrollBar = ' '
    ThirdParty.ThirdStaticText = ' '
    ThirdParty.ThirdNativePaint = ' '
    Left = 280
    Top = 176
  end
  object SkinProvider: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'Tahoma'
    AddedTitle.Font.Style = []
    FormHeader.AdditionalHeight = 0
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 320
    Top = 184
  end
  object StylePreviews: TsAlphaImageList
    Height = 46
    Width = 46
    Items = <>
    UseCache = False
    Left = 200
    Top = 184
  end
  object customAvatarLoad: TsOpenPictureDialog
    Left = 272
    Top = 232
  end
  object fbTimer: TTimer
    OnTimer = fbTimerTimer
    Left = 200
    Top = 240
  end
  object ApplicationEvents1: TApplicationEvents
    OnException = ApplicationEvents1Exception
    Left = 304
    Top = 8
  end
  object styleOpenDialog: TsOpenDialog
    Filter = 'All files|*.*|FastTalk Style|*.zip'
    ZipShowing = zsAsFile
    Left = 248
    Top = 184
  end
end
